# -*- coding: utf-8 -*-


class BaseClientHook(object):
    """Abs class for client hooks.

    This class has method "catsh" which take the queue message from redis and
    convert it to new format.
    """
    def __init__(self):
        pass

    def catch(self, message):
        """Catch the message

        Args:
            message (dict): message from queue

        Raises:
            NotImplementedError: this method must be reimplemented
        """
        raise NotImplementedError('This method must be implemented in a '
                                  'subclass.')

    def modify(self, message):
        """Modify the message

        Args:
            message (dict): message from queue

        Raises:
            NotImplementedError: this method must be reimplemented
        """
        raise NotImplementedError('This method must be implemented in a '
                                  'subclass.')

    def catchAndModify(self, message):
        """Catch the message

        Args:
            message (dict): message from queue

        Raises:
            NotImplementedError: this method must be reimplemented
        """
        raise NotImplementedError('This method must be implemented in a '
                                  'subclass.')

