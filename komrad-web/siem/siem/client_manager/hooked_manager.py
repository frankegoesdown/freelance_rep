# -*- coding: utf-8 -*-
"""
Misses:
    hooker`s loop tries to modify message in all hooks, even it`s not need
"""


from socketio import RedisManager
from flask_login import current_user

class HookedManager(RedisManager):  # pragma: no cover

    name = 'hooked'

    def __init__(self, app, url='redis://localhost:6379/0', channel='socketio',
                 write_only=False):
        self.hooks = []
        self.app_context = app.app_context()

        super(HookedManager, self).__init__(url=url,
                                            channel=channel,
                                            write_only=write_only)

    def addHook(self, obj):
        self.hooks.append(obj)

    def removeHook(self, obj):
        pass

    def _handle_emit(self, message):
        # Events with callbacks are very tricky to handle across hosts
        # Here in the receiving end we set up a local callback that preserves
        # the callback host and id from the sender
        with self.app_context:
            for _obj in self.hooks:
                hooker = _obj()
                try:
                    message = hooker.catchAndModify(message)
                    if not message:
                        return
                except:
                    return
        if not message:
            return
        # print message
        super(HookedManager, self)._handle_emit(message)

    # def initialize(self, server):
    #         super(initialize, self).initialize(server)

