# -*- coding: utf-8 -*-
"""
base instances of objects
"""

from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_nav import Nav
from flask_socketio import SocketIO
from flask_redis import FlaskRedis


db = SQLAlchemy()


login_manager = LoginManager()

redis_store = FlaskRedis()

login_manager = LoginManager()

sio = SocketIO()
