# -*- coding: utf-8 -*-

from flask import jsonify
from siem.wrappers import access_required
from siem.models import Group
from siem.correlation import correlation
import json
import siem.route_access as ra

##! requests are same

@correlation.route('/correl/assign_list')
@access_required(ra.PAGE_INCIDENTS)
def get_assign_list():
    """Return event fields data as json """
    res = []
    for row in Group.query.all():
        res.append({
            'id': row.id,
            'name': row.name,
            'is_user': row.alone,
            'active': row.active
        })

    return jsonify(res)


@correlation.route("/groups/all", methods=["GET"])
@access_required(ra.PAGE_INCIDENTS)
def get_groups():
    groups = Group.get_all()
    return json.dumps(groups), 200
