# -*- coding: utf-8 -*-

import flask
from flask import render_template, url_for, request, current_app, \
    make_response, abort

from ..models.alert_requester import *
from ..models.constants import INC_STATUSES, INC_COLORS
from siem.wrappers import access_required
from siem.models import Group
from siem.models import EventFields
from siem.classes import makeReport


from siem.correlation import correlation
import siem.route_access as ra
import pandas as pd


def getTimeParams(_from, _to):
    interval = _to - _from
    if interval > 12614400:  # 4 years
        return 'A', '%Y'
    if interval > 15768000:   # year
        return 'M', '%m-%Y'
    if interval > 1209600:   # year half
        return 'W', '%d-%m'
    if interval > 172800:    # 2 weeks
        return 'D', '%d-%m'
    if interval > 86400:     # 2 days
        return 'Hour', '%H-%d'
    if interval > 43200:      # a day
        return '30T', '%H:%M'
    if interval > 14400:      # day half
        return '15T', '%H:%M'
    if interval > 2880:      # day/6
        return '10T', '%H:%M'
    if interval > 1440:       # day/30
        return '5T', '%H:%M'
    return 'T', '%H:%M:%S'


@correlation.route("/incidents/report", methods=["GET"])
@access_required(ra.PAGE_INCIDENTS)
def make_inc_report():
    if not request.args.get('inc', None):
        return abort(400)

    render_params = {}
    render_params['inc_id'] = request.args.get('inc')
    render_params['makeGraph'] = int(request.args.get('graph', '0'))
    render_params['makeLog'] = int(request.args.get('log', '0'))
    render_params['makeEvents'] = int(request.args.get('events', '0'))
    render_params['eventFields'] = request.args.getlist('f')
    render_params['eventFields'] = [EventFields.get_by_id_safe(x).serialize \
                                    for x in render_params['eventFields']]

    alert_data = AlertRequester.getAlertByUUID(
        current_user.id, render_params['inc_id'], True, True)
    groups = {}
    for gr in Group.query.all():
        groups[gr.id] = {
            'name': gr.name,
            'alone': gr.alone
        }

    js_files = [
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='bower_components/jquery/dist/jquery.min.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='bower_components/highcharts/highcharts.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='bower_components/highcharts/highcharts-3d.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='bower_components/highcharts/highcharts-more.js')
    ]

    rendered_template = render_template(
        "correlation/reports/alert.html", js=js_files,
        data=render_params, alert_data=alert_data, time=int(time.time()),
        groups=groups)

    response = make_response(makeReport(rendered_template))
    response.headers['Content-Disposition'] = "attachment; \
        filename='some.pdf"
    response.headers['Content-Type'] = "application/pdf; charset=utf-8"

    return response


@correlation.route("/incidents/report/all", methods=["POST"])
@access_required(ra.PAGE_INCIDENTS, act_type=1)
def make_all_inc_report():

    try:
        inc_query = request.get_json().get('query')
    except:
        abort(400)
    inc_order_by = request.get_json().get('order_by', 'id')
    inc_order = request.get_json().get('order', 'desc')

    incidents, total = AlertRequester.getAll(
        current_user.id, inc_query, 'alert_time', 'asc')

    df_rule, fmt = getTimeParams(
        incidents[0]['alert_time'], incidents[-1]['alert_time'])

    df = pd.DataFrame(incidents)
    statuses = df.groupby('status').count().to_dict(orient="index")
    status_series = []
    for k, v in INC_STATUSES.iteritems():
        status_series.append({
            'name': unicode(v),
            'y': statuses.get(int(k), {'alert_id': 0})['alert_id'],
            'color': INC_COLORS[k]
        })

    directives = df.groupby('ru_name').count()
    directives = [
        [i, r['alert_id']] for i, r in directives.iterrows()]

    df['alert_time'] = pd.to_datetime(df['alert_time'], unit='s')
    col_to_del = [x for x in df.columns if x not in ['alert_time']]
    for col in col_to_del:
        df.drop(col, axis=1, inplace=True)
    g = df.resample(df_rule, on='alert_time').count()

    time_plot = [
        [i.strftime(fmt), r['alert_time']] for i, r in g.iterrows()]


    js_files = [
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='bower_components/jquery/dist/jquery.min.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='bower_components/highcharts/highcharts.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='bower_components/highcharts/highcharts-3d.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='bower_components/highcharts/highcharts-more.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='bower_components/moment/min/moment.min.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='js/jQuery-QueryBuilder/dist/js/query-builder.standalone.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='js/jQuery-QueryBuilder/dist/i18n/query-builder.ru.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='js/jQuery-QueryBuilder_plugins/QueryBuilder_mapping2_plugin/plugin_mapping2.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='js/jQuery-QueryBuilder_plugins/QueryBuilder_datetimepicker_plugin/plugin_datetimepicker.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='js/jQuery-QueryBuilder_plugins/QueryBuilder_list_search_plugin/plugin_list_search.js'),
    ]

    inc_query = qbUUID2Name(inc_query)

    rendered_template = render_template(
        "correlation/reports/alerts.html", js=js_files,
        statuses=json.dumps(status_series),
        directives=json.dumps(directives), time=int(time.time()),
        timeline=json.dumps(time_plot), alert_list=incidents,
        qb_rules=json.dumps(inc_query),
        status_names=INC_STATUSES)

    response = make_response(makeReport(rendered_template))
    response.headers['Content-Disposition'] = "attachment; \
        filename='incidents.pdf"
    response.headers['Content-Type'] = "application/pdf; charset=utf-8"

    return response