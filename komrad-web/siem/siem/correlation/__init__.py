# -*- coding: utf-8 -*-
"""Blueprint and controllers of event listener module

Attributes:
    ACCESS_MODULE (str): module name for access wrapper
    event_listener (TYPE): Blueprint for this module
"""
from flask import Blueprint

correlation = Blueprint('correlation', __name__,
                        template_folder='templates')

ACCESS_MODULE = 'access_correl'

from controllers import *
from sio import *
