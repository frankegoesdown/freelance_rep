# -*- coding: utf-8 -*-

from siem.wrappers import access_required

from flask_socketio import Namespace, emit, join_room, leave_room, \
    close_room, rooms, disconnect
from siem.extensions import sio
from flask import request
from ..models import AlertRequester
from flask_login import current_user
from flask import current_app
from flask import copy_current_request_context
import eventlet

import siem.route_access as ra


class CorrelNamespace(Namespace):

    def getRoomName(self):
        return current_app.config['DEFAULT_ROOM_PREFIX'] + str(current_user.id)

    def on_leave(self, message):
        leave_room(self.getRoomName())

    def on_disconnect_request(self):
        leave_room(self.getRoomName())
        disconnect()

    @access_required(ra.PAGE_DIR_CONSTRUCTOR)
    def on_join_corr(self, data):

        @copy_current_request_context
        def makeNote(_id):
            req_json = {
                'dir': '',
                'number': 10,
                'page': 0,
                'order_by': 'id',
                'order': 'desc',
                "body": {
                    "condition": "AND",
                    "rules": [{
                        "field": "id",
                        "id": "id",
                        "input": "text",
                        "operator": "greater",
                        "type": "integer",
                        "value": 0
                    }]
                }
            }
            data = {
                'a': 'noteInfo',
                't': AlertRequester.getTotal(current_user.id),
                'l10': AlertRequester.getByPage(
                    current_user.id, req_json)['page']
            }
            join_room(self.getRoomName())
            emit('note', data, room=self.getRoomName())

        eventlet.spawn(makeNote, current_user.id)

    @access_required(ra.PAGE_DIR_CONSTRUCTOR)
    def on_modify(self, data):
        @copy_current_request_context
        def makeNote(data):
            AlertRequester.updateAlert(data)

        eventlet.spawn(makeNote, data)

    def on_connect(self):
        print('Client connected', request.sid)

    def on_disconnect(self):
        leave_room(self.getRoomName())
        print('Client disconnected', request.sid)


sio.on_namespace(CorrelNamespace('/correl'))
