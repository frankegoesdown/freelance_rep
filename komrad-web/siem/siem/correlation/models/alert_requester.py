# -*- coding: utf-8 -*-

from sqlalchemy import Column
from flask_login import current_user
from siem.extensions import redis_store

from siem.extensions import db
from siem.utils import get_current_time
import requests
import time
import datetime
import json
import uuid
from collections import Counter

import eventlet

from flask import current_app as app

from siem.models import Group, User
from siem.event_listener.models import LuceneRequester


def getSerializedGroups(groups_string):
    """Просто фунцкция для поиска группы по Id в списке моделей groups

    Args:
        groups_string (TYPE): unsplitted groups list
        groups (list of Group): list of Group models

    Returns:
        TYPE: Serialized list on None
    """
    if not groups_string:
        return None

    tmp_list = groups_string.split(';')
    return [int(x) for x in tmp_list]


def recDirNamereplace(qb_part, names):
    for r in qb_part['rules']:
        if 'condition' in r.keys():
            r = recDirNamereplace(r, names)
            continue
        if r['id'] == u'directive_id':
            r['value'] = names.get(r['value'], '')
    return qb_part


def qbUUID2Name(qb):
    names = AlertRequester.getDrilldown(current_user.id)
    uuid_and_name = {}
    for i in names:
        uuid_and_name[i['id']] = i['ru_name']
    return recDirNamereplace(qb, uuid_and_name)


class AlertRequester(object):

    @staticmethod
    def getByPage(user_id, json_request):
        json_request['user'] = user_id
        r = requests.post(app.config['ALERTS_STAT_URL'] + 'incidents/page',
            json=json_request)
        if r.status_code != requests.codes.ok:
            r.raise_for_status()
        return json.loads(r.content)

    @staticmethod
    def getAll(user_id, query, order_by='id', order='desc'):
        req = {
            "body": query,
            "dir": "",
            "order": order,
            "order_by": order_by,
            "page": 0,
            "number": 1,
            "user": user_id
        }
        r = requests.post(app.config['ALERTS_STAT_URL'] + 'incidents/all',
            json=req)
        if r.status_code != requests.codes.ok:
            r.raise_for_status()
        data = json.loads(r.content)
        return data['page'], data['total']


    @staticmethod
    def getAlertByUUID(user_id, uuid, with_events=True, with_graph=False):

        # запрос основной информации
        data = requests.get(app.config['ALERTS_STAT_URL'] +
                            'incidents/by_id?id=' + uuid + '&user=' +
                            str(user_id))
        if data.status_code != 200:
            data.raise_for_status()
        data = json.loads(data.content)

        # select all groups (because not many now)
        # exchanging groups list and users
        for i in xrange(len(data['history'])):
            data['history'][i]['changed_by'] = User.get_by_id(
                data['history'][i]['changed_by']).serialize_strict
            data['history'][i]['added_groups'] = getSerializedGroups(
                data['history'][i]['added_groups'])
            data['history'][i]['removed_groups'] = getSerializedGroups(
                data['history'][i]['removed_groups'])

        if not with_events:
            return data

        # тут 2 последовательных запроса - лучше запилить через краулер
        data_events = requests.get(app.config['ALERTS_STAT_URL'] +
            'incidents/alerts/by_id?id=' + data['alert_id']).content
        data_events = json.loads(data_events)
        data['events'] = json.loads(data_events['events'])
        levels = requests.get(app.config['FACTS_BASE_URL'] +
            'correl/directive_levels?id=' + data['directive_id']).content
        levels = json.loads(levels)

        # замена уровней на список объектов с извлеченными именами
        events_data = []
        inc_event_levels = sorted([int(x) for x in data['events'].keys()])
        inc_event_levels = [str(x) for x in inc_event_levels]

        all_events = []
        for i, k in enumerate(inc_event_levels):
            if levels[k][u'absence']:
                continue
            try:
                all_events += sorted(data['events'][k], key=lambda x: float(x[0:-7]))
            except:
                all_events += data['events'][k]

        try:
            normalized_events = LuceneRequester.getEvents(all_events)
        except:
            data['events'] = []
            return data

        # подмена списков ийдишников событий на сами события
        _from = 0
        for lvl in inc_event_levels:
            lvl_name = levels.get(lvl, {}).get(u'name', lvl)
            if levels[lvl][u'absence']:
                events_data.append({
                    u'name': lvl_name,
                    u'events': [],
                    u'absense': True,
                    u'timer': levels[lvl][u'timer']
                })
                continue
            _to = len(data['events'][lvl]) + _from
            events_data.append({
                u'name': lvl_name,
                u'events': normalized_events[_from:_to],
                u'absense': False,
                u'timer': levels[lvl][u'timer']
            })
            _from = _to
        # запрос данных из редиса (сразу все):
        data['events'] = events_data

        if not with_graph:
            return data

        graph_data = {
            'lvls': [],
            'events': []
        }

        # prepare data for graph
        # !1 if all rules are absense
        if all(x[u'absense'] is True for x in data['events']):
            end_time = data[u'alert_time']
            for lvl in reversed(events_data):
                end_time -= lvl[u'timer']
                graph_data['lvls'].append({
                    'ts': end_time,
                    'name': lvl['name']
                })
            data['graph'] = graph_data
            return data

        # Если absense встретились перед остальными правилами, тогда нужно
        # считать отступ по времени от начала первого not_absense правила
        absense_from_start = False  # первые n правил - absense
        right_ts_board = None  # правая граница правила (ts последнего события)
        for idx, lvl in enumerate(data['events']):
            if lvl['absense']:
                # если это первые правила то просто их пропцскаем
                if idx == 0:
                    absense_from_start = True
                if idx > 0 and absense_from_start:
                    continue
                # просто рприбавляем время и иджектим палочку
                if idx > 0 and not absense_from_start:
                    right_ts_board += lvl['timer']
                    graph_data['lvls'].append({
                        'ts': right_ts_board,
                        'name': lvl['name'],
                        'absense': True,
                        'timer': lvl['timer']
                    })
                continue
            if len(lvl['events']) == 0:
                continue
            graph_data['lvls'].append({
                'ts': int(lvl['events'][0]['timestamp']),
                'name': lvl['name']
            })

            # хук на параметр, если найден не эбсенс, и все эбсенсы в начале,
            # то расчет их по отношению к первому событию правила
            if absense_from_start:
                left_board = int(lvl['events'][0]['timestamp'])
                for passed_id in range(idx, 0, -1):
                    left_board -= lvl['timer']
                    graph_data['lvls'].insert(0, {
                        'ts': left_board,
                        'name': lvl['name'],
                        'absense': True,
                        'timer': lvl['timer']
                    })
                absense_from_start = False

            tuples = Counter(
                x['timestamp'] for x in lvl['events']).most_common()
            for ts, value in tuples:
                graph_data['events'].append({
                    'ts': int(ts),
                    'count': value,
                    'absense': False,
                        'timer': lvl['timer']
                })
            right_ts_board = graph_data['events'][-1]['ts']


        # end_time = data[u'alert_time']
        # for lvl in reversed(events_data):
        #     # if lvl['absense']:
        #         end_time -= lvl[u'timer']
        #         graph_data['lvls'].append({
        #             'ts': end_time,
        #             'name': lvl['name']
        #         })
        #     # don`t know what is it!
        #     if len(lvl['events']) == 0:
        #         continue
        #     end_time = int(lvl['events'][0]['timestamp'])
        #     graph_data['lvls'].append({
        #         'ts': end_time,
        #         'name': lvl['name']
        #     })
        #     tuples = Counter(
        #         x['timestamp'] for x in lvl['events']).most_common()
        #     for ts, value in tuples:
        #         graph_data['events'].append({
        #             'ts': int(ts),
        #             'count': value
        #         })


        data['graph'] = graph_data

        return data


    def prepareGraphData(self, data):
        pass

    @staticmethod
    def prepareEvents_csv(user_id, uuid):
        # запрос основной информации
        events = AlertRequester.getAlertByUUID(user_id, uuid, True)['events']


    @staticmethod
    def updateAlert(data):
        to_send = {
            'status': data.get(u's'),
            'risk': data.get(u'r'),
            'comment': data.get(u'c'),
            'groups': data.get(u'gr'),
            'alert_id': data.get(u'id'),
            'user_id': current_user.id
        }
        print to_send
        data = requests.post(app.config['ALERTS_STAT_URL'] +
                             'incidents/update', json=to_send)
        return None

    @staticmethod
    def deleteAlert(data):
        to_send = {
            'alert_id': data.get(u'id'),
            'user_id': current_user.id
        }
        data = requests.post(app.config['ALERTS_STAT_URL'] +
                             'incidents/remove', json=to_send)
        return None	
		
    @staticmethod
    def getDrilldown(user_id):
        data = requests.get(app.config['ALERTS_STAT_URL'] +
                            'incidents/drilldown?user=' + str(user_id)).content
        return json.loads(data)

    @staticmethod
    def getTotal(user_id):
        total_url = app.config['ALERTS_STAT_URL'] + \
            'incidents/total?user=' + str(user_id)
        data = requests.get(total_url)
        if data.status_code != 200:
            return {"total": 0, "opened": 0}
        return json.loads(data.content)

    # @staticmethod
    # def getLast(user_id, limit=10):
    #     data = requests.get(app.config['ALERTS_STAT_URL'] +
    #                         'incidents/total/' + str(user_id) + '/' +
    #                         str(limit))
    #     if data.status_code != 200:
    #         return []
    #     return json.loads(data.content)

