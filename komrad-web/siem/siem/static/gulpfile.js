
const gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    csso = require('gulp-csso'), // Минификация CSS
    Server = require('karma').Server,
    jasmine = require('gulp-jasmine'),
    concat = require('gulp-concat'),
    ngAnnotate = require('gulp-ng-annotate');


var paths = {
    src: ['js/app/app.komrad2.js','js/app/**/*module*.js', 'js/app/**/*.js'],
    dest: 'build'
};

gulp.task('build', function() {
    return gulp.src(paths.src)
        .pipe(concat('komrad.min.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest(paths.dest));
});

gulp.task('concat', function() {
    return gulp.src(paths.src)
        .pipe(concat('komrad.js'))
        .pipe(gulp.dest(paths.dest));
});

gulp.task('minify', function() {
    return gulp.src('build/komrad.js')
        .pipe(uglify())
        .pipe(gulp.dest(paths.dest));

});

gulp.task('ng-annotate', function() {
    return gulp.src('build/komrad.js')
        .pipe(ngAnnotate())
        .pipe(gulp.dest(paths.dest));

});


gulp.task('cssmin', function () {
    gulp.src(['css/**/*.css', '!css/**/*.min.css'])
        .pipe(csso())
        .pipe(gulp.dest('build'));
});

/**
 * Run test once and exit
 */
gulp.task('test', function (done) {
    new Server({
        configFile: require('path').resolve('karma.conf.js'),
        singleRun: true
    }, done).start();

});

gulp.task('default', function () {
    gulp.src('spec/test.js')
        //gulp-jasmine works on filepaths so you can't have any plugins before it
        .pipe(jasmine());
});


