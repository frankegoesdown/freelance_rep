(function () {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .controller('alertsNotify', AlertsNotificator);

    AlertsNotificator.$inject = ['$scope', 'sioCorrel', 'Notification', '$state'];

    function AlertsNotificator($scope, sioCorrel, Notification, $state) {
        var alerts_in_menu = 10;
        $scope.alerts_count = 0;
        $scope.alerts_list = [];
        $scope.dataPulled = false;
        $scope.startTime = 0;

        sioCorrel.forward('new', $scope);
        sioCorrel.forward('update', $scope);
        sioCorrel.forward('note', $scope);

        var close_note = $scope.$on('note', function (ev, data) {
            $scope.alerts_count = data.t.opened;
            $scope.alerts_list = data.l10;
            close_note();
            $scope.dataPulled = true;
            Notification.info({
                message:'Открытых инцидентов: ' + data.t.opened,
                delay: 10000
            });
        });

        $scope.$on('new', function (ev, data) {
            console.log(data)
            $scope.alerts_count++;
            var now = moment().unix(),
                tempCurrent = now;
            if (!$scope.startTime) {
                $scope.startTime = now;
                tempCurrent = 0;
            }
            if ((now - $scope.startTime) < 5 && (now - $scope.startTime) > 0) {
                return;
            }
            Notification.error({
                message:'Новый инцидент: ' + ' ' + data.ru_name,
                delay: 10000
            });
            if ($scope.alerts_list.length > alerts_in_menu) {
                $scope.alerts_list.splice(-1, 1);
            }
            $scope.alerts_list.unshift(data);
            if (tempCurrent) {
                $scope.startTime = 0;
            }
        });


        $scope.$on('update', function (ev, data) {
            if (data.new_status === 2)
                $scope.alerts_count--;
            if (data.new_status === 0)
                $scope.alerts_count++;
            for (var i = 0; i < alerts_in_menu; i++) {
                if ($scope.alerts_list[i].alert_id != data.alert_id) continue;
                $scope.alerts_list[i].status = data.status;
            }
        });

        $scope.goToAlert = function (alert_id) {
            $state.transitionTo('single_alert', {alertId: alert_id});
        }

    }


})();

