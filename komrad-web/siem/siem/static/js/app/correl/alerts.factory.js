(function () {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .factory('AletrsContent', alertsGetterFactory);

    alertsGetterFactory.$inject = ['$http', '$filter', 'Notification', '$q', 'statuses'];

    function alertsGetterFactory($http, $filter, Notification, $q, statuses) {
        //main service constructor function
        var SearchContent = function () {
            this.message = '';
            this.busy = false;
            this.has_error = false;
            this.page = 0;
            this.dir = '';
            this.drilldown = [];
            this.order = 'desc';
            this.order_by = 'id';
            this.rule = {
                condition: 'AND',
                rules: [{
                    field: 'id',
                    id: 'id',
                    input: 'text',
                    operator: 'greater',
                    type: "integer",
                    value: 0
                }]
            };
            this.drilldownBusy = false;
            this.incStatBusy = false;
        };

        SearchContent.prototype.setStates = function (ids, new_state) {
            return $http.post('/incidents/set_states', {state: new_state, ids: ids})
                .success(function (data) {
                });
        }

        SearchContent.prototype.deleteIncedent = function (ids) {
            return $http.post('/incidents/delete', {ids: ids})
                .success(function (data) {
                });
        }

        //get data to left-side drilldown menu
        SearchContent.prototype.pullDrilldown = function () {
            this.drilldownBusy = true;
            var deferred = $q.defer();
            var drilldown = [];
            var filters = [{
                id: 'id',
                label: '№',
                type: 'integer',
                input: 'text',
                operators: ['equal',
                    'not_equal',
                    'less',
                    'greater',
                    'contains',
                    'not_contains']
            }, {
                id: 'directive_id',
                label: 'Имя директивы',
                type: 'string',
                input: 'select',
                operators: ['equal', 'not_equal'],
                values: {}
            }, {
                id: 'alert_time',
                label: 'Дата фиксации',
                type: 'datetime',
                operators: ['equal',
                    'not_equal',
                    'less',
                    'greater',
                    'between',
                    'is_null',
                    'is_not_null']
            }, {
                id: 'events_number',
                label: 'События',
                type: 'integer',
                input: 'text',
                operators: ['equal',
                    'not_equal',
                    'less',
                    'greater',
                    'contains',
                    'not_contains',
                    'is_null',
                    'is_not_null']
            }, {
                id: 'duration',
                label: 'Длительность',
                type: 'time',
                operators: ['equal',
                    'not_equal',
                    'less',
                    'greater',
                    'contains',
                    'not_contains',
                    'is_null',
                    'is_not_null']
            }, {
                id: 'risk',
                label: 'Риск',
                input: 'select',
                values: {
                    0: '0',
                    1: '1',
                    2: '2',
                    3: '3',
                    4: '4',
                    5: '5',
                    6: '6',
                    7: '7',
                    8: '8',
                    9: '9',
                    10: '10'
                },
                operators: ['equal',
                    'not_equal',
                    'less',
                    'greater',
                    'contains',
                    'not_contains',
                    'is_null',
                    'is_not_null']
            }, {
                id: 'status',
                label: 'Статус',
                type: 'integer',
                input: 'select',
                values: {
                    0: 'Новый',
                    1: 'Просмотрен',
                    2: 'Закрыт'
                },
                operators: ['equal', 'not_equal']
            },  {
                id: 'first_event_time',
                label: 'Дата начала',
                type: 'datetime',
                operators: ['equal',
                    'not_equal',
                    'less',
                    'greater',
                    'between',
                    'is_null',
                    'is_not_null']
            }];
            $http.get('/correl/alerts/drilldown')
                .success(function (data) {
                    if (typeof data === 'undefined') {
                        return;
                    }
                    drilldown = this.drilldown.concat(data.response);

                    data.response.forEach(function (item, i) {
                        filters[1].values[item.id] = item.ru_name;
                    });

                    deferred.resolve({
                        drilldown: drilldown,
                        filters: filters
                    });
                    this.has_error = false;
                    this.drilldownBusy = false;
                }.bind(this))
                .error(function (data) {
                    Notification.error("Ошибка получения данных с сервера.");
                    this.drilldownBusy = false;
                    this.has_error = true;
                }.bind(this))
            return deferred.promise
        };

        //get data to open / total alerts statistic
        SearchContent.prototype.incedentsStat = function () {
            this.incStatBusy = true;
            var deferred = $q.defer();

            $http.get('/correl/alerts/stat')
                .success(function (data) {
                    deferred.resolve({
                        opened: data.total.opened,
                        viewed: data.total.viewed,
                        closed: data.total.closed,
                        total: data.total.total
                    });
                    this.incStatBusy = false;
                }.bind(this))
                .error(function (data) {
                    Notification.error("Ошибка получения данных с сервера.");
                    this.incStatBusy = false
                    this.has_error = true;
                }.bind(this))
            return deferred.promise;
        }

        return SearchContent;
    }

})();
