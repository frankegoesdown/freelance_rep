(function () {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .factory('DirTreeContent', dirTreeFactory);

    dirTreeFactory.$inject = ['$http', 'Notification', '$filter'];

    function dirTreeFactory($http, Notification, $filter) {
        var SearchContent = function(qb) {
            this.items = [];
            this.total = 0;
        };

        SearchContent.prototype.loadData = function() {
            $http.get('/correl/tree').success(function(data) {
                this.items = data;
                this.total = this.items.length;
            }.bind(this))
            .error(function(data) {
                Notification.error("Ошибка получения данных с сервера.");
            }.bind(this))
        };

        SearchContent.prototype.getElByUUID = function(uuid){
            return $filter('getByField')(this.items, uuid, 'uuid');
        }

        return SearchContent;
    }
})();