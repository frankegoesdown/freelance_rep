(function () {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .controller('alertsListController', alertsListCtrl)
        .controller('deleteAlertCtrl', deleteAlertCtrl)

    alertsListCtrl.$inject = ['$scope', '$filter', '$q', '$timeout', '$uibModal', 'ngPanels',
        'AletrsContent', 'DirTreeContent', 'risks', 'statuses', 'sioCorrel', 'moment',
        'firstLoadData', 'incStat', 'drilldown'];

    deleteAlertCtrl.$inject = ['$scope', '$http', '$uibModalInstance', 'ids'];

    function alertsListCtrl($scope, $filter, $q, $timeout, $uibModal, ngPanels, AletrsContent, DirTreeContent,
                            risks, statuses, sioCorrel, moment, firstLoadData, incStat, drilldown) {
        $scope.message = ''

        $scope.preloadData = firstLoadData.data;
        $scope.service = firstLoadData.service;
        $scope.rules = $scope.service.rule;
        $scope.statistics = incStat;
        $scope.statuses = statuses;
        //get filters and drilldown
        $scope.drillAndFilters = drilldown;
        $scope.incStat = {};
        $scope.searchDrilldown = ''; //значение поиска по умолчанию drilldown
        $scope.sortType = ''; // значение сортировки по умолчанию drilldown
        $scope.sortReverse = {};  // обратная сортировка drilldown

        $scope.data = new AletrsContent();

        $scope.$on('$viewContentLoaded', function () {
            $scope.tree = new DirTreeContent();
            $scope.tree.loadData();
        });

        $scope.panels = ngPanels.newGroup('demo', {
            one: {},
            two: {
                masks: ['one']
            }
        });

        $scope.openPanelTwo = function () {
            $scope.panels.open('two');
        };

        //left-side drilldown getter
        $scope.displayedDrilldown = drilldown.drilldown;

        //open/total alert stat getter
        $scope.incStat.opened = $scope.statistics.opened;
        $scope.incStat.viewed = $scope.statistics.viewed;
        $scope.incStat.closed = $scope.statistics.closed;
        $scope.incStat.total = $scope.statistics.total;

        $scope.ruNameFromTree = function (uuid) {
            var data;
            try {
                data = $scope.tree.getElByUUID(uuid).ru_name;
            } catch (e) {
                data = 'загрузка...';
            }
            return data;
        };


        //go to single alert page
        $scope.gotoDirective = function (dir_id) {
            var url = $state.href('loaded_dirconstructor',
                {
                    dirId: dir_id
                }
            );
            window.open(url, '_blank');
        }


        //drilldown filter main table
        $scope.setDrilldownFilter = function (uuid) {
            var body = {
                condition: 'AND',
                rules: [{
                    field: 'directive_id',
                    id: 'directive_id',
                    input: 'text',
                    operator: 'equal',
                    type: "integer",
                    value: uuid
                }]
            };
            $scope.$broadcast('qb.setFirstRule', body);
        };

        $scope.$on('qb.firstFilterSet', function (e, data) {
            $scope.$broadcast('table.setFilter', data);
        });


        $scope.setStates = function (new_state) {
            $scope.$broadcast('correl.table.getSelectedRows', new_state)
        };

        $scope.deleteIncedents = function () {
            $scope.$broadcast('correl.table.getSelectedRows', null)
        };

        $scope.$on('correl.table.resolveSelectedRows', function (e, data) {
            var ids = [];
            data.selectedRows.forEach(function (item) {
                ids.push(item.id);
            });
            //var ids = []
            //for (var i = 0; i < $scope.alertsData.length; i++) {
            //if ($scope.alertsData[i].ch)
            // ids.push($scope.alertsData[i].id);
            // }
            if (data.newStatus !== null) {
                $scope.data.setStates(ids, data.newStatus);
                return;
            }

            var removeModalInstance = $uibModal.open({
                scope: $scope,
                templateUrl: 'dialog.html',
                controller: 'deleteAlertCtrl',
                windowClass: "app-modal-window",
                size: 'sm',
                resolve: {
                    ids: function () {
                        return ids;
                    }
                }
            });

            removeModalInstance.result.then(function (ids) {
                if (ids) {
                    $scope.data.deleteIncedent(ids);
                }

            }, function () {
            });
        });

        //qb filters getter and other options
        var filters = $q.defer();
        filters.resolve(drilldown.filters);
        $scope.filters = filters.promise;
        $scope.lang = 'ru';
        $scope.plugins = ['sortable', 'datetimepicker'];

        //listen for qb rules resolv and send request
        $scope.$on('resolveGetRules', function (e, rules) {
            $scope.$broadcast('table.setFilter', rules)
        });

        //qb getRules btn
        $scope.setQuery = function () {
            $scope.$broadcast('qb.getRules');
        };

        //export result-table to csv
        $scope.exportData = function (fmt) {
            if (fmt === 'csv') {
                $scope.$broadcast('table.export');
                return;
            }
            $scope.service.exportPDF();
        };


        sioCorrel.forward('update', $scope);
        sioCorrel.forward('removed', $scope);
        $scope.$on('update', function (ev, data) {
            $scope.$broadcast('table.update', data)
        });

        $scope.$on('removed', function (ev, data) {
            data.remove = true;
            $scope.$broadcast('table.update', data)
        });

        var checked = false;
        $scope.toggleAll = function () {
            checked = !checked;
            if (checked) {
                $scope.$broadcast('table.selectAllRows');
            } else {
                $scope.$broadcast('table.deselectAllRows');
            }
        }

        $scope.$on('hideOvelray', function (e, data) {
            $scope.$broadcast('correl.table.hideOvelray', null)
        })

    }

    function deleteAlertCtrl($scope, $http, $uibModalInstance, ids) {
        
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
            $scope.$emit('hideOvelray', null)
        }

        $scope.remove = function () {
            $uibModalInstance.close(ids);
        }


    }
})();

