(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .controller('UsersAndRolesController', usersAndRolesCtrl);

    usersAndRolesCtrl.$inject = ['$scope', 'UsersModel', 'RolesModel', 'accessLvls',
        '$uibModal', '$filter', 'GroupsModel', 'appModulesHierarchy', 'rmConfModal'];

    function usersAndRolesCtrl($scope, UsersModel, RolesModel, accessLvls, $uibModal,
                               $filter, GroupsModel, appModulesHierarchy, rmConfModal) {
        $scope.users = [];
        $scope.roles = [];
        $scope.lvls = accessLvls;
        $scope.accessModules = function(){
            var accessModules = angular.copy(appModulesHierarchy);
            for(var i=0; i<accessModules.length && i<5; ++i){
                accessModules[i].displayed = true;
            }
            return accessModules;
        }();

        $scope.displayedModules = [];

        $scope.setDisplayedModules = function(){
            $scope.displayedModules = [];
            for(var i=0; i<$scope.accessModules.length; ++i){
                if ($scope.accessModules[i].displayed)
                    $scope.displayedModules = $scope.displayedModules
                        .concat($scope.accessModules[i].submodules);
            }
        };

        $scope.$on('$viewContentLoaded', function() {
            $scope.roles = RolesModel();
            $scope.roles.get();
            $scope.users = UsersModel();
            $scope.users.get();
            $scope.groups = new GroupsModel();
            $scope.groups.get();

            // this is the array which table of roles will display
            $scope.setDisplayedModules();
        });

        $scope.getRoleName = function(id){
            var role = $scope.roles.getRoleById(id);
            if (role)
                return role.name;
            return '';
        };

        $scope.getGroupName = function(id){
            var gr = $scope.groups.getById(id);
            if (!gr){
                return ;
            }
            return $scope.groups.getById(id).name;
        };

        $scope.getGroups = function(list){
            var result = [];
            for (var i = 0; i < list.length; i++) {
                var gr = $scope.groups.getById(list[i]);
                if (!gr){
                    continue ;
                }
                result.push(gr.name);
            }

            return result;
        };

        $scope.getLvlName = function(id){
            var selected = $filter('filter')(accessLvls, {id: id});
            return selected[0].name;
        };

        $scope.addUser = function(user_data){
            $scope.users.add(user_data).then(function(response) {
            });
        };

        $scope.updateUser = function(user_data){
            $scope.users.update(user_data).then(function(response) {
            });
        };


        $scope.rmUser = function(row){
            $scope.users.disable(row.id).then(function(response) {
            });
        };

        $scope.restoreUser = function(row){
            $scope.users.restore(row.id).then(function(response) {
            });
        };


        $scope.addRole = function(role_data){
            $scope.roles.add(role_data).then(function(response) {
            });
        };

        $scope.updateRole = function(role_data){
            $scope.roles.update(role_data).then(function(response) {
            });
        };

        $scope.rmRole = function(row){
            $scope.roles.remove(row).then(function(response) {
            });
        };

        $scope.exportRoles = function(fmt){
            $scope.roles.export(fmt).then(function(response) {
            });
        };


        $scope.addGroup = function(role_data){
            $scope.groups.add(role_data).then(function(response) {
            });
        };

        $scope.updateGroup = function(role_data){
            $scope.groups.update(role_data).then(function(response) {
            });
        };

        $scope.rmGroup = function(row){
            $scope.groups.remove(row.id).success(function(response) {
            });
        };

        $scope.openAccessMap = function (access_instance, type) {
            /* if row is not undefind, the modal instance will update record, not create */
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'rolesModal.html',
                controller: 'RolesController',
                size: 'lg',
                windowClass:"app-modal-window",
                scope: $scope,
                resolve: {
                    controlService: function() {
                        if (type === 'user')
                            return $scope.users;
                        return $scope.roles;

                    },
                    statement: function () {
                        if (!access_instance)
                            return undefined;
                        var i_wanna_be_edited = angular.copy(access_instance)

                        // amplify access map by user
                        if (type === 'user'){
                            i_wanna_be_edited.access_map = $scope.users.amplifyAccessMap(i_wanna_be_edited);
                            console.log(i_wanna_be_edited)
                        }
                        return i_wanna_be_edited;
                    },
                }
            });

            modalInstance.result.then(function (data) {
            }, function () {
            });

        }

        /* if row is not undefind, the modal instance will update record, not create */
        $scope.openUserModal = function (row) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: function(row){
                    if (row != undefined){
                        return 'updateUserModal.html';
                    }
                    else{
                        return 'createUserModal.html';
                    }
                }(row),
                controller: 'createUserCntroller',
                size: 'lg',
                windowClass:"app-modal-window",
                scope: $scope,
                resolve: {
                    user_data:function () {
                        if (row)
                            return angular.copy(row)
                        return undefined;
                    },
                    groupsObj: function () {
                        return $scope.groups;
                    },
                    control_service: $scope.users
                }
            });


            modalInstance.rendered.then(function () {
                $scope.$broadcast('runScropper', row);
            }, function () {
            });

            modalInstance.result.then(function (user_data) {
/*                if (row === undefined)
                    $scope.addUser(user_data);
                else
                    $scope.updateUser(user_data);*/
            }, function () {
            });
        };

        /* if row is not undefind, the modal instance will update record, not create */
        $scope.openGroupModal = function (row) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'groupsModal.html',
                controller: 'GroupsCntroller',
                size: 'lg',
                windowClass:"app-modal-window",
                resolve: {
                    groupData: function () {
                        if (row){
                            return row;
                        }
                        return undefined;
                    },
                    control_service: $scope.groups
                }
            });


            modalInstance.result.then(function (data) {
                //if (row === undefined)
                    //$scope.addGroup(data);
                //else
                    //$scope.updateGroup(data);
            }, function () {
            });
        };

        $scope.confirmRemoving = function (row, type) {

            var head_msg = 'Удаление ';
            var body_msg = 'Удалить ';
            var success_cb = undefined;

            if (type === 'user') {
                head_msg += 'пользователя';
                body_msg += 'пользователя <b>' + row.username + '</b>?';
                success_cb = $scope.rmUser;
            }
            else if (type === 'role') {
                head_msg += 'роли';
                body_msg += ' роль <b>' + row.name + '</b>?';
                success_cb = $scope.rmRole;
            }
            else if (type === 'group') {
                head_msg += 'группы';
                body_msg += ' группу <b>' + row.name + '</b>?';
                success_cb = $scope.rmGroup;
            }
            else {
                console.log('Wrong action');
                return;
            }

            rmConfModal.remove(
                head_msg, body_msg,
                function () {
                    success_cb(row);
                },
                function () {});

        };


    }

})();