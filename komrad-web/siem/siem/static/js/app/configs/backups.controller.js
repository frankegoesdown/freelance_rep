(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .controller('backupsController', backupsCtrl)

    backupsCtrl.$inject = ['$scope', '$filter'];

    function backupsCtrl($scope, $filter) {


        $scope.chartConfigBase = {
            options: {
                exporting: { enabled: false },
                tooltip: {
                    //shared: true,
                   // headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: 'Занято: {point.y}<br/><br/>'
                },
                chart: {
                    type: 'area',
                    height: 300,
                }
            },
            xAxis: {
                labels: {
                    formatter: function() {
                        var date = new Date(this.value);
                        return $filter('date')(date, 'd MMMM yyyy');
                    },
                },
                type: 'datetime',
                categories: [1452556800000, 1454338638000, 1455029838000, 1455548238000, 
                1455893838000, 1456325838000, 1456930638000, 1457449038000]
            },
            credits: {
                    enabled: false
                },
            yAxis:{
                title: {
                    text: 'Размер диска (GB)'
                },
            },
            series: [{
                name: 'Доступно',
                data: [567, 567, 567, 567, 567, 567, 567, 567]
            },{
                name: 'Использовано',
                data: [0.8122, 4.8122, 6.2999, 7.1100, 11.8042, 13.1452, 17.6504, 18.9992]
            }],
            title:{
                text:''
            },
        };

        $scope.dbDatas = {
            first:{
                backupScheme:
                  [{
                    id: 'by_events',
                    name: 'По количеству событий'
                  },{
                    id: 'by_time',
                    name: 'По времени'
                  },{
                    id: 'by_user',
                    name: 'По желанию пользователя'
                  }],
                backupSchemeSelected: 'by_events',
                period: 5000000,
                pathForBackups: '/var/log/evBase',
                doBackups: true,
                doRemove: true,
                removeDelay: 700,
                backups:{
                    data:[
                        {
                            id:1,
                            size: 0.8122,
                            date: 1452556800000,
                        },{
                            id:2,
                            size: 4.8122,
                            date: 1454338638000,
                        },{
                            id:3,
                            size: 6.2999,
                            date: 1455029838000,
                        },{
                            id:4,
                            size: 7.1100,
                            date: 1455548238000,
                        },{
                            id:5,
                            size: 11.8042,
                            date: 1455893838000,
                        },{
                            id:6,
                            size: 13.1452,
                            date: 1456325838000,
                        },{
                            id:7,
                            size: 17.6504,
                            date: 1456930638000,
                        },{
                            id:8,
                            size: 18.9992,
                            date: 1457449038000,
                        }
                    ].slice().reverse()
                }
            },
            second:{

                backupScheme:
                  [{
                    id: 'by_events',
                    name: 'По количеству событий'
                  },{
                    id: 'by_time',
                    name: 'По времени'
                  },{
                    id: 'by_user',
                    name: 'По желанию пользователя'
                  }],
                backupSchemeSelected: 'by_time',
                period: 120,
                pathForBackups: '10.0.1.95:/home/backups/some/',
                doBackups: true,
                backups:{
                    data:[
                        {
                            id:1,
                            size: 0.8122,
                            date: 1452556800000,
                        },{
                            id:2,
                            size: 4.8122,
                            date: 1454338638000,
                        },{
                            id:3,
                            size: 18.2999,
                            date: 1455029838000,
                        },{
                            id:4,
                            size: 39.1100,
                            date: 1455548238000,
                        },{
                            id:5,
                            size: 55.8042,
                            date: 1455893838000,
                        },{
                            id:6,
                            size: 69.1452,
                            date: 1456325838000,
                        },{
                            id:7,
                            size: 94.6504,
                            date: 1456930638000,
                        },{
                            id:8,
                            size: 107.9992,
                            date: 1457449038000,
                        }
                    ]
                }
            },
            third:{
                backupScheme:
                  [{
                    id: 'by_events',
                    name: 'По количеству событий'
                  },{
                    id: 'by_time',
                    name: 'По времени'
                  },{
                    id: 'by_user',
                    name: 'По желанию пользователя'
                  }],
                backupSchemeSelected: 'by_user',
                period: 120,
                pathForBackups: '10.0.1.99:/home/backups/some/',
                doBackups: false,
                backups:{
                    data:[
                        {
                            id:1,
                            size: 0.8122,
                            date: 1452556800000,
                        },{
                            id:2,
                            size: 4.8122,
                            date: 1454338638000,
                        },{
                            id:3,
                            size: 94.6504,
                            date: 1456930638000,
                        },{
                            id:4,
                            size: 107.9992,
                            date: 1457449038000,
                        }
                    ]
                }}
        }

        $scope.doNothing = function(id){
            return;
        }

        $scope.switchThis = function(destination, newValue){
            $scope.dbDatas[destination].backupSchemeSelected = newValue.id;
        }

    }
})();

