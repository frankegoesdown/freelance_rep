(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .factory('RotationsContent', rotationsFactory);

    rotationsFactory.$inject = ['$http', '$filter', 'Notification', '$q', 'statuses'];

    function rotationsFactory($http, $filter, Notification, $q, statuses) {
        //main service constructor function
        var SearchContent = function () {
            this.message = '';
            this.busy = false;
            this.has_error = false;
            this.page = 1;
            this.dir = '';
            this.reverse = false;
            this.order = 'desc';
            this.order_by = 'id';
            this.rule = undefined;
            this.data = [];
            this.config = {
                ttl_readonly: undefined,
                ttl_archive: undefined
            };
        };

        //get data to main alerts table
        SearchContent.prototype.loadAll = function () {
            var deferred = $q.defer();
            if (this.message === '') {
                this.message = 'Загрузка...';
            }
            this.busy = true;
            $http.get('/config/rotations/all')
                .success(function (data) {
                    this.data = data;
                    deferred.resolve(data);
                    if (data.total === 0) {
                        this.message = 'По данному запросу ничего не найдено';
                        return;
                    }
                    this.message = 'Всего найдено:  ';
                    this.has_error = false;
                    this.busy = false;
                }.bind(this))
                .error(function (data) {
                    Notification.error("Ошибка получения данных с сервера.");
                    this.busy = false;
                    this.has_error = true;
                }.bind(this))
            return deferred.promise;
        }

        //get data to main alerts table
        SearchContent.prototype.loadConfig = function () {
            return $http.get('/config/rotations/cfg')
                .success(function (data) {
                    this.config.ttl_readonly = +data.ttl_readonly;
                    this.config.ttl_archive = +data.ttl_archive;
                }.bind(this))
                .error(function (data) {
                }.bind(this))
        }

        //get data to main alerts table
        SearchContent.prototype.saveConfig = function (data) {
            return $http.post('/config/rotations/cfg', angular.toJson(data))
                .success(function (data) {
                }.bind(this))
                .error(function (data) {
                }.bind(this))
        }

        SearchContent.prototype.getCheckedIds = function () {
            var choised_states = [];
            for (var i = 0; i < this.data.length; i++) {
                if (this.data[i].ch)
                    choised_states.push(this.data[i].id)
            }
            return choised_states
        }

        SearchContent.prototype.setComment = function (data) {
            return $http.post('/config/rotations/comment', angular.toJson(data))
                .success(function (data) {
                }.bind(this))
                .error(function (data) {
                }.bind(this))
        }

        SearchContent.prototype.sendUpdate = function (action, ids) {
            var deferred = $q.defer(),
                url = '/config/rotations';
            if (action === 'calculate') {
                url = '/config/rotations/calc'
            }
            $http.post(url, {to_state: action, id_list: ids})
                .success(function (data, status) {
                    deferred.resolve({
                        status: action,
                        id: ids,
                        success: data.success,
                        data: data
                    });
                }.bind(this))
                .error(function (data) {
                }.bind(this))
            return deferred.promise;
        }

        SearchContent.prototype.archieve = function () {
            return this.sendUpdate('archieve', this.getCheckedIds());
        }

        SearchContent.prototype.rm = function () {
            return this.sendUpdate('remove', this.getCheckedIds());
        }

        SearchContent.prototype.calc = function () {
            return this.sendUpdate('calculate', this.getCheckedIds());
        }

        SearchContent.prototype.restore = function () {
            return this.sendUpdate('restore', this.getCheckedIds());
        }

        SearchContent.prototype.save = function () {
            return this.sendUpdate('save', this.getCheckedIds());
        }

        return SearchContent;
    }

})();
