(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .config(uiRouterConfig);


    uiRouterConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

    function uiRouterConfig($stateProvider, $urlRouterProvider, $httpProvider) {
        var labels = {
            backups: 'Резервные копии',
            journals: 'Журналы',
            license: 'Информация о лицензии',
            users_and_roles: 'Пользователи',
            rotations: 'Хранилище событий',
            plugins: 'Плагины',
            plugin_sid: 'SID\'ы плагина с ID {[{plugin_id}]}',
        }

        $stateProvider
            .state('backups', {
                url: '/config/backups',
                templateUrl: '/config/backups',
                ncyBreadcrumb: {
                    label: labels.backups
                },
                controller: 'backupsController'
            })
            .state('journals', {
                url: '/config/journals',
                templateUrl: '/config/journals',
                ncyBreadcrumb: {
                    label: labels.journals
                }
            })
            .state('license', {
                url: '/config/license',
                templateUrl: '/config/license',
                controller: 'licenceController',
                ncyBreadcrumb: {
                    label: labels.license
                },
                resolve: {
                    license: function ($http) {
                        return $http({method: 'GET', url: '/config/license/info'});
                    },
                    performance: function ($http) {
                        return $http({method: 'GET', url: '/config/license/performance'});
                    }
                }
            })
            .state('users-and-roles', {
                url: '/config/users-and-roles',
                templateUrl: '/config/users-and-roles',
                controller: 'UsersAndRolesController',
                ncyBreadcrumb: {
                    label: labels.users_and_roles
                }
            })
            .state('rotations', {
                url: '/config/rotations',
                templateUrl: '/config/rotations',
                controller: 'RotationsController',
                ncyBreadcrumb: {
                    label: labels.rotations
                }
            })
            .state('plugins', {
                url: '/config/plugins',
                templateUrl: '/config/plugins',
                ncyBreadcrumb: {
                    label: labels.plugins
                },
                controller: 'PluginsController',
                resolve: {
                    pluginsData_preloaded: function ($q, PluginsPuller) {
                        var number = 20;
                        var service = new PluginsPuller();
                        var deferred = $q.defer();
                        service
                            .getPage(number, 0)
                            .then(function (data) {
                                deferred.resolve({
                                    data: data,
                                    service: service
                                })
                            });
                        return deferred.promise;
                    }
                }
            })
            .state('plugin_sid', {
                url: '/config/plugins/:pid',
                templateUrl: '/config/plugins/sid',
                ncyBreadcrumb: {
                    label: labels.plugin_sid,
                    parent: 'plugins'
                },
                controller: 'PluginSidController',
                resolve: {
                    sid_data: function ($http, $stateParams) {
                        var id = $stateParams.pid || '';
                        return $http({method: 'GET', url: '/config/plugins/' + id});
                    },
                    plugin_id: function ($stateParams) {
                        var id = $stateParams.pid || '';
                        return id;
                    }
                }
            });


    }


})();

