(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .factory('PluginsPuller', pluginsGetterFactory);

    pluginsGetterFactory.$inject = ['$filter', '$q', 'Notification', 'Restangular'];

    function pluginsGetterFactory($filter, $q, Notification, Restangular) {
        //main service constructor function
        var SearchContent = function () {
            this.page = 0;
            this.order = 'desc';
            this.order_by = 'id';
            this.number = 20;
            this.data = undefined;
            this.numberOfPages = 0;
            this.term = undefined;
            this.restang = Restangular.all('config/plugins');
        };


        var deffededPages,
            storedData,
            sortPhrase;

        //get data to main alerts table
        SearchContent.prototype.getPage = function (number, page) {

            var vm = this,
                sliceStart = number * page,
                sliceEnd = number * (page + 1),
                reverse = false;

            if (this.order === 'desc') {
                reverse = true;
            }
            if(number) {
                vm.number = number;
            }
            if (page || page === 0) {
                this.page = page;
            }

            if (storedData) {
                deffededPages.resolve({
                    data: vm.data,
                    numberOfPages: vm.numberOfPages,
                    total: storedData.length
                });
            }

            function successGetPage(data) {
                storedData = data.response;
                if (vm.term) {
                    storedData = $filter('filter')(storedData, vm.term);
                }
                if (vm.order_by) {
                    storedData = $filter('orderBy')(storedData, vm.order_by, reverse);
                }

                vm.data = storedData.slice(sliceStart, sliceEnd);
                vm.numberOfPages = Math.ceil(storedData.length / vm.number);
                for (var i = 0; i < vm.data.lenght; i++) {
                    vm.data[i].is_selected = false;
                }
                
                deffededPages.resolve({
                    data: vm.data,
                    numberOfPages: vm.numberOfPages,
                    total: storedData.length
                });
            }

            function errorGetPage(err) {
                if (err.status === -1) {
                    return;
                }
                Notification.error("Ошибка получения данных с сервера.");
            }

            if (deffededPages) deffededPages.resolve();

            deffededPages = $q.defer();

            this.restang
                .one('all')
                .withHttpConfig({timeout: deffededPages.promise})
                .get()
                .then(successGetPage, errorGetPage);

            return deffededPages.promise;

        };
        
        SearchContent.prototype.setFilter = function (term) {
            this.term = undefined;
            this.page = 0;

            if (!storedData) {
                deferred.resolve();
            }

            if (term) {
                this.term = term;
            }

            return this.getPage(this.number, this.page);

        };

        SearchContent.prototype.setSort = function (number, propertyName, order, page) {
            this.number = number;
            this.page = page;

            if (!storedData) {
                deferred.resolve();
            }
            this.order_by = propertyName;
            this.order = order;

            return this.getPage(this.number, this.page);

        };

        SearchContent.prototype.setStateByIds = function (ids, state) {

            var update_promise = $q.defer();

            this.restang
                .withHttpConfig({timeout: update_promise.promise})
                .post({
                    plugins: ids,
                    state: state
                })
                .then(function (data) {
                    update_promise.resolve(data.response);
                });

            return update_promise.promise;
        };

        SearchContent.prototype.getAllPlugins = function () {
            function successGet(data) {
                this.data = data.response;
            }

            function errorGet(err) {
                return;
            }
            this.restang
                .one('all')
                .get()
                .then(successGet.bind(this), errorGet.bind(this));

        }

        return SearchContent;
    }

})();
