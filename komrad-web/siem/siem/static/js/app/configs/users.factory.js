/**
 * data loaders for users and roles for smart table extension
 */
(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .factory('UsersModel', userFactory);

    userFactory.$inject = ['$http', 'Notification', 'RolesModel'];

    function userFactory($http, Notification, RolesModel) {
        var roles = RolesModel();

        var users = undefined;
        var userFactory = function(qb) {
            this.items = [];
            this.diplayData = [];
            this.total = 0;
        };

        /**
         * return discrepancy of user access map and role access map
         * @param access_map
         */
        userFactory.prototype.discAccessMap =function (statement){
            var role = roles.getRoleById(statement.role);
            var to_update = {};
            for (var module in role.access_map){
                if (statement.access_map[module] != role.access_map[module])
                    to_update[module] = statement.access_map[module];
            }
            statement.access_map = to_update;
        };

        userFactory.prototype.amplifyAccessMap = function (statement){
            var role = roles.getRoleById(statement.role);
            var amplified = angular.copy(role.access_map)
            for (var module in statement.access_map){
                amplified[module] = statement.access_map[module];
            }
            return amplified;
        };


        userFactory.prototype.get = function() {
            $http.get('/config/users/all').success(function(data) {
                this.items = data.response;
                this.diplayData = data.response;
                this.total = this.items.length;
            }.bind(this))
            .error(function(data) {
                Notification.error("Ошибка получения данных с сервера.");
            }.bind(this))
        };

        function makeFunc(ob, user_data) {
            var vm = ob;
            var user = user_data;
            function onComplite(response) {
                user.image_id = response.data.id;
                return vm.save(user);
            }
            return onComplite;
        }

        userFactory.prototype.add = function(user_data) {
            if (user_data.img_fd){
                var promise_img = this.saveImage(user_data.img_fd);
                var fuck = makeFunc(this, user_data);
                return promise_img.then(fuck);
            }
            return this.save(user_data);
        };

        userFactory.prototype.saveImage = function(fd) {
            return $http.post('/file', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function(data, status) {
                })
            .error(function() {
                });
        };

        userFactory.prototype.save = function(user_data) {
            return $http.post('/config/user', angular.toJson(user_data))
                .success(function(data, status) {
                    this.get();
                }.bind(this)).error(function() {
                    Notification.error('Ошибка записи данных');
                })

        }

        userFactory.prototype.disable = function(id) {
            return $http.delete('/config/user/' + id)
                .success(function(data, status) {
                    this.get();
                }.bind(this)).error(function() {
                })
        };

        userFactory.prototype.restore = function(id) {
            return $http.post('/config/user/restore', {id: id})
                .success(function(data, status) {
                    this.get();
                }.bind(this)).error(function() {
                })
        };

        userFactory.prototype.update = function(user_data) {
            if (user_data.access_map)
                this.discAccessMap(user_data);
            return $http.put('/config/user', angular.toJson(user_data))
                .success(function(data, status) {
                    this.get();
                }.bind(this)).error(function() {
                    Notification.error('Ошибка записи данных');
                })
        };
        function getUsers(){
            if (!users)
                users = new userFactory();
            return users;
        }

        return getUsers;
    }

})();