(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .directive('uniqUsername', usernameVal);

    usernameVal.$inject = ['$filter', 'UsersModel'];

    // widgetConfig directive duration and update period
    function usernameVal($filter, UsersModel) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function(username) {
                    var userlist = UsersModel().items;
                    var filtered = $filter('filter')(userlist, {username: username}, true);
                    if (filtered.length >= 1) {
                        ctrl.$setValidity('duplicate', false);
                        return undefined;
                    } else {
                        ctrl.$setValidity('duplicate', true);
                        return username;
                    }
                });
            }
        };

    }

})();


