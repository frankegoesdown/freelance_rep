/**
 * requester for groups
 * @return {[type]} [description]
 */
(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .factory('GroupsModel', GroupFactory);

    GroupFactory.$inject = ['$http', '$filter', 'Notification'];

    function GroupFactory($http, $filter, Notification) {
        var GroupFactory = function(qb) {
            this.data = [];
            this.diplayData = [];
            this.total = 0;
            this.loaded = false;
            this.busy = false;
        };

        GroupFactory.prototype.get = function() {

            if (this.busy) return;
            this.busy = true;

            $http.get('/config/group/all').success(function(data) {
                this.items = data.response;
                this.diplayData = data.response;
                this.total = this.items.length;
                this.loaded = true;
                this.busy = false;
            }.bind(this))
            .error(function(data) {
                Notification.error("Ошибка получения данных с сервера.");
                this.busy = false;
            }.bind(this))
        };

        GroupFactory.prototype.getById = function(id){
            if (!this.loaded){
                return;
            }
            var gr = $filter('getByField')(this.items, id, 'id');
            return gr;
        }

        GroupFactory.prototype.add = function(user_data) {
            return $http.post('/config/group', angular.toJson(user_data))
                .success(function(data, status) {
                    this.get();
                }.bind(this))
                .error(function(data) {
                })
        };

        GroupFactory.prototype.remove = function(id) {
            return $http.delete('/config/group/' + id)
                .success(function(data, status) {
                    this.get();
                }.bind(this)).error(function() {
                })
        };

        GroupFactory.prototype.update = function(user_data) {
            return $http.put('/config/group', angular.toJson(user_data))
                .success(function(data, status) {
                    this.get();
                }.bind(this))
                .error(function(data, status) {
                    //Notification.error('');
                })
        };

        GroupFactory.prototype.restore = function(user_data) {
            user_data.restore = true;
            return this.add(user_data)
        };

        return GroupFactory;
    }

})();