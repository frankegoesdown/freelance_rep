(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .controller('GroupsCntroller', groupModalCtrl);

    groupModalCtrl.$inject = ['$scope', '$uibModalInstance', 'groupData', 'control_service'];



    function groupModalCtrl($scope, $uibModalInstance, groupData, control_service) {
        // default values
        $scope.showRestoring = false;
        $scope.data = function(){
            if (groupData){
                return {
                    id: groupData.id,
                    name: groupData.name,
                    desc: groupData.desc
                };
            }
            return {
                name: undefined,
                desc: undefined
            };
        }();


        $scope.ok = function (data) {
            if (!this.form.$valid){
                this.form.name.$dirty = true;
                return;
            }
            if (angular.equals($scope.old_data, data)){
                $uibModalInstance.dismiss('cancel');
            }
            if (groupData) {
                control_service
                    .update(data)
                    .success(function (data) {
                        $uibModalInstance.close();
                    })
                    .error(function (data) {
                        if (data.err_msg === "duplicate")
                            $scope.showRestoring = true;
                    })
            }
            else {
                control_service
                    .add(data)
                    .success(function (data) {
                        $uibModalInstance.close();
                    })
                    .error(function (data) {
                        if (data.err_msg === "duplicate")
                            $scope.showRestoring = true;
                    })
            }
        };

        $scope.restore = function (data) {
            if (!this.form.$valid){
                this.form.name.$dirty = true;
                return;
            }
            control_service
                .restore(data)
                .success(function(data){
                     $uibModalInstance.close();
                })
                .error(function(data){
                     //$uibModalInstance.close();
                })
        };


        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();