(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .controller('licenceController', licenceCtrl);

    licenceCtrl.$inject = ['$scope', 'license', 'performance'];

    function licenceCtrl($scope, license, performance) {
        $scope.license = {};
        $scope.license.number = license.data.response.license_number[0];
        $scope.license.owner = license.data.response.owner[0];
        $scope.license.expire = license.data.response.expire_time[0];
        if (performance.data.response.eps_limit) {
            $scope.license.eps = performance.data.response.eps_limit[0] + ' EPS';
        } else {
            $scope.license.eps = performance.data.response.license_key;
        }
    }

})();