(function() {
    'use strict';

    angular.module('KomradeApp.configs', [
        'KomradeApp.core',
        'ngAnimate',
        'highcharts-ng',
        'ncy-angular-breadcrumb',
        'ui.bootstrap',
        'ui-notification',
        'angularMoment',
        'validation.match',
        'xeditable',
        'ui.uploader',
        'uiCropper',
        'selectize',
        'restangular',
        'smart-table'
    ])
    .constant('accessLvls',
        [{
            id: 0,
            name: 'нет доступа'
        },{
            id: 1,
            name: 'чтение'
        },{
            id: 2,
            name: 'запись'
        }])
    .constant('pluginStatuses', [{
            id: '0',
            name: 'Выключен',
            props: 'badge badge-danger'
        }, {
            id: '1',
            name: 'Включен',
            props: 'badge badge-success'
        }])
    .constant('appModulesHierarchy', function() {
        var default_lvls = 2; // availdable lvls for submods
        var modules = [
            {
                id: 'WIDGETS',
                name: 'Виджеты',
                submodules: [
                    {
                        id: 'PAGE_WIDGETS',
                        name: 'Виджеты'
                    }
                ]
            }, {
                id: 'REALTIME',
                name: 'События в реальном времени',
                submodules: [
                    {
                        id: 'PAGE_REALTIME',
                        name: 'События в реальном времени',
                        lvls: 1
                    }
                ]
            }, {
                id: 'RESOURCES',
                name: 'Активы',
                submodules: [
                    {
                        id: 'PAGE_RESOURCES_CONTROL',
                        name: 'Управление активами',
                    }
                ]
            }, {
                id: 'EVENT_LISTENER',
                name: 'События безопасности',
                submodules: [
                    {
                        id: 'PAGE_SEARCH',
                        name: 'Поиск по событиям',
                    }, {
                        id: 'PAGE_QUERIES',
                        name: 'Все запросы',
                    }
                ]
            }, {
                id: 'CORRESPONDENCIES',
                name: 'Контроль соотвествия',
                submodules: [{
                    id: 'PAGE_CORRESPONDENCIES',
                    name: 'ГОСТ Р ИСО/МЭК 27001-2006'
                }]
            }, {
                id: 'CORRELATION',
                name: 'Корреляция',
                submodules: [{
                    id: 'PAGE_DIR_CONSTRUCTOR',
                    name: 'Конструктор директив',
                }, {
                    id: 'PAGE_INCIDENTS',
                    name: 'Инциденты',
                }]
            }, {
                id: 'ANALITICS',
                name: 'Аналитика',
                submodules: [{
                    id: 'PAGE_EVENTVISIO',
                    name: 'Визуализатор событий',
                }, {
                    id: 'PAGE_FACTS_BASE',
                    name: 'База фактов',
                }]
            }, {
                id: 'AVAILABILITY',
                name: 'Мониторинг доступности',
                submodules: [{
                    id: 'PAGE_AVAILABILITY',
                    name: 'Доступность',
                }, {
                    id: 'PAGE_MAP',
                    name: 'Карта',
                }]
            }, {
                id: 'CONFIGS',
                name: 'Администрирование',
                submodules: [
                {
                    id: 'PAGE_LICENSE',
                    name: 'Информация о лицензии',
                    lvls: 1
                }, {
                    id: 'PAGE_PLUGINS',
                    name: 'Плагины'
                }, {
                    id: 'PAGE_ROTATIONS',
                    name: 'Хранилище событий',
                }, {
                    id: 'PAGE_USERS',
                    name: 'Пользователи',
                }]
            }];

        // add submodules levels if it wasn`t defined
        for (var mod_id in modules){
            for (var submod_id in modules[mod_id].submodules) {
                if (!modules[mod_id].submodules[submod_id].lvls)
                    modules[mod_id].submodules[submod_id].lvls = default_lvls;
            }
        }
        return modules;
    }());


})();