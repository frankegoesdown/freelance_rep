(function () {
    'use strict';

    angular
        .module('KomradeApp.av_monitoring')
        .config(uiRouterConfig);


    uiRouterConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

    function uiRouterConfig($stateProvider, $urlRouterProvider, $httpProvider) {
        var labels = {
            map: 'Карта',
            availability: 'Доступность',
            nagset: 'Управление активами'
        }

        $stateProvider
            .state('map', {
                url:'/avmonitor/map',
                templateUrl: '/nagvis_frame',
                ncyBreadcrumb: {
                    label: labels.map,
                }
            })
            .state('availability', {
                url:'/avmonitor/availability',
                templateUrl: '/cgi-bin/nagios3_frame',
                ncyBreadcrumb: {
                    label: labels.availability,
                }
            })
            .state('nagset', {
                url:'/avmonitor/settings',
                templateUrl: '/avmonitor/settings',
                ncyBreadcrumb: {
                    label: labels.nagset,
                },
                controller: 'nagiosSettings'
            });


    }



})();

