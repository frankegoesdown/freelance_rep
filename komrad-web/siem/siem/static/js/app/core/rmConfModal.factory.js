(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .factory('rmConfModal', removeConfirmation);

    removeConfirmation.$inject = ['$uibModal'];

    function removeConfirmation($uibModal) {

        var openRemoveModal = function(header, body, success_callback, error_callback) {
            var removeModalInstance = $uibModal.open({
                templateUrl: 'removeConfirmation.html',
                controller: function ($scope, $uibModalInstance, $sce){
                    $scope.msg = {
                        header: header,
                        body: $sce.trustAsHtml(body)
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }

                    $scope.remove = function () {
                        $uibModalInstance.close();
                    }

                },
                windowClass: "app-modal-window",
                size: 'sm'
            });

            removeModalInstance.result.then(
                success_callback,
                error_callback
            );
        }
        return {
            remove: openRemoveModal
        }
    }

})();

