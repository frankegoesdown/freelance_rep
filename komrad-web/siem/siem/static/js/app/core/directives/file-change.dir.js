(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .directive('fileChange', fileChange);

    fileChange.$inject = ['$parse']

    // widgetConfig directive duration and update period
    function fileChange($parse) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function ($scope, element, attrs, ngModel) {
                var attrHandler = $parse(attrs.fileChange);
                var handler = function (e) {
                    $scope.$apply(function () {
                        attrHandler($scope, {$event: e, files: e.target.files});
                    });
                };
                element[0].addEventListener('change', handler, false);
            }
        }
    }

})();