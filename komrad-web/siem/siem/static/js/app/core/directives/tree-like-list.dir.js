(function () {
    'use strict';

    angular
        .module('treeView', ['ivh.treeview', 'fansyCheckboxes'])
        .directive('treeView', treeView);

    // widgetConfig directive duration and update period
    function treeView($filter) {
        var template = '<div class="row" style="margin-top: 10px">' +
            '<div class="col-lg-12" style="padding-left: 25px; padding-right: 25px;">' +
            '<input type="text" class="form-control search-field-input" placeholder="Поиск..."' +
            'ng-model="self.searchInFields" ng-change="self.changeFilterInput()">' +
            '</div>' +
            '</div>' +
            '<div class="small-box" style="margin-top: 10px; padding-top: 0px; padding-bottom: 0px;">' +
            '<div ivh-treeview="self.fields" ivh-treeview-use-checkboxes="true"' +
            'ivh-treeview-on-toggle="self.setLastToggle(ivhNode, ivhTree)" ivh-treeview-node-tpl="self.tpl" ' +
            'ivh-treeview-filter="self.searchInFields" class="tree-wrapper">' +
            '</div>' +
            '</div>';
        return {

            // шаблон задан как ссылка или выражение

            template: template,

            // применение директивы ограничивается

            restrict: 'E',
            scope: {
                fields: '=',
                headers: '='
            },
            // контроллер для директивы

            controller: treeViewCtrl,
            controllerAs: 'self',
            bindToController: true, // because the scope is isolated
            // прописана основная функциональность директивы

            link: function postLink(scope, element, attrs) {


            }

        };

    }

    function treeViewCtrl($scope, $element, $filter, $q, ivhTreeviewMgr, ivhTreeviewOptions) {

        var self = this;

        self.tpl = [
            '<div style="position: relative" ' +
            'ng-class="(node.hasFiltered || node.withoutFilter)?\'filteredChild\':\'ng-hide\'">',
            '<span ivh-treeview-toggle>',
            '<span ivh-treeview-twistie></span>',
            '</span>',
            '<span class="ivh-treeview-node-label" ivh-treeview-toggle>',
            '<span class="tree-field-node-label">{[{trvw.label(node)}]}</span>',
            '<fansy-checkboxes></fansy-checkboxes>',
            '</span>',
            '<div ivh-treeview-children></div>',
            '</div>'
        ].join('\n');


        self.fields.forEach(function (item) {
            item.withoutFilter = true;
            if (item.children) {
                item.children.forEach(function (child) {
                    child.withoutFilter = true;
                });
            }
        })

        self.fields[0].__ivhTreeviewExpanded = true;

        var searchInput = angular.element(document.querySelector('.search-field-input'));
        self.changeFilterInput = function (e) {
            var target = searchInput.val().toLocaleLowerCase();
            if (!searchInput.val().toLocaleLowerCase()) {
                self.fields.forEach(function (item) {
                    item.filtered = false;
                    item.__ivhTreeviewExpanded = false;
                    if (item.children) {
                        item.children.forEach(function (child) {
                            child.withoutFilter = true;
                            if (child.selected === true) {
                                item.__ivhTreeviewExpanded = true;
                            }
                        })
                    }

                });
                return;
            }
            self.fields.forEach(function (item) {
                var parentLabel = item.label.toLocaleLowerCase();
                item.filtered = true;
                if (item.children) {
                    item.children.forEach(function (child) {
                        child.withoutFilter = true;
                        if (child.hasFiltered) {
                            delete child.hasFiltered;
                        }
                        var childrenLabel = child.label.toLocaleLowerCase();
                        if (!(childrenLabel.indexOf(target) != -1) || (parentLabel.indexOf(target) != -1)) {
                            item.__ivhTreeviewExpanded = true;
                            delete child.withoutFilter;
                        }
                        if ((parentLabel.indexOf(target) != -1)) {
                            child.hasFiltered = true
                            ivhTreeviewMgr.expand(self.fields, self.fields[self.fields.indexOf(item)]);
                        }
                    })
                }

            });
        };

        self.headers.forEach(function (item) {
            if (item.id === 'event_id' ||
                item.id === 'data') {
                item.display = true;
            }
        });


        function checkEmpty(headers) {
            var empty;
            for (var i = 0; i < headers.length; i++) {
                if (headers[i].display === true) {
                    empty = false;
                    break;
                }
                empty = true;
            }
            return empty;
        }

        $scope.$on('treeFields.deliver', function (e, data) {
            var empty = false;
            if (!data.length) {
                self.headers.forEach(function (item) {
                    if (item.id == data.name) {
                        item.display = !item.display;
                    }
                });

                empty = checkEmpty(self.headers)
                $scope.$emit('fieldsHeaders.empty', empty);
                return;
            }

            self.headers.forEach(function (item, i) {
                data.forEach(function (dataItem) {
                    if (item.id === dataItem.name && item.display !== dataItem.selected) {
                        item.display = !item.display;
                    }
                });
            });

            empty = checkEmpty(self.headers)
            $scope.$emit('fieldsHeaders.empty', empty);
        });

    }

})();