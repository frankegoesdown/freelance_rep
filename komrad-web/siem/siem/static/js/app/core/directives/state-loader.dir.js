(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .directive('stateLoader', stateLoader);

    function stateLoader() {
        var template = '<div ng-show="self.load" class="state-loader">' +
            '<img src="../../../../../static/img/335.gif">' +
            '</div>';
        return {

            // шаблон задан как ссылка или выражение

            template: template,

            // применение директивы ограничивается
            restrict: 'E',
            scope: {},
            // контроллер для директивы

            controller: stateLoaderCtrl,
            controllerAs: 'self',
            bindToController: true, // because the scope is isolated
            // прописана основная функциональность директивы

            link: function postLink(scope, element, attrs) {

            }

        };

    }

    stateLoaderCtrl.$inject = ['$scope', '$element', '$attrs'];

    function stateLoaderCtrl($scope, $element, $attrs) {
        var self = this;

        $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            self.load = true;
        });
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            self.load = false;
        });
        $scope.$on('loader.start', function (event) {
            self.load = true;
            $scope.$apply()
        });
        $scope.$on('loader.stop', function (event) {
            self.load = false;
            $scope.$apply()
        });

    }

})();