(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .directive('layoutRender', layoutRender);

    layoutRender.$inject = ['$filter', '$window'];
    layoutRenderCtrl.$inject = ['$scope', '$element', '$attrs', '$filter', '$q'];

    // widgetConfig directive duration and update period
    function layoutRender($filter, $window) {
        var dir = this;
        var template = '<div ui-layout="{flow : \'column\', disableToggle: true}" class="maincontainer">' +
                '<div ui-layout-container min-size="20%" max-size="60%" size="25%" class="ui-layout-left">' +
                '<div ng-include="self.left"></div>' +
                '</div>' +
                '<div ui-layout-container min-size="40%" max-size="80%" size="75%" class="ui-layout-center">' +
                '<div ng-include="self.main"></div>' +
                '</div>' +
                '</div>',
            templateMin = '<div class="row">' +
                '<div class="ng-panel-viewport maincontainer">' +
                '<section class="panel-one panel-static" panel-view panel-options="panels.get(\'one\')">' +
                '<div class="scroller">' +
                '<div class="ui-layout-center">' +
                '<div ng-include="self.main"></div>' +
                '</div>' +
                '</div>' +
                '</section>' +
                '<section class="panel-two" panel-view panel-options="panels.get(\'two\')">' +
                '<button class="btn btn-default slide-btn" ng-click="openPanelTwo()">' +
                '<span class="fa  fa-angle-double-left"></span>' +
                '</button>' +
                '<div class="scroller">' +
                '<div class="ui-layout-left" style="position: absolute; top:0; right: 0; left: 0; padding-right: 0; padding-left: 0">' +
                '<div ng-include="self.left"></div>' +
                '</div>' +
                '</div>' +
                '</section>' +
                '</div>' +
                '</div>';
        return {

            // шаблон задан как ссылка или выражение

            template: function () {
                var winWidth = $window.screen.availWidth;
                if (winWidth < 640) {
                    return templateMin
                }
                return template
            },

            // применение директивы ограничивается

            restrict: 'E',
            // контроллер для директивы

            controller: layoutRenderCtrl,
            controllerAs: 'self',
            bindToController: true, // because the scope is isolated
            // прописана основная функциональность директивы

            link: function postLink(scope, element, attrs) {
                
            }

        };

    }

    function layoutRenderCtrl($scope, $element, $attrs, $filter, $q) {

        var self = this;
        self.left = $attrs.left;
        self.main = $attrs.main;

    }

})();