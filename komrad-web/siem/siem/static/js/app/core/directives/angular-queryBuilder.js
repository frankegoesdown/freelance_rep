(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .directive('queryBuilder', queryBuilder);

    // widgetConfig directive duration and update period
    function queryBuilder($filter) {
        var template = '<div class="query-builder"></div>';
        return {

            // шаблон задан как ссылка или выражение

            template: template,

            // применение директивы ограничивается

            restrict: 'E',
            scope: {
                filters: '=',
                setter: '=',
                plugins: '=',
                lang: '='
            },
            // контроллер для директивы

            controller: builderCtrl,
            controllerAs: 'self',
            bindToController: true, // because the scope is isolated
            // прописана основная функциональность директивы

            link: function postLink(scope, element, attrs) {

                var builderOptions = {};
                var builder = element.find('.query-builder');
                builderOptions.filters = scope.self.filters;
                builderOptions.lang_code = scope.self.lang;
                builderOptions.plugins = scope.self.plugins;
                if(scope.self.filters.length){
                    builder.queryBuilder(builderOptions);
                }

                scope.$on('filtersInited', function (e, data) {
                    builderOptions.filters = data;
                    builder.queryBuilder(builderOptions);
                    if(!$filter('isEmpty')(scope.self.setter) ||
                        !scope.self.setter === undefined){
                        builder.queryBuilder('setRules', scope.self.setter)
                    }
                })

            }

        };

    }

    function builderCtrl($scope, $element, $filter, $q) {

        var self = this;

        if(typeof self.filters === "object" && self.filters !== null){
            self.filters.then(function (result) {
                $scope.$broadcast('filtersInited', result)
                $scope.$emit('qb.inited')

            });
        }

        var builder = $element.find('.query-builder');

        $scope.$on('qb.getRules', function (e, param) {
            if(param){
                self.rules = builder.queryBuilder('getRules');
                $scope.$emit('qb.saveQuery', self.rules);
                return;
            }

            self.rules = builder.queryBuilder('getRules');
            $scope.$emit('resolveGetRules', self.rules)
        });

        $scope.$on('qb.setFirstRule', function (e, data) {
            if ($filter('isEmpty')(self.rules)) {
                builder.queryBuilder('setRules', data);
                $scope.$emit('qb.firstFilterSet', data);
                return;
            }
            self.rules = builder.queryBuilder('getRules');
            self.rules.rules.forEach(function (item) {
                if(item.id !== 'directive_id'){
                    data.rules.push(item);
                    return;
                }
            });
            builder.queryBuilder('setRules', data);
            $scope.$emit('qb.firstFilterSet', data);
        });

        $scope.$on('qb.setRules', function (e, data) {
            if ($filter('isEmpty')(data)) {
                return;
            }
            builder.queryBuilder('setRules', data);
        });

        $scope.$on('qb.reset', function () {
            builder.queryBuilder('reset');
        });

    }

})();