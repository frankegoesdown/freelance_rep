(function () {
    'use strict';

    angular
        .module('fansyCheckboxes', [])
        .directive('fansyCheckboxes', fansyCheckboxes);

    // widgetConfig directive duration and update period
    function fansyCheckboxes(ivhTreeviewMgr, $timeout, $rootScope) {
        var dir = this;
        var template = [
            '<span class="" style="position: absolute; right: 0; top:3px; z-index: 1000">',
            '<span ng-show="node.selected && !node.filtered" class="x"><i class="fa fa-check-square" style="color:#63A8EB; font-size: 20px"></i></span>',
            '<span ng-hide="node.selected || node.filtered"><i class="fa fa-square-o" style="color:#E7EBEC; font-size: 20px"></i></span>',
            '</span>'
        ].join('');
        return {

            // шаблон задан как ссылка или выражение

            template: template,

            // применение директивы ограничивается

            restrict: 'E',
            // контроллер для директивы

            controller: layoutRenderCtrl,
            controllerAs: 'self',
            bindToController: true, // because the scope is isolated
            // прописана основная функциональность директивы

            link: function postLink(scope, element) {
                scope.$on('$destroy', function () {
                    ivhTreeviewMgr.deselectAll(scope.self.fields);
                    scope.self.fields[0].children[0].selected = true;
                    scope.self.fields[0].children[3].selected = true;
                });

                scope.self.fields[0].children[0].selected = true;
                scope.self.fields[0].children[3].selected = true;

                element.bind('click', function (e) {
                    $rootScope.$broadcast('loader.start');
                    if (e.stopPropagation) e.stopPropagation();
                    else e.cancelBubble = true;
                    scope.node.selected = !scope.node.selected;
                    if (scope.node.children) {
                        scope.node.children.forEach(function (item) {
                            if (scope.node.selected && !item.selected) {
                                item.selected = !item.selected;
                            }
                            if (!scope.node.selected) {
                                item.selected = !item.selected;
                            }
                        });

                        scope.$emit('treeFields.deliver', scope.node.children);
                        scope.$evalAsync()
                        $timeout(function () {
                            $rootScope.$broadcast('loader.stop');
                        }, 0)
                        return;
                    }
                    scope.$emit('treeFields.deliver', scope.node);
                    scope.$evalAsync()
                    $timeout(function () {
                        $rootScope.$broadcast('loader.stop');
                    }, 0)
                });
            }

        };

    }

    function layoutRenderCtrl($rootScope) {
        var self = this;
        self.fields = $rootScope.testLeftFields;

    }

})();