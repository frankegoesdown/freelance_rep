(function () {
    'use strict';

    angular
        .module('KomradeApp.core', [
            'ui.router',
            'ui-notification',
            'highcharts-ng',
            'agGrid',
            'btford.socket-io',
            'ivh.treeview',
            'xeditable'])
        //'ngScrollbars'])
        .config(setInterpolateSymbols)
        .config(configProvide)
        .config(configNotifications);
    //.config(configScrollBar);
    // .config(['$provide', configureTemplateFactory]);


    function setInterpolateSymbols($interpolateProvider) {
        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');
    }

    function configProvide($provide) {
        $provide.decorator('$state', function ($delegate, $stateParams) {
            $delegate.forceReload = function () {
                return $delegate.go($delegate.current, $stateParams, {
                    reload: true,
                    inherit: false,
                    notify: true
                });
            };
            return $delegate;
        });
    }

    function configNotifications(NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 1000,
            startTop: 60,
            startRight: 10,
            verticalSpacing: 10,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'top'
        });
    }


})();

