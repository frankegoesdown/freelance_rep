(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .run(setHighchartsRuLocale)
        .run(setAncjorScrollOffset)
        .run(ololo)
        .run(preloadUserInfo)
        .run(xeditableCofnfig);

    setAncjorScrollOffset.$inject = ['$anchorScroll'];
    ololo.$inject = ['$rootScope', '$state', '$window', '$templateCache'];
    preloadUserInfo.$inject = ['me'];
    xeditableCofnfig.$inject = ['editableOptions', 'editableThemes'];

    function preloadUserInfo(me) {
        me.get();
    };

    function xeditableCofnfig(editableOptions, editableThemes) {
        editableThemes.bs3.inputClass = 'input-sm';
        editableThemes.bs3.buttonsClass = 'btn-sm';
        editableOptions.theme = 'bs3';
    };

    function setHighchartsRuLocale() {
        Highcharts.setOptions({
            lang: {
                loading: 'Загрузка...',
                months: [
                    'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август',
                    'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
                ],
                weekdays: [
                    'Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг',
                    'Пятница', 'Суббота'
                ],
                shortMonths: [
                    'Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг',
                    'Сент', 'Окт', 'Нояб', 'Дек'
                ],
                exportButtonTitle: "Экспорт",
                printButtonTitle: "Печать",
                rangeSelectorFrom: "С",
                rangeSelectorTo: "По",
                rangeSelectorZoom: "Период",
                downloadPNG: 'Скачать PNG',
                downloadJPEG: 'Скачать JPEG',
                downloadPDF: 'Скачать PDF',
                downloadSVG: 'Скачать SVG',
                printChart: 'Напечатать график',
                resetZoom: 'Сбросить'
            },
            global: {
                useUTC: false
            }
        });
    };

    function setAncjorScrollOffset($anchorScroll) {
        $anchorScroll.yOffset = 65;   // always scroll by 65 extra pixels
    };


    function ololo($rootScope, $state, $window, $templateCache) {

        $rootScope.$on('$stateChangeSuccess', function (event, to, toParams, from, fromParams) {
            $rootScope.$previousState = from;
        });
    }

    
})();
