(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .config(uiRouterConfig);


    uiRouterConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

    function uiRouterConfig($stateProvider, $urlRouterProvider, $httpProvider) {

        $httpProvider.interceptors.push(['$q', '$injector', function($q, $injector) {
            return {
                'request': function(config) {
                    return config;
                },

                // optional method
                'requestError': function(rejection) {
                    return $q.reject(rejection);
                },
                // optional method
                'response': function(response) {
                    return response;
                },

                // optional method
                'responseError': function(rejection) {
                    if (rejection.status === 401) {
                        window.location = "/login";
                        return;
                    }
                    if (rejection.status === 403) {
                        // see next:
                        // http://stackoverflow.com/questions/20230691/injecting-state-ui-router-into-http-interceptor-causes-circular-dependency
                        $injector.get('$state').transitionTo('403_default_error');
                        return;
                    }
                    return $q.reject(rejection);
                }
            }
        }]);

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('logout', {
                url: '/logout',
                controller: 'logoutController'
            })
            .state('403_default_error', {
                templateUrl: '403_default_error.html'
            });


    }



})();

