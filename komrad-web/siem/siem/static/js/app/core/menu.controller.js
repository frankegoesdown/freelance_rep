(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .controller('menuController', menuNavigator)

    menuNavigator.$inject = ['$scope', '$state', '$rootScope', '$http', '$window',
        '$templateCache', 'Notification', 'me'];

    function menuNavigator($scope, $state, $rootScope, $http, $window, $templateCache,
                           Notification, me) {
        $scope.state = $state;
        $scope.my_username = '';
        me.get().then(function(data){
            console.log(data);
            $scope.my_username = data.username;
        });
        // var myBlockUI = blockUI.instances.get('workspace');

        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams, options) {
                // myBlockUI.start({'size': '64px'});
            }
        )

        $scope.activeLinkShotdown = function () {
            // remove listeners
            var link = angular.element(document.querySelector('.docs'));
            link.blur();
        };

        $rootScope.$on('$stateChangeSuccess', function (event) {
            // myBlockUI.stop();
            ;
        });

        $rootScope.$on('$destroy', function (event) {
            removeListener();
        });
        // $scope.$on('$viewContentLoaded', function() {
        //     forwardListeners();
        // });

        $scope.logout = function () {
            $http.post('/logout')
                .success(function (data, status) {
                    $templateCache.removeAll();
                    $window.location = '/';
                    $window.location.reload();
                    //$window.location.reload();
                })
        }

        function goHome() {
            var loc = location,
                home = loc.host + '/index';
            location.assign(home);
        }

        $scope.renderLayout = function () {
            var viewWidth = angular.element(document.querySelector('.maincontainer')).width(),
                winWidth = window.innerWidth,
                leftWidth = angular.element(document.querySelector('.ui-layout-left')).width(),
                center = angular.element(document.querySelector('.ui-layout-center')),
                fixBtn = angular.element(document.querySelector('.fix-nav')),
                targetWidth = winWidth - leftWidth,
                widthDifference = winWidth - viewWidth;
            

            if (fixBtn.hasClass('selected')) {
                targetWidth = winWidth - (leftWidth + widthDifference + 110);
            } else {
                targetWidth = winWidth - (leftWidth + 50);
            }

            center.width(targetWidth);

        }
    }


})();

