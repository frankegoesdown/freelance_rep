(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .directive('wrapper', function() {
              return {
                restrict: 'C',
                link: function(scope, elm, attrs) {
                  elm.layout({
                      applyDefaultStyles: true,
                      closable: false,
                      west__minSize: 340,
                      west__size: 340
                  });
                }
              };
        });

})();

