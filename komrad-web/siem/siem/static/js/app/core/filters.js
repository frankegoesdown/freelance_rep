(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .filter('getByField', getFirstObjectByField)
        .filter('isEmpty', checkEmptyObject)
        .filter('millsecondsToTimeString', millsecondsToTimeString)
        .filter('objLength', getObjectLenght);

    function getFirstObjectByField(){
        return function(input, id, field_name) {
            var f_name = field_name || 'id'
            var i=0, len=input.length;
            for (; i<len; i++) {
                if (input[i][f_name] == id) {
                    return input[i];
                }
            }
            return null;
        }
    }

    function checkEmptyObject(){
        return function (obj) {
            for(var prop in obj) {
                if(obj.hasOwnProperty(prop))
                    return false;
            }
            return true;
        }
    }

    function millsecondsToTimeString() {
        function formatValue(value){
            if (value >= 9) return value;
            if (value >= 0) return '0' + value;
        }

        return function(millseconds) {
            var seconds = Math.floor(millseconds / 1000);
            //var days = Math.floor(seconds / 86400);
            var hours = Math.floor(seconds / 3600);
            var minutes = Math.floor((seconds % 3600) / 60);
            var seconds = seconds % 60;
            var timeString = '';
            //if(days > 0) timeString += (days > 1) ? (days + " days ") : (days + " day ");
            if(hours > 0) timeString += hours + ":";
            if(minutes >= 0) timeString += formatValue(minutes) + ":";
            timeString += formatValue(seconds);
            return timeString;
        }
    }

    function getObjectLenght() {
        return function(object) {
            if (!object)
                return 0;
            return Object.keys(object).length;
        }
    }

})();
