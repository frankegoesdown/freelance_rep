(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .factory('socketio', SocketIOFactory);

    SocketIOFactory.$inject = ['socketFactory'];

    function SocketIOFactory(socketFactory) {
        var myIoSocket = io.connect('/');
        var  socketio = socketFactory({
            ioSocket: myIoSocket,
            prefix: ''
        });
        return socketio;
    }

})();

