(function () {
    'use strict';

    angular
        .module('KomradeApp.bof')
        .factory('BofLists', listsGetter);


    listsGetter.$inject = ['$http', '$q', 'Notification'];


    function listsGetter($http, $q, Notification) {
        var listsGetter = function () {
            this.busy = false;
            this.message = '';
            this.data = [];
            this.has_error = true;
        };

        listsGetter.prototype.getAllLists = function() {
            var defer = $q.defer();

            if (this.message === '') {
                this.message = 'Загрузка...';
            }
            if (this.busy) return;

            return $http.get('/bof/all')
                .success(function (data) {
                    this.data = data;
                    this.busy = false;
                }.bind(this))
                .error(function (data) {
                    this.message = 'При выполнении запроса произошла ошибка';
                    Notification.error(this.message);
                    this.has_error = true;
                    this.busy = false;
                }.bind(this))
        }

        return listsGetter;
    }



})();
