(function () {
    'use strict';

    angular
        .module('KomradeApp.bof')
        .factory('sioBof', SocketIOFactory);

    SocketIOFactory.$inject = ['socketFactory'];

    function SocketIOFactory(socketFactory) {
        var myIoSocket = io.connect('/facts_base');
        var  socketio = socketFactory({
            ioSocket: myIoSocket,
            prefix: ''
        });
        return socketio;
    }

})();

