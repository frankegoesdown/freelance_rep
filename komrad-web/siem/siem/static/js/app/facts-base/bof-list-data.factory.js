(function () {
    'use strict';

    angular
        .module('KomradeApp.bof')
        .factory('ListsRequester', ListsRequesterFactory);


    ListsRequesterFactory.$inject = ['$http', '$q', 'Notification'];

    function ListsRequesterFactory($http, $q, Notification) {
        var ListsRequester = function () {
            this.busy = false;
            this.message = '';
            this.has_error = false;
        };

        function requestSetter(type) {
            var requestUrl = '/bof/alist';
            if (type === 'custom') {
                requestUrl = '/bof/clist';
            }
            return requestUrl;
        }

        /**
         * get list data by item
         * @param  {[object]} item have to contain {type: _auto_/_custom_, id:_uuid_}
         * @return {[promise]}      http promise
         */
        ListsRequester.prototype.getList = function (item) {
            var requestUrl;

            requestUrl = requestSetter(item.type);

            if (this.message === '') {
                this.message = 'Загрузка...';
            }
            if (this.busy) return;

            return $http.get(requestUrl + '/' + item.id)
                .success(function (data) {
                    if(data.values === null) {
                        this.has_error = true;
                        this.message = 'Некорректный запрос';
                        return;
                    }
                    if (data.values.length === 0) {
                        this.has_error = true;
                        this.message = 'Нет значений';
                        return;
                    }
                    this.has_error = false;
                }.bind(this))
                .error(function (data) {
                    this.message = 'При выполнении запроса произошла ошибка';
                    Notification.error(this.message);
                    this.has_error = true;
                    this.busy = false;
                }.bind(this))

        }

        ListsRequester.prototype.createList = function (settings) {
            return $http
                .post(requestSetter(settings.type), angular.toJson(settings));
        }

        ListsRequester.prototype.updateList = function (settings) {
            return $http.put(requestSetter(settings.type), angular.toJson(settings));

        }

        ListsRequester.prototype.deleteList = function (settings) {
            return $http.delete(requestSetter(settings.type) + '/' + settings.id)
                .success(function (data, status) {
                })
                .error(function () {
                    Notification.error('Ошибка удаления данных');
                });
        }

        ListsRequester.prototype.rawSqlRequest = function (sql) {
            return $http.post('/bof/raw_request', {request: sql})
                .success(function (data, status) {
                }).error(function () {
            });
        }

        return ListsRequester;
    }

})();
