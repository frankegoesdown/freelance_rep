(function () {
    'use strict';

    angular.module('KomradeApp.bof', [
        'KomradeApp.core',
        'ngAnimate',
        'infinite-scroll',
        'highcharts-ng',
        'ui.bootstrap',
        'ncy-angular-breadcrumb',
        'ui-notification',
        'ngFileSaver',
        'angularMoment',
        'summernote',
        'mgo-angular-wizard',
        'ui.layout',
        'restangular',
        
    ]);
})();