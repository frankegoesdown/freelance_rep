(function () {
    'use strict';

    angular
        .module('KomradeApp.bof')
        .factory('ChartService', chartFactory);


    chartFactory.$inject = ['$http', 'WidgetPuller'];


    function chartFactory($http, WidgetPuller) {

        function convertToColumn(data){
            var res = [];
            for (var i = 0; i < data.length; i++) {1
                res.push({name: data[i][0], data:[data[i][1]]})
            }
            return res
        }

        function convertToColumn3D(data){
            var res = [];
            for (var i = 0; i < data.length; i++) {1
                res.push({name: data[i][0], data:[data[i][1]]})
            }
            return {series:res}
        }

        function configData(graphType, data) {
            var values = data;
            var settings = {
                graph_type: graphType,
                facet: [{c: 1, id: '', locale: "Категория", name: "", type: ""}]
            }

            if (graphType === 'column'){
                values = convertToColumn(data);
            }
            if (graphType === 'column3d'){
                values = convertToColumn3D(data);
            }

            configData = WidgetPuller.getChartDataConfig(settings, values).chart;
            configData.options.plotOptions.series.point.events.click = undefined;
            return configData;
        }


        return {
            getConfigs: configData
        };

    }

})();
