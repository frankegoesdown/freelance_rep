(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .controller('queryListController', queryListCtrl)
        .controller('deleteDialogController', deleteDialogCtrl)

    queryListCtrl.$inject = ['$scope', '$state', '$http', '$uibModal', 'QueriesContent'];
    deleteDialogCtrl.$inject = ['$scope', '$http', '$uibModalInstance', 'settings'];

    function queryListCtrl($scope, $state, $http, $uibModal, QueriesContent) {

        $scope.showQuery = function (id) {

        };

        // remove query
        $scope.removeQuery = function (entity) {
            var removeModalInstance = $uibModal.open({
                scope: $scope,
                templateUrl: 'dialog.html',
                controller: 'deleteDialogController',
                windowClass: "app-modal-window",
                size: 'sm',
                resolve: {
                    settings: function () {
                        return entity;
                    }
                }
            });

        };

        $scope.$on('$destroy', function () {
        });

        $scope.$on('$viewContentLoaded', function () {
            $scope.data = new QueriesContent();
            $scope.data.loadData();
            $scope.displayedData = [].concat($scope.data.items);
        });

        $scope.$on('queryDeleted', function (e, settings) {
            var index = $scope.data.items.indexOf(settings);
            $scope.data.items.splice(index, 1);
        })

    }

    function deleteDialogCtrl($scope, $http, $uibModalInstance, settings) {
        
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }

        $scope.remove = function () {
            $http.delete('/query/body/' + settings.id)
                .success(function (data, status, headers, config) {
                    $scope.$emit('queryDeleted', settings);
                    $uibModalInstance.dismiss('cancel');
                }).error(function (data) {
            });

        }


    }
})();

