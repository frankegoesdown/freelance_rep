(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .config(uiRouterConfig);


    uiRouterConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

    function uiRouterConfig($stateProvider, $urlRouterProvider, $httpProvider) {

        var labels = {
            widgets: 'Виджеты',
            realtime: 'События в реальном времени',
            search: 'Поиск по событиям',
            querylist: 'Все запросы',
            eventvisio: 'Визуализатор событий'
        }
        $stateProvider
            .state('summary', {
                url: '/',
                templateUrl: '/summary',
                ncyBreadcrumb: {
                    label: labels.widgets
                },
                controller: 'widgetsPageController',
                data: {
                    docFile: "event_listener/widgets.pdf"
                }
            })
            .state('realtime', {
                url: '/realtime',
                templateUrl: '/realtime',
                ncyBreadcrumb: {
                    label: labels.realtime
                },
                controller: 'realtimeController',
                data: {
                    docFile: "event_listener/realtime.pdf"
                },
                resolve: {
                    fields: function ($q, treeFields) {
                        var deffered = $q.defer();
                        treeFields.getAll().then(function (data) {
                            deffered.resolve(data);
                        }, function (error) {
                            return;
                        });
                        return deffered.promise;
                    }
                }
            })
            // next resolves:
            // widget - widget data - need for getting query
            // duration - widget data like query duration and field of normalization (ef)
            .state('search', {
                url: '/search/:queryId',
                templateUrl: '/search/',
                ncyBreadcrumb: {
                    label: labels.search
                },
                controller: 'searchController',
                data: {
                    docFile: "event_listener/search.pdf"
                },
                resolve: {
                    widget: function () {
                        return undefined;
                    },
                    duration: function () {
                        return undefined;
                    },
                    query: function (SearchQuery, $stateParams) {
                        var query = new SearchQuery();
                        return query.get($stateParams.queryId)
                    },
                    fields: function ($q, treeFields) {
                        var deffered = $q.defer();
                        treeFields.getAll().then(function (data) {
                            deffered.resolve(data);
                        }, function (error) {
                            return;
                        });
                        return deffered.promise;
                    }
                }
            })
            .state('searchdef', {
                url: '/search',
                templateUrl: '/search/',
                ncyBreadcrumb: {
                    label: labels.search
                },
                controller: 'searchController',
                data: {
                    docFile: "event_listener/search.pdf"
                },
                resolve: {
                    widget: function () {
                        return undefined;
                    },
                    duration: function () {
                        return undefined;
                    },
                    query: function () {
                        return undefined;
                    },
                    fields: function ($q, treeFields) {
                        var deffered = $q.defer();
                        treeFields.getAll().then(function (data) {
                            deffered.resolve(data);
                        }, function (error) {
                            return;
                        });
                        return deffered.promise;
                    }
                }
            })
            .state('search_widget', {
                url: '/search/widget/:id/:from/:to/:ef_value',
                templateUrl: '/search/',
                ncyBreadcrumb: {
                    label: labels.search
                },
                controller: 'searchController',
                data: {
                    docFile: "event_listener/search.pdf"
                },
                resolve: {
                    query: function () {
                        return undefined;
                    },
                    widget: function ($http, $stateParams) {
                        var id = $stateParams.id || '';
                        return $http({method: 'GET', url: '/widget/' + id});
                    },
                    duration: function ($stateParams) {
                        return {
                            from: $stateParams.from,
                            to: $stateParams.to,
                            ef_value: $stateParams.ef_value
                        };
                    },
                    fields: function (eventFields, $stateParams) {
                        $stateParams.fields = [];
                        eventFields.getAll().then(function (data) {
                            data.forEach(function (item) {
                                item.label = item.label;
                                item.id = item.name;
                                $stateParams.fields.push(item);
                            });
                        }, function (error) {
                            return;
                        });
                        return $stateParams.fields;
                    }
                }
            })
            .state('querylist', {
                url: '/querylist',
                templateUrl: '/querylist',
                ncyBreadcrumb: {
                    label: labels.querylist
                },
                controller: 'queryListController',
                data: {
                    docFile: "event_listener/query_list.pdf"
                }
            })
            .state('sec_event', {
                url: '/event',
                templateUrl: '/event',
                controller: 'eventDetailController'
            })
            .state('eventvisio', {
                url: '/eventvisio',
                templateUrl: '/eventvisio',
                ncyBreadcrumb: {
                    label: labels.eventvisio
                }
            })


    }


})();

