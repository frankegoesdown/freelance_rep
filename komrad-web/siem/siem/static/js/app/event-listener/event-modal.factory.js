(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .factory('eventViewer', openEventModal);

    openEventModal.$inject = ['$uibModal', '$http'];

    function openEventModal($uibModal, $http) {

        var openRemoveModal = function(id) {
            var modalInstance = $uibModal.open({
                templateUrl: '/event.html',
                size: 'lg',
                windowClass: "app-modal-window",
                controller: 'eventDetail',
                resolve:{
                    event_data:  $http.get('/event/' + id)
                }
            });

            modalInstance.result.then(function () {
            }, function () {
            });
        }
        return {
            open: openRemoveModal
        }
    }

})();

