(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .directive('ngShow', ngShowForAnimate);


    function ngShowForAnimate($compile, $animate) {
        return {
            priority: 1000,
            link: function(scope, element, attrs) {
                if (element.hasClass('animate-show')) {
                    // we could add no-animate and $compile per 
                    // http://stackoverflow.com/questions/23879654/angularjs-exclude-certain-elements-from-animations?rq=1
                    // or we can just include that no-animate directive's code here
                    $animate.enabled(element,false)
                    scope.$watch(function() {
                        $animate.enabled(element, true)
                    })
                }
            }
        }
    }
})();
