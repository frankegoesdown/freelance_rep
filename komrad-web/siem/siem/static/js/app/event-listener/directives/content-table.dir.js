(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .directive('contenteditable', editableTableDir);

    function editableTableDir() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, elm, attr, ngModel) {

                function updateViewValue() {
                    ngModel.$setViewValue(this.innerHTML);
                }

                //Or bind it to any other events
                //elm.on('keyup', updateViewValue);

                scope.$on('$destroy', function() {
                    elm.off('keyup', updateViewValue);
                });

                ngModel.$render = function() {
                    elm.html(ngModel.$viewValue);
                }
                elm.bind('blur', function() {
                    scope.$apply(function() {
                        ngModel.$setViewValue(elm.html());
                    });
                });
            }
        }
    }
})();

