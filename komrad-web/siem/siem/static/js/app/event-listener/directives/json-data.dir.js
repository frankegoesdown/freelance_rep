(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .directive('jsonData', jsonDataDir);

    function jsonDataDir() {
        return {
            restrict: 'A',
            link: function(scope, element, attributes, controller) {
                //send data to template
                scope.$emit('templateHaveJson', angular.fromJson(element.html()));
            }
        };
    }
})();


