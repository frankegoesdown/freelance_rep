(function() {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .run(preloadEventFields)
        .run(joinEventsRoom);

    preloadEventFields.$inject = ['eventFields'];
    joinEventsRoom.$inject = ['socketio'];

    function preloadEventFields(eventFields) {
        eventFields.getAll();
        eventFields.getFaceted();
    };

    function joinEventsRoom(socketio) {
        socketio.emit('join_events', {});
    };

})();
