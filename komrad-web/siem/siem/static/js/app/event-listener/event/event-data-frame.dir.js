(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .directive('eventDataFrame', integratedEvent);

    eventDataFrameCtrl.$inject = ['$scope'];
    integratedEvent.$inject = ['$compile'];

    function getTemplate(type) {
        var for_include = includeTemplate(type);

        return '<div class="row">' +
            '<div class="col-lg-12 col-md-12">' +
            '<label class="pull-left" style="color: #63a8eb;margin-bottom: 0px;">{[{ data.label }]}</label>' +
            '<div class="content-tools i-block pull-right">' +
                '<a class="on-white"' +
                   'style="color: #63a8eb;margin-bottom: 0px;"' +
                   'ng-click="showOrHide()">' +
                    '<i ng-show="showData" class="fa fa-minus-circle">' +
                    '</i>' +
                    '<i ng-hide="showData" class="fa fa-plus-circle">' +
                    '</i>' +
                '</a>' +
            '</div>' +
            '</div>' +
            '<hr style="width: 100%; color: #63A8EB; height: 1px; background-color:#63A8EB; margin-top:0px">' +
            '<div ng-show="showData" class="col-lg-12">' +
                for_include.included_template +
            '</div>' +
            '<div ng-hide="showData">' +
            '</div>' +
            '</div>';
    }

    function includeTemplate(type) {
        var ext_tmpl = {};
        ext_tmpl.included_template = "<regular-data-frame data='data'></regular-data-frame>";
        ext_tmpl.img = ''

        if (type === 'net_data') {
            ext_tmpl.included_template = "<net-event-data data='data'></net-event-data>";
        }
        else if (type === 'base_additional_data') {
            ext_tmpl.included_template = '<div ng-include src="\'baseEventData_bottom.html\'"></div>';
        }
        else if (type === 'rubicon') {
            ext_tmpl.included_template = "<rubicon-data-frame data='data'></rubicon-data-frame>";
            ext_tmpl.img = 'static/img/rubicon.jpg';
        }
        return ext_tmpl;
    }

    function integratedEvent($compile) {
        return {
            restrict: 'E',
            scope: {
                data: '=',
                showData: '@?show'
            },
            link: function (scope, element, attrs) {
                var newEl = angular.element(getTemplate(scope.data.name));
                // using jQuery after new element creation, to append element
                element.append(newEl);
                // returns a function that is looking for scope
                // use angular compile service to instanitate a new widget element
                $compile(newEl)(scope);

                attrs.show = attrs.show || 'true';
                attrs.show = attrs.show === 'true';
            },
            controller: eventDataFrameCtrl
        };
    }

    function eventDataFrameCtrl($scope) {
        $scope.showOrHide = function() {
            $scope.showData = !$scope.showData;
        }

    }






})();
