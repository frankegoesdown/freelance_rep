(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .service('eventFields', getEventFields)
        .service('widgetsService', getUserWidgets);

    getEventFields.inject = ['$http', '$q', '$filter'];

    function getEventFields($http, $q, $filter) {

        var allFields = undefined;
        var facetedFields = undefined;


        function getAllFileds() {
            var defer = $q.defer();
            if(allFields) {
                defer.resolve(allFields);
            }

            $http.get('/fields/all')
            .success(function(data) {
                allFields=data.data;
                defer.resolve(allFields);
            })
            return defer.promise;
        }

        function getFacetedFields() {
            var defer = $q.defer();
            if(facetedFields) {
                defer.resolve(facetedFields);
            }

            $http.get('/fields/all/1')
            .success(function(data) {
                facetedFields=data.data;
                defer.resolve(data.data);
            })
            return defer.promise;
        }

        function getFieldById(field_id) {
            if(!allFields) {
                return undefined;
            }
            return $filter('filter')(allFields, {id:field_id})[0];
        }

        return  {
            getAll: getAllFileds,
            getFaceted: getFacetedFields,
            byId: getFieldById
        }
    }

    function getUserWidgets($http) {
        var vm = this;
        vm.fields = [];
        var promise = $http.get('/user/widgets').
        success(function (data) {
            vm.fields = data.response;
            return vm.fields;
        });
        return promise;
    }


})();
