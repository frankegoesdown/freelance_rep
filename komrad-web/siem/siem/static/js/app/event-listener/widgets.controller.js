(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .controller('createUpdateWidgetInstanceCtrl', cuwInstanceCtrl)
        .controller('CustomWidgetCtrl', customWidgetCtrl)
        .controller('widgetsPageController', widgetsPageCtrls)
        .controller('widgetWizardController', widgetWizardCtrl);

    cuwInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'QueriesContent', '$filter',
        'eventFields', '$q', '$timeout', 'settings'];

    customWidgetCtrl.$inject = ['$scope', '$uibModal', '$http', 'moment',
        'eventFields', '$filter', '$timeout', 'WidgetPuller', 'socketio', '$state'];

    widgetsPageCtrls.$inject = ['$scope', '$uibModal', '$timeout', '$log', '$http', '$filter',
        'widgetsService', 'Notification', '$animate', '$element'];


    widgetWizardCtrl.$inject = ['$scope', '$uibModalInstance', 'QueriesContent', '$filter', '$q',
        '$timeout', 'settings', 'WidgetWizard', 'FacetNesting', 'FieldsTransform'];

    /**
     * [cuwInstanceCtrl controller for modal window (create or update widget)]
     */
    function cuwInstanceCtrl($scope, $uibModalInstance, QueriesContent, $filter, eventFields, $q, $timeout, settings) {

        var _selected;
        $scope.selected_query = undefined;


        $scope.queries = new QueriesContent();
        $scope.queries.loadData();
        $scope.dropdown = [];
        $scope.selectedItem = undefined;

        /**
         * [widget_settings check loaded settings. if parent scope hasn`t it
         * return default settings]
         * @return {[object]} [settings config]
         */
        $scope.widget_settings = function () {
            if (typeof settings === 'undefined') {
                return {
                    name: undefined,
                    widgetType: 'standart',
                    description: '',
                    query_body: undefined,
                    plot_duration: undefined,
                    graph_type: undefined,
                    update_period: undefined,
                    facet: undefined,
                    results_count: 1,
                    data_type: undefined,
                    show_delta: false,
                    sort: false
                }
            }
            else {
                var setts = angular.copy(settings);

                // заглушка от мультифасетов
                setts.results_count = setts.facet["0"].c;
                setts.data_type = setts.facet["0"].id;
                setts.plot_duration = setts.plot_duration - setts.update_period;
                return setts;
            }
        }();

        $scope.$on('setQueryBuilder', function (event, data, inputValue) {
            // Make sure that the interval is destroyed too
            setRules($scope.widget_settings.query_body);
        });

        $scope.$watch('adder.$valid', function (newVal) {
            $scope.informationStatus = true;
        });

        // query builder jquery element

        /**
         * [setRules set rules in query builder]
         * @param {[json]} rules [rules json data]
         */
        function setRules(rules) {
            if (typeof rules === 'string') {
                rules = JSON.parse(rules);
            }
            var queryBuilderEl = angular.element(
                document.querySelector('#builder'));
            queryBuilderEl.queryBuilder('setRules', rules);
        }

        $scope.showGraphConfirm = false;


        $scope.items = [{
            id: 'line',
            name: 'Линейный график'
        }, {
            id: 'pie',
            name: 'Круговая диаграмма'
        }, {
            id: 'column',
            name: 'Гистограмма'
        }, {
            id: 'polar',
            name: 'Радиальная диаграмма'
        }, {
            id: 'solidgauge',
            name: 'Индикатор 100%'
        }, {
            id: 'gauge',
            name: 'Спидометр'
        }/*,{
         id: 'bar',
         name: 'Горизонтальная гистограмма'
         },{
         id: 'stackedcolumn',
         name: 'Комбинированная гистограмма'
         }*/];

        $scope.item = {};

        $scope.data_items = [];
        eventFields.getFaceted().then(function (data) {
            for (var f in data) {
                $scope.data_items.push({
                    id: data[f].id,
                    name: data[f].locale,
                })
            }
        });
        // directive callback function
        $scope.callbackQuery = function (item) {
            if (item.body !== $scope.widget_settings.query_body) {
                var queryBuilderEl = angular.element(document.querySelector(
                    '#builder'));
                queryBuilderEl.queryBuilder('reset');
                setRules(item.body);
            } else {
                return;
            }
        };


        $scope.dataWaiting = function () {
            var query = $q.defer();
            var dropdown = [];
            $timeout(function () {
                $scope.queries.items.forEach(function (item) {
                    dropdown.push({
                        readableName: item.readableName,
                        id: item.id,
                        created: item.created,
                        body: item.body,
                        description: item.description,
                        role_access: item.role_access
                    });
                });

                query.resolve(dropdown); // через какое-то время (асинхронно) - исполняем промис
            }, 1000);

            return query.promise;
        };

        $scope.dataWaiting()
            .then(successGetData, errorGetData);

        function successGetData(value) {
            value.forEach(function (item) {
                $scope.dropdown.push({
                    readableName: item.readableName,
                    id: item.id,
                    created: item.created,
                    body: item.body,
                    description: item.description,
                    role_access: item.role_access
                });
            });
        }

        function errorGetData() {
            return;
        }

        $scope.objectMessage = '';

        $scope.queryObject = null;

        $scope.filterObjectList = function (userInput) {
            var filter = $q.defer();
            var normalisedInput = userInput.toLowerCase();

            var filteredArray = $scope.dropdown.filter(function (query) {
                var matchQueryName = query.readableName.toLowerCase().indexOf(normalisedInput) === 0;

                return matchQueryName;
            });

            filter.resolve(filteredArray);
            return filter.promise;
        };

        $scope.areaChartFlag = false;
        $scope.facetedChartFlag = false;
        $scope.graph3d = false;
        $scope.disableFlag = false;

        // adding new widget
        $scope.ok = function (new_settings) {
            var queryBuilderEl = angular.element(document.querySelector(
                '#builder'));
            var duration = angular.element(document.querySelector(
                '.duration')),
                period = angular.element(document.querySelector(
                    '.period'));
            this.form.duration.$invalid = false;

            if (typeof new_settings.data_type == 'undefined') {
                new_settings.data_type = $scope.data_items[0];
            }


            new_settings.query_body = queryBuilderEl.queryBuilder('getRules');
            if (new_settings.graph_type === undefined ||
                new_settings.name === undefined ||
                $filter('isEmpty')(new_settings.query_body) ||
                new_settings.update_period === undefined ||
                new_settings.results_count === undefined ||
                new_settings.plot_duration === undefined ||
                new_settings.data_type === undefined ||
                +duration.val() < +period.val()) {
                this.form.name.$dirty = true;
                this.form.period.$dirty = true;
                this.form.ecount.$dirty = true;
                this.form.graphType.$dirty = true;
                this.form.dataType.$dirty = true;
                // ахуеть какой костыль
                if (+duration.val() < +period.val()) {
                    this.form.duration.$invalid = true;
                    this.form.duration.$dirty = true;
                }
                return;
            }

            new_settings.graph_type = new_settings.graph_type.id;
            $scope.widget_settings = new_settings;


            // setup new format data of request
            new_settings.facet = {
                0: {
                    id: new_settings.data_type.id,
                    c: new_settings.results_count
                }
            };

            delete new_settings.data_type;
            delete new_settings.results_count;

            if ($scope.widget_settings.name == "") {
                $scope.widget_settings.name = "Новый виджет";
            }

            if ($scope.widget_settings.widget_type == "") {
                $scope.widget_settings.widget_type = 'standart';
            }

            $scope.getGraphOptions();
            //new_settings.plot_duration += new_settings.update_period;
            $uibModalInstance.close($scope.widget_settings);

        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };


        $scope.restore = function () {
            var queryBuilderEl = angular.element(document.querySelector(
                '#builder'));
            var input = angular.element(document.querySelector(
                ".input-dropdown input"));
            queryBuilderEl.queryBuilder('reset');
            input.val('').trigger('change');
        };


        $scope.showDataConfirm = false;

        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };

    }


    function customWidgetCtrl($scope, $uibModal, $http, moment,
                              eventFields, $filter, $timeout, WidgetPuller, socketio, $state) {
        $scope.inited = false;
        // set forward for socketio
        function getEvent() {
            return 'WD:' + $scope.widget.settings.uuid;
        }

        var dataUpdater = undefined;
        var lastTimestamp = 0;
        $scope.chart = undefined;
        var listener = function () {
        };

        function removeListeners() {
            listener();
            socketio.removeListener(getEvent());
        }

        function forwardListeners() {
            socketio.forward(getEvent());
            listener = $scope.$on(getEvent(), function (ev, data) {
                var dataUpdater = WidgetPuller.getPusher($scope.widget.settings.graph_type);
                if (!$scope.inited) {
                    //$timeout(function () {
                        var configs = WidgetPuller.getChartDataConfig(
                            $scope.widget.settings, data);
                        $scope.chart = configs.chart;
                        lastTimestamp = configs.l_ts;
                        $scope.inited = true;
                    //}, 1000)
                }
                else {
                    var chconfig = $scope.chart.getHighcharts();
                    lastTimestamp = dataUpdater(chconfig, data, $scope.widget.settings.plot_duration,
                        $scope.widget.settings.update_period);
                }
            });
        }


        function startInitGetter() {
            $http.get('/widget/' + $scope.widget.settings.uuid + '/' + lastTimestamp)
                .success(function (data) {
                    // set forwarding for socket
                    // init chart
                    //$timeout(function () {
                        var configs = WidgetPuller.getChartDataConfig($scope.widget.settings,
                            data.result);

                        $scope.chart = configs.chart;
                        lastTimestamp = configs.l_ts;
                        $scope.inited = true;
                        forwardListeners();
                    //}, 1000)
                })
                .error(function (data) {
                    forwardListeners();
                });
        }

        startInitGetter();

        $scope.$on('$destroy', function () {
            // remove listeners
            removeListeners();
        });

        // slot for changing widget event
        function widgetChanged(new_settings) {
            var facetEquals = true;
            if (new_settings.facet.length === $scope.widget.settings.facet.length) {
                for (var i = 0; i < new_settings.facet.length; i++) {
                    if (new_settings.facet[i].id !== $scope.widget.settings.facet[i].id ||
                        new_settings.facet[i].c !== $scope.widget.settings.facet[i].c) {
                        facetEquals = false;
                        break;
                    }
                }
            }
            // check: if query was changed, u didn`t recreate widget
            if (angular.equals(new_settings.query_body, $scope.widget.settings.query_body) &&
                new_settings.update_period === $scope.widget.settings.update_period &&
                new_settings.plot_duration === $scope.widget.settings.plot_duration &&
                new_settings.data_type === $scope.widget.settings.data_type &&
                new_settings.results_count === $scope.widget.settings.results_count &&
                new_settings.graph_type === $scope.widget.settings.graph_type &&
                new_settings.show_delta === $scope.widget.settings.show_delta &&
                new_settings.sort === $scope.widget.settings.sort && facetEquals) {

                $scope.widget.settings = new_settings;
                $scope.$emit('needToSaveGridster', [1]);

                return;
            }

            removeListeners();
            var uuid_to_put = $scope.widget.settings.uuid;
            $scope.widget.settings = new_settings;
            delete new_settings.uuid;

            $http.put('/widget/' + uuid_to_put, angular.toJson(new_settings))
                .success(function (data, status) {
                    $scope.widget.settings.uuid = data.uuid;
                    $scope.chart = undefined;
                    $scope.inited = false;
                    lastTimestamp = 0;
                    startInitGetter();

                }).error(function () {
            });

        }

        // $scope.onClick = function (points, evt) {
        // };

        //notification message when widget delete
        $scope.deleteWidget = function (widget) {
            var removeModalInstance = $uibModal.open({
                scope: $scope,
                templateUrl: 'dialog.html',
                controller: 'createUpdateWidgetInstanceCtrl',
                windowClass: "app-modal-window",
                size: 'sm',
                resolve: {
                    settings: function () {
                        return undefined;
                    }
                }
            });
        };

        $scope.goToSearch = function () {
            var url = $state.href('search_widget',
                {
                    id: $scope.widget.settings.uuid,
                    from: undefined,
                    to: undefined,
                    ef_value: undefined
                }
            );
            window.open(url, '_blank');
        }

        // remove grid from gridster
        $scope.remove = function (without_container) {
            removeListeners();
            without_container = without_container || false;
            var promise = $http.delete('/widget/' + $scope.widget.settings.uuid)
                .success(function (data, status, headers, config) {
                    // we can delete widget only from server not from view
                    if (!without_container) {
                        $scope.widgets.splice($scope.widgets.indexOf($scope.widget), 1);
                        $scope.$emit('needToSaveGridster', [1]);
                    }
                }).error(function (data) {
                    Notification.error('Ошибка удаления данных');
                });
            return promise;
        };

        // open setting for selected widget
        $scope.openSettings = function (widget) {
            if (typeof widget.settings.query_body === 'string') {
                try {
                    widget.settings.query_body = angular.fromJson(widget.settings.query_body);
                } catch (err) {
                    widget.settings.query_body = widget.settings.query_body;
                }

            }

            var modalInstance = $uibModal.open({
                scope: $scope,
                templateUrl: 'createWidgetWizard.html',
                controller: 'widgetWizardController as wizard',
                windowClass: "app-modal-window",
                size: 'lg',
                resolve: {
                    settings: function () {
                        return widget.settings;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $scope.$broadcast('setQueryBuilder', widget.settings);
            });

            modalInstance.result.then(function (widget_settings) {
                var newSettings = angular.copy(widget_settings);
                //newSettings.plot_duration -= newSettings.update_period;
                var equal = angular.equals(newSettings, widget.settings);
                var facetEquals = true;
                if (newSettings.facet.length === widget.settings.facet.length) {
                    for (var i = 0; i < widget.settings.facet.length; i++) {
                        if (newSettings.facet[i].id !== widget.settings.facet[i].id ||
                            newSettings.facet[i].c !== widget.settings.facet[i].c) {
                            facetEquals = false;
                            break;
                        }
                    }
                }
                if (equal && facetEquals) {
                    return;
                }
                widgetChanged(widget_settings);
            }, function () {
            });
        };
    }

    function widgetsPageCtrls($scope, $uibModal, $timeout, $log, $http, $filter,
                              widgetsService, Notification, $animate, $element) {

        $scope.animationsEnabled = true;
        $scope.enable_gridster = false;
        $scope.widgets = [];

        function changeCursorToMove() {
            var panel = angular.element(document.querySelectorAll(
                ".panel-heading"));
            panel.each(function (i, pan) {
                pan.classList.add('move');
            });
        }

        function changeCursoreToDefault() {
            var panel = angular.element(document.querySelectorAll(
                ".panel-heading"));
            panel.each(function (i, pan) {
                pan.classList.remove('move');
            });
        }

        // connect to events

        widgetsService.then(function (data) {
            // I don`t know why response getting id data.data object
            //if (data.data.response === {})
            //  $scope.widgets = []
            $scope.widgets = data.data.response || [];
            if ($filter('isEmpty')(data.data.response))
                $scope.widgets = []
        });


        $scope.openCreator = function (size) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'createWidgetWizard.html',
                controller: 'widgetWizardController as wizard',
                size: size,
                windowClass: "app-modal-window",
                resolve: {
                    settings: function () {
                        return undefined;
                    }
                }

            });
            modalInstance.rendered.then(function () {
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

            modalInstance.result.then(function (widget_name) {
                $scope.addWidget(widget_name);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.gridsterOptions = {
            margins: [20, 20],
            columns: 24,
            draggable: {
                enabled: false,
                stop: function (event, $element, widget) {
                    $scope.save();
                }
            },
            rowHeight: 50,
            resizable: {
                enabled: false,
                resize: function (event, $element, widget) {
                    $timeout(function () {
                        window.dispatchEvent(new Event('resize'));
                    }, 0);
                },
                stop: function (event, $element, widget) {

                    $scope.save();
                }
            },
            floating: true,
            minSizeY: 4,
            minSizeX: 4
        };

        $scope.$on('gridster-resized', function (sizes, gridster) {
            $timeout(function () {
                $scope.$broadcast('highchartsng.reflow');
            }, 10);
        });

        $scope.clear = function () {
            $scope.widgets = [];
        };

        // save $scope.widgets data on server
        $scope.save = function () {
            $http.post('/user/widgets',
                angular.toJson({widgets: $scope.widgets}))
                .success(function (data, status) {
                    // Notification.success('Страница сохранена успешно');
                }).error(function () {
                Notification.error('Ошибка записи данных');
            })
        };

        // $scope.$on('needToSaveGridster')

        // save $scope.widgets data on server
        $scope.block_unblock_gridster = function () {
            if ($scope.enable_gridster) {
                $scope.enable_gridster = false;
                $scope.gridsterOptions.draggable.enabled = false;
                $scope.gridsterOptions.resizable.enabled = false;
                changeCursoreToDefault();
            }
            else {
                $scope.enable_gridster = true;
                $scope.gridsterOptions.draggable.enabled = true;
                $scope.gridsterOptions.resizable.enabled = true;
                changeCursorToMove();
            }
        };

        function initWidgetSize(graph_type) {
            if (graph_type === 'pie' || graph_type === 'polar' || graph_type === 'pie3d' ||
                graph_type === 'solidgauge' || graph_type === 'gauge')
                return {x: 4, y: 6};
            return {x: 10, y: 6}
        }

        $scope.watcher = function () {
        };

        // add widgets to panel
        $scope.addWidget = function (new_settings) {
            // var json_for_post = angular.toJson(new_settings);

            // add uuid to settings
            $http.post('/widget/add', angular.toJson(new_settings))
                .success(function (data, status) {
                    new_settings.uuid = data.uuid;
                    new_settings.locale = data.locale;
                    var sett = angular.copy(new_settings);
                    var size = initWidgetSize(new_settings.graph_type);
                    $scope.widgets.push({
                        sizeX: size.x,
                        sizeY: size.y,
                        settings: sett
                    });
                    setTimeout(function () {
                        $scope.save();
                    }, 1000);
                }).error(function () {
            })
        };

        $scope.$on('needToSaveGridster', function () {
            $scope.save();
        });
    }

    function widgetWizardCtrl($scope, $uibModalInstance, QueriesContent, $filter,
                              $q, $timeout, settings, WidgetWizard, FacetNesting, FieldsTransform) {
        var self = this;
        self.selected_query = undefined;
        self.queries = new QueriesContent();
        var dropdownQueries = self.queries.loadData();
        var dropdownLists = self.queries.getFactBaseLists();
        self.dropdown = [];
        self.factBaseLists = [];
        self.selectedItem = undefined;
        self.sourceSwitch = false;
        self.facetsNestingLevel = [];
        var facet = new FacetNesting();
        facet.getFields();
        facet.id = self.facetsNestingLevel.length;
        self.facetsNestingLevel.push(facet);
        self.facetItems = {};
        self.facetCounts = {};
        self.ecountError = {};
        self.facetTypeError = {};
        self.qbFilters = FieldsTransform.transform();
        self.qbPlugins = ['sortable', 'mapping2', 'datetimepicker', 'list_search'];
        self.qbLanguage = 'ru';
        self.widgetConfirmed = false;
        self.facetCountErr = [];
        self.facetTypeErr = [];
        self.createButtonText = 'Создать';
        self.facetTypeErr = {
            0: true,
            1: true
        };
        self.facetCountErr = {
            0: true,
            1: true
        };

        /**
         * [widget_settings check loaded settings. if parent scope hasn`t it
         * return default settings]
         * @return {[object]} [settings config]
         */
        self.widget_settings = function () {
            if (typeof settings === 'undefined') {
                return {
                    name: undefined,
                    widgetType: 'standart',
                    description: '',
                    query_body: undefined,
                    plot_duration: undefined,
                    graph_type: 'line',
                    update_period: undefined,
                    facet: [],
                    show_delta: 0,
                    sort: 0
                }
            }
            else {
                self.facetsNestingLevel.length = 0;
                var setts = angular.copy(settings);
                setts.facet.forEach(function (item, i) {

                    var facet = new FacetNesting(),
                        modifyItem = {};
                    facet.getFields().then(function (data) {
                        data.forEach(function (dataItem) {
                            if (item.id === dataItem.facetId) {
                                modifyItem = {
                                    id: dataItem.id,
                                    facetId: dataItem.facetId,
                                    locale: dataItem.name
                                }
                            }
                        });
                        facet.id = self.facetsNestingLevel.length;
                        self.facetsNestingLevel.push(facet);
                        self.facetItems[i] = modifyItem;
                        self.facetCounts[i] = item.c;
                    });
                });
                return setts;
            }
        }();
        
        self.wizardConfig = {
            name: 'widgetWizard',
            currentStep: {}
        };


        $scope.$on('setQueryBuilder', function (event, data, inputValue) {
            self.widget_settings = angular.copy(data);
            self.widgetConfirmed = true;
        });


        /**
         * [setRules set rules in query builder]
         * @param {[json]} rules [rules json data]
         */
        function setRules(rules) {
            var input = angular.element(document.querySelector(
                ".facts-base input"));
            if (typeof rules === 'string') {
                try {
                    rules = JSON.parse(rules);
                } catch (err) {
                    self.sourceSwitch = true;
                    for (var i = 0; i < self.factBaseLists.length; i++) {
                        if (rules === self.factBaseLists[i].body) {
                            input.val(self.factBaseLists[i].readableName);
                            break;
                        }
                    }
                    return;
                }
            }
            $scope.$broadcast('qb.setRules', rules);
        }

        // add facet nesting
        self.addNestingLevel = function () {
            var facet = new FacetNesting();
            facet.getFields();
            facet.id = self.facetsNestingLevel.length;
            self.facetsNestingLevel.push(facet);
        };

        //delete facet nesting
        self.deleteNestingLevel = function () {
            self.facetsNestingLevel.pop();
        };

        //restore for base of facts
        self.restoreBof = function () {
            var input = angular.element(document.querySelector(
                ".facts-base input"));
            self.widget_settings.query_body = '';
            input.val('').trigger('change');
        };

        //restore for event base
        self.restoreEb = function () {
            $scope.$broadcast('qb.reset');
            var input = angular.element(document.querySelector(
                ".event-base input"));
            input.val('').trigger('change');
        };

        self.showGraphConfirm = false;

        self.items = [{
            id: 'line',
            name: 'Линейный'
        }, {
            id: 'area',
            name: 'Линейный c заливкой'
        }, {
            id: 'facetedLine',
            name: 'Линейный множественный'
        }, {
            id: 'pie',
            name: 'Круговая диаграмма'
        }, {
            id: 'pie3d',
            name: 'Объемная круговая'
        }, {
            id: 'column',
            name: 'Гистограмма'
        }, {
            id: 'column3d',
            name: 'Объемная гистограмма'
        }, {
            id: 'polar',
            name: 'Радиальная диаграмма'
        }/*, {
         id: 'solidgauge',
         name: 'Индикатор 100%'
         }, {
         id: 'gauge',
         name: 'Спидометр'
         },{
         id: 'bar',
         name: 'Горизонтальная гистограмма'
         },{
         id: 'stackedcolumn',
         name: 'Комбинированная гистограмма'
         }*/];

        self.config = WidgetWizard;
        self.chart = self.config.getChartBaseConfig(self.widget_settings);

        self.colorScheme = {};

        self.graphSelect = function (item) {
            self.config = WidgetWizard;
            self.widget_settings.graph_type = item.id;
            self.chart = self.config.getChartBaseConfig(self.widget_settings);
        };

        self.item = {};

        // directive callback function
        self.callbackQuery = function (item) {
            if (item.body !== self.widget_settings.query_body) {
                $scope.$broadcast('qb.reset');
                setRules(item.body);
            } else {
                return;
            }
        };

        //select list on fact base input-dropdown
        self.factBaseSelector = function (item) {
            if (item.body !== self.widget_settings.query_body) {
                self.widget_settings.query_body = item.body;
            } else {
                return;
            }
        };


        //wait and add lists to input-dropdown fact base
        dropdownLists
            .then(successGetLists, errorGetLists);

        function successGetLists(value) {
            value.queries.forEach(function (item) {
                self.factBaseLists.push({
                    readableName: item.readableName,
                    body: item.body,
                    type: item.type
                });
            });
        }

        function errorGetLists() {
            return;
        }

        //wait and add lists to input-dropdown event base
        dropdownQueries
            .then(successGetData, errorGetData);

        function successGetData(value) {
            value.queries.forEach(function (item) {
                self.dropdown.push({
                    readableName: item.readableName,
                    id: item.id,
                    created: item.created,
                    body: item.body,
                    description: item.description,
                    role_access: item.role_access
                });
            });
        }

        function errorGetData() {
            return;
        }

        self.objectMessage = '';

        self.queryObject = null;
        self.bofObject = null;

        //filter for input-dropdown
        self.filterObjectList = function (userInput) {
            var filter = $q.defer();
            var normalisedInput = userInput.toLowerCase();

            var filteredArray = self.dropdown.filter(function (query) {
                var matchQueryName = query.readableName.toLowerCase().indexOf(normalisedInput) === 0;

                return matchQueryName;
            });

            filter.resolve(filteredArray);
            return filter.promise;
        };

        self.checkDuration = function () {
            var updatePeriod = angular.element(
                document.querySelector('.period'));
            var duration = angular.element(
                document.querySelector('.duration'));
            return +duration.val() < +updatePeriod.val();
        };

        var updatePeriod;
        var duration;

        self.inputsInit = function () {
            updatePeriod = angular.element(
                document.querySelector('.period'));
            duration = angular.element(
                document.querySelector('.duration'));
            changeDisabledDuration();
        };

        function changeDisabledDuration() {
            if (updatePeriod) {
                updatePeriod.on('input', function () {
                    if (self.widget_settings.graph_type === 'pie' ||
                        self.widget_settings.graph_type === 'polar') {
                        duration.val(updatePeriod.val());
                        self.widget_settings.plot_duration = +duration.val();
                    }
                });
            }
        }

        //validation for chart step
        self.stepOne = function () {
            self.name = angular.element(document.querySelector('.name-form'));
            if (self.widget_settings.name === undefined) {
                self.name.$dirty = true;
                return false;
            }
            return true;
        };


        //sett qb when open edit-mode on wizard
        $timeout(function () {
            var steps = angular.element(document.querySelectorAll('.steps-indicator li a'));

            if (typeof self.widget_settings.query_body === 'string') {
                try {
                    self.widget_settings.query_body = angular.fromJson(self.widget_settings.query_body);
                } catch (err) {
                    self.sourceSwitch = true;
                }
            }
            steps.each(function (i, item) {
                var step = angular.element(item);
                if (step.text() === 'Данные') {
                    step.on('click', function () {
                        setRules(self.widget_settings.query_body);
                    });
                }
                if (step.text() === 'Результат') {
                    step.on('click', function () {
                        self.ok();
                    });
                }
            });
        }, 0);

        //set qb when next step click
        self.setQb = function (query_body) {
            if (query_body !== undefined && query_body !== '') {
                setRules(self.widget_settings.query_body);
            }
        };

        //send getRules event to angular-queryBuilder
        self.stepTwo = function (query_body) {
            if (typeof query_body === 'object' ||
                query_body === undefined) {
                $scope.$broadcast('qb.getRules');
            }
        };


        //resolve qb rules from angular-querybuilder
        $scope.$on('resolveGetRules', function (e, data) {
            self.widget_settings.query_body = data;
        });

        //validation for qb wizard step
        self.rulesValidation = function () {
            self.bof = angular.element(document.querySelector(
                ".bof-form"));
            var d = $q.defer();
            $timeout(function () {
                if ($filter('isEmpty')(self.widget_settings.query_body) &&
                    self.sourceSwitch === false) {
                    return d.resolve(false);
                }
                if (self.widget_settings.query_body === '' &&
                    self.sourceSwitch === true) {
                    self.bof.$dirty = true;
                    return d.resolve(false);
                }
                return d.resolve(true);
            }, 0);
            return d.promise;
        };

        //select event base and reset qb to undefined
        self.ebSelect = function (query_body) {
            self.bof = angular.element(document.querySelector(
                ".bof-form"));
            var inputFB = angular.element(document.querySelector(
                ".facts-base input"));
            if (typeof query_body === 'string') {
                inputFB.val('').trigger('change');
                self.widget_settings.query_body = undefined;
                self.bof.$dirty = false;
            }

        };

        //select base of facts and reset qb to empty string
        self.bofSelect = function (query_body) {
            var inputEb = angular.element(document.querySelector(
                ".event-base input"));
            if (typeof query_body === 'object' ||
                query_body === undefined) {
                inputEb.val('').trigger('change');
                $scope.$broadcast('qb.reset');
                self.widget_settings.query_body = '';

            }
        };

        // adding new widget
        self.ok = function () {
            self.resource = 'База событий';
            self.delta = 'Выключено';
            self.sort = 'Выключено';

            if (self.widget_settings.show_delta) {
                self.delta = 'Включено';
            }

            if (self.widget_settings.sort) {
                self.sort = 'Включено';
            }

            if (typeof self.widget_settings.query_body === 'string') {
                try {
                    JSON.parse(self.widget_settings.query_body);
                } catch (err) {
                    self.resource = 'База фактов';

                }
            }

            if (self.widget_settings.graph_type !== 'line') {
                self.widget_settings.facet.length = 0;
                var count = [],
                    facet = [];
                self.facetTypeErr = {
                    0: undefined,
                    1: undefined
                };
                self.facetCountErr = {
                    0: undefined,
                    1: undefined
                };
                for (var k in self.facetCounts) {
                    if (self.facetCounts[k]) {
                        var cErr = true;
                        count.push(self.facetCounts[k]);
                        self.facetCountErr[k] = cErr;
                    }
                }

                for (var j in self.facetItems) {
                    if (self.facetItems[j]) {
                        var idErr = true;
                        facet.push(self.facetItems[j].facetId);
                        self.facetTypeErr[j] = idErr;
                    }
                }

                self.facetsNestingLevel.forEach(function (item, i) {
                    if (facet.length === self.facetsNestingLevel.length &&
                        count.length === self.facetsNestingLevel.length) {
                        self.widget_settings.facet.push({
                            id: facet[i],
                            c: count[i]
                        })
                    }
                })
            }

            self.config = angular.element(document.querySelector('.config-form'));
            self.period = angular.element(document.querySelector('.period'));
            self.duration = angular.element(document.querySelector('.duration'));
            if (self.widget_settings.update_period === undefined ||
                self.widget_settings.plot_duration === undefined ||
                +self.widget_settings.plot_duration < +self.widget_settings.update_period) {
                self.period.$dirty = true;
                self.duration.$dirty = true;
                return false;
            }

            for (var i = 0; i < self.facetsNestingLevel.length; i++) {
                if (self.facetCountErr[i] === undefined || self.facetTypeErr[i] === undefined) {
                    return false;
                }
            }

            if (self.widget_settings.graph_type === 'line' ||
                self.widget_settings.graph_type === 'area') {
                self.widget_settings.facet = [{
                    id: 11,
                    c: 1
                }]
            }

            self.items.forEach(function (item) {
                if (self.widget_settings.graph_type === item.id) {
                    self.graph_type = item.name;
                }
            });

            return true;
        };

        if (self.widget_settings.name) {
            self.resource = 'База событий';
            self.delta = 'Выключено';
            self.sort = 'Выключено';
            if (self.widget_settings.graph_type !== 'line') {
                self.widget_settings.facet.length = 0;
                var count = [],
                    facet = [];
                for (var k in self.facetCounts) {
                    count.push(self.facetCounts[k])
                }

                for (var j in self.facetItems) {
                    facet.push(self.facetItems[j].facetId)
                }

                self.facetsNestingLevel.forEach(function (item, i) {
                    self.widget_settings.facet.push({
                        id: facet[i],
                        c: count[i]
                    })
                })
            }

            self.items.forEach(function (item) {
                if (self.widget_settings.graph_type === item.id) {
                    self.graph_type = item.name;
                }
            });

            if (self.widget_settings.show_delta) {
                self.delta = 'Включено';
            }

            if (self.widget_settings.sort) {
                self.sort = 'Включено';
            }

            if (typeof self.widget_settings.query_body === 'string') {
                try {
                    JSON.parse(self.widget_settings.query_body);
                } catch (err) {
                    self.resource = 'База фактов';

                }
            }

            self.createButtonText = 'Завершить';
        }

        self.confirmStep = function () {

            if (self.widget_settings.name == "") {
                self.widget_settings.name = "Новый виджет";
            }
            if (self.widget_settings.widget_type == "") {
                self.widget_settings.widget_type = 'standart';
            }

            $uibModalInstance.close(self.widget_settings);
        };


        self.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        self.showDataConfirm = false;

        self.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
    }


})();

