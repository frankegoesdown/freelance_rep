(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .factory('WidgetWizard', widgetWizard)
        .factory('FacetNesting', facetNesting);

    widgetWizard.$inject = ['$http', '$interval', 'moment', '$state'];
    facetNesting.$inject = ['$q', '$http', 'eventFields'];

    function widgetWizard($http, $interval, moment, $state) {

        function getChartConfig(settings) {

            function reflow() {
                var c = this;
                setTimeout(function () {
                    c.reflow();
                }, 0);
            }

            var graphType = settings.graph_type;

            if (graphType === 'line') {
                // basic line
                return {
                    options: {
                        chart: {
                            type: 'line',
                            height: 200,
                            events: {
                                load: reflow,
                                addSeries: reflow
                            },
                        },
                        tooltip: {
                            valueSuffix: '',
                            formatter: function () {
                                return "Количество: " + this.point.y + '<br/>' + Highcharts.dateFormat('%A, %b %e, %Y %H:%M:%S', this.x);
                            }
                        },
                        plotOptions: {
                            line: {
                                marker: {
                                    enabled: false,
                                    symbol: 'circle',
                                    radius: 2,
                                    states: {
                                        hover: {
                                            enabled: true
                                        }
                                    }
                                }
                            },
                            series: {
                                cursor: 'pointer'
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        },
                        legend: {
                            enabled: false
                        },
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'События'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Количество',
                        data: [{
                            x: 1479981600000,
                            y: 2000
                        }, {
                            x: 1479981900000,
                            y: 2000
                        }, {
                            x: 1479982200000,
                            y: 2000
                        }, {
                            x: 1479982500000,
                            y: 2000
                        }, {
                            x: 1479982800000,
                            y: 2000
                        }, {
                            x: 1479983100000,
                            y: 1500
                        }, {
                            x: 1479983400000,
                            y: 1500
                        }, {
                            x: 1479983700000,
                            y: 1700
                        }, {
                            x: 1479984000000,
                            y: 1800
                        }, {
                            x: 1479984300000,
                            y: 2000
                        }, {
                            x: 1479984600000,
                            y: 2000
                        }, {
                            x: 1479984900000,
                            y: 2000
                        }]
                    }]
                };
            }
            if (graphType === 'pie') {

                // pie chart
                return {
                    options: {
                        chart: {
                            type: 'pie',
                            height: 200,
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><br/>',
                            pointFormat: 'Количество: <b>{point.y}</b>'
                        },
                        title: {
                            text: ''
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: false
                                },
                                showInLegend: true
                            },
                            series: {
                                cursor: 'pointer'
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Количество',
                        data: [{
                            name: 'bufer',
                            y: 2000
                        }, {
                            name: 'parser',
                            y: 7455
                        }, {
                            name: 'index',
                            y: 6567
                        }, {
                            name: 'store',
                            y: 3543
                        }]
                    }],
                };
            }
            if (graphType === 'column') {

                // basic column
                return {
                    options: {
                        chart: {
                            type: 'column',
                            height: 200,
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px"> Время:{point.key}</span><br/>' +
                            '<span>' + '{series.name}' + '</span><br/><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">Количество: </td>' +
                            '<td style="padding:0"><b>{point.y}</b></td></tr>',
                            footerFormat: '</table>',
                            useHTML: true
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            },
                            series: {
                                cursor: 'pointer'
                            }
                        },
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'События'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'bufer',
                        data: [{
                            x: 1479981600000,
                            y: 2000
                        }, {
                            x: 1479981900000,
                            y: 2000
                        }, {
                            x: 1479982200000,
                            y: 2000
                        }, {
                            x: 1479982500000,
                            y: 2000
                        }, {
                            x: 1479982800000,
                            y: 2000
                        }, {
                            x: 1479983100000,
                            y: 1500
                        }, {
                            x: 1479983400000,
                            y: 1500
                        }, {
                            x: 1479983700000,
                            y: 1700
                        }, {
                            x: 1479984000000,
                            y: 1800
                        }, {
                            x: 1479984300000,
                            y: 2000
                        }, {
                            x: 1479984600000,
                            y: 2000
                        }, {
                            x: 1479984900000,
                            y: 2000
                        }]

                    }, {
                        name: 'parser',
                        data: [{
                            x: 1479981600000,
                            y: 2000
                        }, {
                            x: 1479981900000,
                            y: 3245
                        }, {
                            x: 1479982200000,
                            y: 4155
                        }, {
                            x: 1479982500000,
                            y: 5572
                        }, {
                            x: 1479982800000,
                            y: 7455
                        }, {
                            x: 1479983100000,
                            y: 7455
                        }, {
                            x: 1479983400000,
                            y: 3783
                        }, {
                            x: 1479983700000,
                            y: 2398
                        }, {
                            x: 1479984000000,
                            y: 3400
                        }, {
                            x: 1479984300000,
                            y: 3321
                        }, {
                            x: 1479984600000,
                            y: 2794
                        }, {
                            x: 1479984900000,
                            y: 2143
                        }]

                    }, {
                        name: 'index',
                        data: [{
                            x: 1479981600000,
                            y: 2000
                        }, {
                            x: 1479981900000,
                            y: 2000
                        }, {
                            x: 1479982200000,
                            y: 2000
                        }, {
                            x: 1479982500000,
                            y: 6567
                        }, {
                            x: 1479982800000,
                            y: 6500
                        }, {
                            x: 1479983100000,
                            y: 2100
                        }, {
                            x: 1479983400000,
                            y: 2203
                        }, {
                            x: 1479983700000,
                            y: 2203
                        }, {
                            x: 1479984000000,
                            y: 2300
                        }, {
                            x: 1479984300000,
                            y: 1843
                        }, {
                            x: 1479984600000,
                            y: 2044
                        }, {
                            x: 1479984900000,
                            y: 2000
                        }]

                    }, {
                        name: 'store',
                        data: [{
                            x: 1479981600000,
                            y: 2000
                        }, {
                            x: 1479981900000,
                            y: 1943
                        }, {
                            x: 1479982200000,
                            y: 2074
                        }, {
                            x: 1479982500000,
                            y: 3543
                        }, {
                            x: 1479982800000,
                            y: 50
                        }, {
                            x: 1479983100000,
                            y: 53
                        }, {
                            x: 1479983400000,
                            y: 48
                        }, {
                            x: 1479983700000,
                            y: 2145
                        }, {
                            x: 1479984000000,
                            y: 1800
                        }, {
                            x: 1479984300000,
                            y: 2365
                        }, {
                            x: 1479984600000,
                            y: 2074
                        }, {
                            x: 1479984900000,
                            y: 2032
                        }]

                    }]
                };
            }
            if (graphType === 'polar') {

                // wind rose
                return {
                    options: {
                        chart: {
                            polar: true,
                            type: 'area',
                            height: 200,
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            shared: false,
                            headerFormat: '<span style="font-size:10px">{point.key}</span><br/>',
                            pointFormat: '<span style="color:{series.color}"> Количество: <b>{point.y:,.0f}</b><br/>'
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                cursor: 'pointer'
                            }
                        },
                    },
                    title: {
                        text: ''
                    },
                    pane: {
                        size: '80%'
                    },
                    xAxis: {
                        min: 0,
                        categories: ['bufer', 'parser', 'index', 'store'],
                        tickmarkPlacement: 'on'
                    },
                    yAxis: {
                        lineWidth: 0
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'vertical'
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        type: 'area',
                        pointPlacement: 'on',
                        name: 'Количество',
                        data: [2000, 7455, 6567, 3543]
                    }]
                };
            }
            if (graphType === 'solidgauge') {

                // solid gauge
                return {
                    options: {
                        chart: {
                            type: 'solidgauge',
                            height: 200,
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            enabled: true,
                            valueSuffix: ' %'
                        },
                        pane: {
                            center: ['50%', '85%'],
                            size: '100%',
                            startAngle: -90,
                            endAngle: 90,
                            background: {
                                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                                innerRadius: '60%',
                                outerRadius: '100%',
                                shape: 'arc'
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    title: {
                        text: ''
                    },
                    // the value axis
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        stops: [
                            [0.3, '#55BF3B'], // green
                            [0.5, '#DDDF0D'], // yellow
                            [0.8, '#DF5353'] // red
                        ],
                        lineWidth: 0,
                        minorTickInterval: null,
                        tickPixelInterval: 400,
                        tickWidth: 0,
                        labels: {
                            y: 16
                        },
                        min: 0,
                        max: 100,
                        title: {
                            text: ''
                        }
                    },
                    series: [{
                        name: 'Загрузка',
                        data: [80],
                        dataLabels: {
                            format: '<div style="text-align:center;margin-bottom:-2px"><span style="font-size:12px;color:' +
                            (Highcharts.theme && Highcharts.theme.contrastTextColor) + '">{y}</span>' +
                            '</div>'
                        }
                    }]
                };
            } //dummy chart
            if (graphType === 'area') {

                // basic area
                return {
                    options: {
                        chart: {
                            type: 'area',
                            height: 200,
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            valueSuffix: '',
                            formatter: function () {
                                return "Количество: " + this.point.y + '<br/>' + Highcharts.dateFormat('%A, %b %e, %Y %H:%M:%S', this.x);
                            }
                        },
                        plotOptions: {
                            area: {
                                marker: {
                                    enabled: false,
                                    symbol: 'circle',
                                    radius: 2,
                                    states: {
                                        hover: {
                                            enabled: true
                                        }
                                    }
                                }
                            },
                            series: {
                                cursor: 'pointer'
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        },
                        legend: {
                            enabled: false
                        },
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Cобытия'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Количество',
                        data: [{
                            x: 1479981600000,
                            y: 2000
                        }, {
                            x: 1479981900000,
                            y: 2000
                        }, {
                            x: 1479982200000,
                            y: 2000
                        }, {
                            x: 1479982500000,
                            y: 2000
                        }, {
                            x: 1479982800000,
                            y: 2000
                        }, {
                            x: 1479983100000,
                            y: 1500
                        }, {
                            x: 1479983400000,
                            y: 1500
                        }, {
                            x: 1479983700000,
                            y: 1700
                        }, {
                            x: 1479984000000,
                            y: 1800
                        }, {
                            x: 1479984300000,
                            y: 2000
                        }, {
                            x: 1479984600000,
                            y: 2000
                        }, {
                            x: 1479984900000,
                            y: 2000
                        }]
                    }]
                };
            }
            if (graphType === 'gauge') {

                //angular gauge
                return {
                    options: {
                        chart: {
                            type: 'gauge',
                            height: 200,
                            plotBackgroundColor: null,
                            plotBackgroundImage: null,
                            plotBorderWidth: 0,
                            plotShadow: false,
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        pane: {
                            startAngle: -150,
                            endAngle: 150,
                            background: [{
                                backgroundColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, '#FFF'],
                                        [1, '#333']
                                    ]
                                },
                                borderWidth: 0,
                                outerRadius: '109%'
                            }, {
                                backgroundColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, '#333'],
                                        [1, '#FFF']
                                    ]
                                },
                                borderWidth: 1,
                                outerRadius: '107%'
                            }, {
                                backgroundColor: '#DDD',
                                borderWidth: 0,
                                outerRadius: '105%',
                                innerRadius: '103%'
                            }]
                        },
                        yAxis: {
                            min: 0,
                            max: 100,

                            minorTickInterval: 'auto',
                            minorTickWidth: 1,
                            minorTickLength: 10,
                            minorTickPosition: 'inside',
                            minorTickColor: '#666',

                            tickPixelInterval: 30,
                            tickWidth: 2,
                            tickPosition: 'inside',
                            tickLength: 10,
                            tickColor: '#666',
                            labels: {
                                step: 2,
                                rotation: 'auto'
                            },
                            title: {
                                text: '%'
                            },
                            plotBands: [{
                                from: 0,
                                to: 60,
                                color: '#55BF3B' // green
                            }, {
                                from: 60,
                                to: 80,
                                color: '#DDDF0D' // yellow
                            }, {
                                from: 80,
                                to: 100,
                                color: '#DF5353' // red
                            }]
                        },
                        tooltip: {
                            enabled: true,
                            valueSuffix: ' %'
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    title: {
                        text: ''
                    },
                    // the value axis
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Загрузка',
                        data: [60],
                        dataLabels: {
                            format: '<div style="text-align:center;margin-bottom:-2px"><span style="font-size:12px;color:' +
                            (Highcharts.theme && Highcharts.theme.contrastTextColor) + '">{y}</span>' +
                            '</div>'
                        }
                    }]
                };
            } //dummy chart
            if (graphType === 'facetedLine') {
                // line chart based on facets
                return {
                    options: {
                        chart: {
                            type: 'line',
                            height: 200,
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px"> Время:{point.key}</span><br/>' +
                            '{series.name}<table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">Количество: </td>' +
                            '<td style="padding:0"><b>{point.y}</b></td></tr>',
                            footerFormat: '</table>',
                            useHTML: true
                        },
                        plotOptions: {
                            line: {
                                marker: {
                                    enabled: false
                                }
                            },
                            series: {
                                cursor: 'pointer'
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'События'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'bufer',
                        data: [{
                            x: 1479981600000,
                            y: 2000
                        }, {
                            x: 1479981900000,
                            y: 2000
                        }, {
                            x: 1479982200000,
                            y: 2000
                        }, {
                            x: 1479982500000,
                            y: 2000
                        }, {
                            x: 1479982800000,
                            y: 2000
                        }, {
                            x: 1479983100000,
                            y: 1500
                        }, {
                            x: 1479983400000,
                            y: 1500
                        }, {
                            x: 1479983700000,
                            y: 1700
                        }, {
                            x: 1479984000000,
                            y: 1800
                        }, {
                            x: 1479984300000,
                            y: 2000
                        }, {
                            x: 1479984600000,
                            y: 2000
                        }, {
                            x: 1479984900000,
                            y: 2000
                        }]
                    }, {
                        name: 'parser',
                        data: [{
                            x: 1479981600000,
                            y: 2000
                        }, {
                            x: 1479981900000,
                            y: 3245
                        }, {
                            x: 1479982200000,
                            y: 4155
                        }, {
                            x: 1479982500000,
                            y: 5572
                        }, {
                            x: 1479982800000,
                            y: 7455
                        }, {
                            x: 1479983100000,
                            y: 7455
                        }, {
                            x: 1479983400000,
                            y: 3783
                        }, {
                            x: 1479983700000,
                            y: 2398
                        }, {
                            x: 1479984000000,
                            y: 3400
                        }, {
                            x: 1479984300000,
                            y: 3321
                        }, {
                            x: 1479984600000,
                            y: 2794
                        }, {
                            x: 1479984900000,
                            y: 2143
                        }]
                    }, {
                        name: 'index',
                        data: [{
                            x: 1479981600000,
                            y: 2000
                        }, {
                            x: 1479981900000,
                            y: 2000
                        }, {
                            x: 1479982200000,
                            y: 2000
                        }, {
                            x: 1479982500000,
                            y: 6567
                        }, {
                            x: 1479982800000,
                            y: 6500
                        }, {
                            x: 1479983100000,
                            y: 2100
                        }, {
                            x: 1479983400000,
                            y: 2203
                        }, {
                            x: 1479983700000,
                            y: 2203
                        }, {
                            x: 1479984000000,
                            y: 2300
                        }, {
                            x: 1479984300000,
                            y: 1843
                        }, {
                            x: 1479984600000,
                            y: 2044
                        }, {
                            x: 1479984900000,
                            y: 2000
                        }]
                    }, {
                        name: 'store',
                        data: [{
                            x: 1479981600000,
                            y: 2000
                        }, {
                            x: 1479981900000,
                            y: 1943
                        }, {
                            x: 1479982200000,
                            y: 2074
                        }, {
                            x: 1479982500000,
                            y: 3543
                        }, {
                            x: 1479982800000,
                            y: 50
                        }, {
                            x: 1479983100000,
                            y: 53
                        }, {
                            x: 1479983400000,
                            y: 48
                        }, {
                            x: 1479983700000,
                            y: 2145
                        }, {
                            x: 1479984000000,
                            y: 1800
                        }, {
                            x: 1479984300000,
                            y: 2365
                        }, {
                            x: 1479984600000,
                            y: 2074
                        }, {
                            x: 1479984900000,
                            y: 2032
                        }]
                    }
                    ]
                }
                    ;
            }
            /*if (graphType === 'bar') {

             // basic bar
             return {
             options: {
             chart: {
             type: 'bar',
             events: {
             load: reflow,
             addSeries: reflow
             }
             },
             tooltip: {
             valueSuffix: ' шт'
             },
             plotOptions: {
             bar: {
             dataLabels: {
             enabled: true
             }
             }
             },
             navigation: {
             buttonOptions: {
             enabled: false
             }
             }
             },
             title: {
             text: ''
             },
             xAxis: {
             categories: ['Rubicon', 'sshd', 'OSSEC', 'telnet', 'sudo']
             },
             yAxis: {
             min: 0,
             title: {
             text: 'События'
             },
             labels: {
             overflow: 'justify'
             }
             },
             legend: {
             layout: 'horizontal',
             align: 'center',
             verticalAlign: 'bottom',
             borderWidth: 0,
             backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
             shadow: false
             },
             credits: {
             enabled: false
             },
             series: [{
             name: '192.168.1.1',
             data: [107, 31, 635, 203, 2]
             }, {
             name: '192.168.1.100',
             data: [133, 156, 947, 408, 6]
             }, {
             name: '192.168.1.244',
             data: [1052, 954, 4250, 740, 38]
             }]
             };
             } //dummy chart*/
            if (graphType === 'pie3d') {
                return {
                    options: {
                        chart: {
                            type: 'pie',
                            height: 200,
                            options3d: {
                                enabled: true,
                                alpha: 45,
                                beta: 0
                            },
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            pointFormat: '{series.name}: {point.y}'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                depth: 35,
                                dataLabels: {
                                    enabled: false
                                },
                                showInLegend: true
                            },
                            series: {
                                cursor: 'pointer'
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        type: 'pie',
                        name: 'Количество',
                        data: [{
                            name: 'bufer',
                            y: 2000
                        }, {
                            name: 'parser',
                            y: 7455
                        }, {
                            name: 'index',
                            y: 6567
                        }, {
                            name: 'store',
                            y: 3543
                        }]
                    }]
                };
            }
            /*if (graphType === 'stackedcolumn') {

             // stacked column
             return {
             options: {
             chart: {
             type: 'column',
             events: {
             load: reflow,
             addSeries: reflow
             }
             },
             tooltip: {
             headerFormat: '<b>{point.x}</b><br/>',
             pointFormat: '{series.name}<br/> Количество :{point.y}<br/>Общее: {point.stackTotal}'
             },
             plotOptions: {
             column: {
             stacking: 'normal',
             dataLabels: {
             enabled: true,
             color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
             style: {
             textShadow: '0 0 3px black'
             }
             }
             }
             },
             navigation: {
             buttonOptions: {
             enabled: false
             }
             }
             },
             title: {
             text: ''
             },
             xAxis: {
             categories: ['Rubicon', 'SShd', 'OSSEC', 'Sudo', 'Tail']
             },
             yAxis: {
             min: 0,
             title: {
             text: 'События'
             },
             stackLabels: {
             enabled: true,
             style: {
             fontWeight: 'bold',
             color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
             }
             }
             },
             legend: {
             align: 'center',
             verticalAlign: 'bottom',
             backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
             borderWidth: 0,
             shadow: false
             },
             credits: {
             enabled: false
             },
             series: [{
             name: '192.155.19.22',
             data: [5, 3, 4, 7, 2]
             }, {
             name: '192.155.199.11',
             data: [2, 2, 3, 2, 1]
             }, {
             name: '244.15.19.100',
             data: [3, 4, 4, 2, 5]
             }, {
             name: '244.150.1.100',
             data: [4, 4, 6, 2, 1]
             }]
             };
             }*/ //dummy chart
            if (graphType === 'column3d') {
                return {
                    options: {
                        chart: {
                            type: 'column',
                            height: 250,
                            options3d: {
                                enabled: true,
                                alpha: 45,
                                beta: 5,
                                depth: 50,
                                viewDistance: 40
                            },
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        plotOptions: {
                            column: {
                                depth: 25
                            },
                            series: {
                                cursor: 'pointer'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px"> Время:{point.key}</span><br/>' +
                            '{series.name}<table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">Количество: </td>' +
                            '<td style="padding:0"><b>{point.y}</b></td></tr>',
                            footerFormat: '</table>',
                            useHTML: true
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime',
                        labels: {
                            rotation: -45
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'События'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'bufer',
                        data: [{
                            x: 1479982800000,
                            y: 2000
                        }]
                    }, {
                        name: 'parser',
                        data: [{
                            x: 1479982800000,
                            y: 7455
                        }]
                    }, {
                        name: 'index',
                        data: [{
                            x: 1479982800000,
                            y: 6500
                        }]
                    }, {
                        name: 'store',
                        data: [{
                            x: 1479982800000,
                            y: 50
                        }]
                    }]
                };
            }
        }

        return {
            getChartBaseConfig: getChartConfig
        };

    }

    function facetNesting($q, $http, eventFields) {

        var facetLevel = function () {
            this.facetFields = [];
            this.facetCounts = 1;
        };

        facetLevel.prototype.getFields = function () {
            var defered = $q.defer();
            var fields = [];
            eventFields.getFaceted().then(function (data) {
                for (var k in data) {
                    fields.push({
                        facetId: data[k].id,
                        id: data[k].name,
                        name: data[k].label
                    });
                }
                defered.resolve(fields)
            });
            this.facetFields = fields;
            return defered.promise;
        }
        
        return facetLevel;
    }
})();
