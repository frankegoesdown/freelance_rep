(function() {
    'use strict';

    angular.module('KomradeApp.corres', [
        'KomradeApp.core',
        'ngAnimate',
        'highcharts-ng',
        'ncy-angular-breadcrumb',
        'ui.bootstrap',
        'ui-notification',
        'ngFileSaver',
        'angularMoment'
    ]);

})();
