(function () {
    'use strict';

    angular
        .module('KomradeApp.corres')
        .controller('corresController', corresCtrl);

    corresCtrl.$inject = ['$anchorScroll', '$location', '$scope', '$http', '$templateCache',
        'FileSaver', 'Blob', '$state', '$q'];

    function corresCtrl($anchorScroll, $location, $scope, $http, $templateCache, FileSaver, Blob,
                        $state, $q) {
        $scope.gotoAnchor = function (x) {
            var newHash = 'anchor' + x;
            if ($location.hash() !== newHash) {
                // set the $location.hash to `newHash` and
                // $anchorScroll will automatically scroll to it
                $location.hash('anchor' + x);
            } else {
                // call $anchorScroll() explicitly,
                // since $location.hash hasn't changed
                $anchorScroll();
            }
        };


        /*$scope.config = {
            autoHideScrollbar: true
        }*/
        // , root_ref for changing highchart data
        $scope.switchImpl = function (id, root_index) {
            return $http.post('/correspondences/switchimpl',
                JSON.stringify({
                    id: id,
                    state: !$scope['impl' + id]
                })).success(function (data) {
                if ($scope['impl' + id]) {
                    $scope.implemented[root_index] -= 1;
                    $scope.not_impl[root_index] += 1;
                } else {
                    $scope.implemented[root_index] += 1;
                    $scope.not_impl[root_index] -= 1;

                }
                $scope['impl' + id] = !$scope['impl' + id]
            }.bind(this))
                .error(function (data) {
            }.bind(this))
        }

        $scope.switchSl = function (id, root_index) {
            if ($scope['sl' + id] && $scope['impl' + id]){
                $scope.switchImpl(id, root_index);
            }
            $http.post('/correspondences/switchsl',
                JSON.stringify({
                    id: id,
                    state: !($scope['sl' + id])
                })).success(function (data) {
                    if ($scope['sl' + id]) {
                        $scope.selected[root_index] += 1;
                        if ($scope['impl' + id]) {
                            //$scope.implemented[root_index] -= 1;
                            //$scope['impl' + id] = !$scope['impl' + id]
                        } else {
                            //$scope.not_impl[root_index] -= 1;
                        }
                    } else {
                        $scope.selected[root_index] -= 1;
                        //$scope.not_impl[root_index] += 1;
                    }
                    $scope['sl' + id] = !($scope['sl' + id]);
            }.bind(this))
                .error(function (data) {
                }.bind(this))
                
        }

        $scope.setComment = function (id, new_data) {
            $http.post('/correspondences/comment',
                JSON.stringify({
                    id: id,
                    comment: $scope['comment' + id]
                })).success(function (data) {
            }.bind(this))
                .error(function (data) {
                }.bind(this))
        }

        $scope.$on('$destroy', function () {
            // Make sure that the interval is destroyed too
            $templateCache.removeAll();
        });

        $scope.$on('$viewContentLoaded', function () {
            // Make sure that the interval is destroyed too
        });

        $scope.chartInit = function (config) {
            var query = $q.defer();
            var chartConf = config;
            query.resolve(chartConf);
            return query.promise;
        }

        $scope.scrollToSection = function (sec) {
            $location.hash(sec);
            $anchorScroll();
        }

        // retrieve data from template
        $scope.$on('templateHaveJson', function (event, obj) {
            $scope.axes = obj[0];
            $scope.selected = obj[1];
            $scope.implemented = obj[2];
            $scope.not_impl = obj[3];
            $scope.chartInit($scope.chartConfig).then(resSuccess);

            function resSuccess(result) {
                var chart = result.getHighcharts();
                chart.xAxis[0].setCategories($scope.axes);
                $scope.chartConfig.series.push({
                    name: 'Не применяется',
                    data: $scope.selected,
                    color: '#e9585b'
                })
                $scope.chartConfig.series.push({
                    name: 'Не реализовано',
                    data: $scope.not_impl,
                    color: '#f7a35c'
                })
                $scope.chartConfig.series.push({
                    name: 'Реализовано',
                    data: $scope.implemented,
                    color: '#42b382'
                });
            }
            
        });

        // $scope.exportData = function () {
        //     var pdf = new jsPDF('p', 'pt', 'a4');
        //     pdf.addHTML(document.body, function () {
        //         pdf.save('*.pdf');
        //     });

        // };

        $scope.chartConfig = {
            options: {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column'
                },
                tooltip: {
                    //shared: true,
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Всего: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                            style: {
                                textShadow: '0 0 3px black'
                            }
                        }
                    },
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (e) {
                                    var newHash = $scope.axes[this.x];
                                    $location.hash(newHash);
                                    $anchorScroll();
                                }
                            }
                        },
                        marker: {
                            lineWidth: 1
                        }
                    }
                },
            },
            title: {
                text: ''
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            series: []
        }
    }

})();
