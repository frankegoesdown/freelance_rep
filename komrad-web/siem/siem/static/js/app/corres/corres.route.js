(function () {
    'use strict';

    angular
        .module('KomradeApp.corres')
        .config(uiRouterConfig);


    uiRouterConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

    function uiRouterConfig($stateProvider, $urlRouterProvider, $httpProvider) {
        var labels = {
            correspondences: 'ГОСТ Р ИСО/МЭК 27001-2006'
        }
        $stateProvider
            .state('correspondences', {
                url:'/correspondences',
                templateUrl: '/correspondences',
                ncyBreadcrumb: {
                    label: labels.correspondences
                },
                controller: 'corresController',
                data:{
                    docFile: "event_listener/corres.pdf"
                }
            })


    }



})();

