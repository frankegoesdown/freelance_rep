/**
 * Applivation root
 */
(function() {
    'use strict';

    angular.module('KomradeApp', [
        'KomradeApp.core',
        'KomradeApp.event_listener',
        'KomradeApp.corres',
        'KomradeApp.configs',
        'KomradeApp.correl',
        'KomradeApp.bof',
        'KomradeApp.av_monitoring'
    ]);

})();