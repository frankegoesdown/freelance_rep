$.fn.queryBuilder.define('datetimepicker', function (options) {

    this.on('getRuleInput.filter', function (h, rule, name) {

        var filter = rule.filter;

        if (filter.type == 'datetime') {
            var datetimeFormat = 'DD/MM/YYYY, HH:mm:ss';

            function dateTimeQBSetup(rule) {
                var input = rule.$el.find('.rule-value-container input');
                input.datetimepicker({
                    allowInputToggle: true,
                    format: datetimeFormat,
                    locale: 'ru'
                });
                input.blur()
            }

            setTimeout(function () {
                dateTimeQBSetup(rule);
            }, 100)


            function valueSetter(rule, value) {
                var element = rule.$el.find('.rule-value-container input');
                if (rule.operator.type === 'between' || rule.operator.type === 'not_between') {
                    var values = [];
                    element.each(function (index, item) {
                        var new_value = moment.unix(rule.value[index]).format(datetimeFormat);
                        var input = $(item);
                        values.push(new_value);
                        $(this).val(new_value);
                    })

                    return values;
                }
                var new_value = moment.unix(value).format(datetimeFormat);
                element.val(new_value);
            }

            function valueGetter(rule) {
                //setTimeout(function() {}, 10);
                var inputs = rule.$el.find('.rule-value-container input');
                if (rule.operator.type === 'between' || rule.operator.type === 'not_between') {
                    var range = [];
                    inputs.each(function (index, item) {
                        var input = $(item);
                        $(this).on('blur', function () {
                            $(this).change();
                        });

                        range.push(moment(input.val(), datetimeFormat).unix());
                    });
                    return range;
                }
                inputs.on('blur', function () {
                    inputs.change();
                });
                var value = moment(inputs.val(), datetimeFormat).unix();
                return value
            }

            function validation(datetime) {
                var emptyErr = 'неверное заполнение',
                    input = rule.$el.find('.rule-value-container input'),
                    hasError = true;
                input.each(function (index, item) {
                    var singleInp = $(item);
                    if (singleInp.val() !== undefined &&
                        singleInp.val() !== '') {
                        hasError = false;
                    } else {
                        hasError = true;
                    }
                });

                if (!hasError) {
                    return true;
                } else {
                    return emptyErr;
                }

            }

            filter.validation = {
                callback: validation
            };
            filter.valueGetter = valueGetter;
            filter.valueSetter = valueSetter;

        }
    });


}, {
    font: 'glyphicons',
    color: 'default'
});