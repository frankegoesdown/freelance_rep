var filters_inherit = [];

function  modifyFiltersForInherit(fil) {
    for (var i in fil) {
        fil[i]['input'] = inheritQBInput;
        fil[i]['valueGetter'] = inheritQBGetter;
        fil[i]['valueSetter'] = inheritQBSetter;
        fil[i]['operators'] = ['equal', 'not_equal'];
    }

    return fil;
}

$.fn.directiveConstructor = function (data, data2) {
    if (typeof (data) !== "undefined") {
        if (typeof (data) === "object") {
            filters = data;
        } else if (typeof (data) === "string") {
            if (data === "getDirective") {
                return saveDirective(false);
            } else if (data === "getDirectiveResolved") {
                return saveDirective(true);
            } else if (data === "setDirective") {
                restoreDirective(data2);
            } else if (data === "clearAll") {
                removeDirective();
                restoreDirective(data2);
                // создать правило, заполнить данными 
                var node = $('#dc-content');
                var new_node = addListToRule(node, 0);

                createRule(new_node, "Правило #0");
            }

            return;
        }
    }

    this.append(template_dc);

    filters_inherit = $.extend(true, [], filters);
    filters_inherit = modifyFiltersForInherit(filters_inherit);

    $('#sortable').nestedSortable({
        forcePlaceholderSize: true,
        handle: '.rule-handler',
        items: 'li',
        opacity: .5,
        placeholder: 'ui-state-highlight',
        revert: 250,
        tabSize: 25,
        tolerance: 'pointer',
        toleranceElement: '> div',
        isTree: true,
        startCollapsed: false,
        distance: 30,
        protectRoot: true,
        cursor: "move"
    });

    $(".rule-default").each(function () {
        createRule($(this), 'Правило #' + count);
    });
    
    // Обновить цвет для absence-правил
    //jQuery('.absence-checkbox').each(function(index, element){
    //    $(this).change();
    //});
    //$(".absence-checkbox").each(function(i, el){$(this).change();});

    return this;
};

var count = 0;


function createRule(node, name) {
    node.append(template_rule);
    node.find('.rule-name').val(name);

    createQBFilter(node.find('.qb-filter'));
    createQBInherit(node.find('.qb-inherit').hide());
    createQBModal(node.find('.qb-modal').hide());

    node.find(".collapse-two").click(onCollapseClick);
    node.find(".button-add").click(onButtonAddClick);
    node.find(".button-copy").click(onButtonCopyClick);
    node.find(".button-delete").click(onButtonDeleteClick);
    node.find(".absence-checkbox").change(onAbsenceCheck);

    // хак для подсветки фильтра
    //node.find(".button-filter").click(onButtonFilterClick);    
    node.find(".button-filter").children('span').css('color', '#FFF');
    node.find(".button-inherit").click(onButtonInheritClick);
    node.find(".button-modal").click(onButtonModalClick);

    // запретить кнопку наследования для правила первого уровня 
    if (typeof (node.closest('li').parent().closest('li').attr('id')) === "undefined") {
        node.find(".button-inherit").addClass('disabled');
    }

    makeHover(node.find(".hover-icons"));

    node.find("[data-toggle='tooltip']").tooltip();

    node.find("#rule-header-static").hide();

    count += 1;
}

function createQBFilter(node) {
    node.queryBuilder({
        filters: filters,
        lang_code: 'ru',
        plugins: ['mapping2', 'sortable', 'list_search']
    });
    
    node.on('afterCreateRuleInput.queryBuilder', function(e, rule) {
        if (rule.filter.id === 'timestamp_event' ||
            rule.filter.id === 'timestamp') {
          var $input = rule.$el.find('.rule-value-container [name*=_value_]');
          $input.on('dp.change', function() {
              $input.trigger('change');
          });
        }
    });
}

function createQBInherit(node) {
    node.queryBuilder({
        filters: filters_inherit,
        lang_code: 'ru',
        plugins: ['mapping2', 'sortable', 'list_search'],
        allow_empty: true,
        allow_groups: false,
        //rules: [],
        conditions: ['AND']
    });
}

function createQBModal(node) {
    node.queryBuilder({
        filters: filters,
        lang_code: 'ru',
        plugins: ['mapping2', 'sortable', 'list_search'],
        allow_empty: true,
        allow_groups: false,
        rules: [],
        conditions: ['AND']
    });

    /* 
     * Используем от QB только рамку, логика своя
     * и основана на selectize.js
     */
    createSelectize(node, []);
}

function createSelectize(node, vals) {
    if (typeof (node.find('.rules-group-header')) !== "undefined")
        node.find('.rules-group-header').remove();


    var element_html = '<select multiple class="modal-selectize">';
    for (var i in filters) {
        element_html = element_html.concat('<option value="' + filters[i]['id'] + '">' + filters[i]['label'] + '</option>');
    }
    element_html = element_html.concat('</select>');


    if (node.find('.modal-selectize').length !== 0) {
        node.find('.modal-selectize').remove();
        node.find('.rules-group-container').append(element_html);
    } else {
        node.children('dl').append(element_html);
    }



    node.find('.modal-selectize').selectize({
        create: false,
        items: vals,
        plugins: ['remove_button'],
        delimiter: ',',
        persist: false
    });
}

function makeHover(node) {
    node.hover(
            function () {
                $(this).css('color', '#fff');
            },
            function () {
                $(this).css('color', '#444;');
            }
    );
}

function onAbsenceCheck() {
    if (this.checked){
        $(this).closest(".panel-heading").css('background', '#76828E');
        $(this).closest(".panel-primary").css('border-color', '#76828E');
        $(this).closest(".panel-heading").find(".input-timer-ti")[0].style.setProperty('background-color', '#76828E', 'important');
    } else {
        $(this).closest(".panel-heading").css('background', '#63A8EB');    
        $(this).closest(".panel-primary").css('border-color', '#63A8EB');
        $(this).closest(".panel-heading").find(".input-timer-ti")[0].style.setProperty('background-color', '#63A8EB', 'important');
    }
}

function onCollapseClick() {
    $(this).closest(".panel").find(".panel-body").slideToggle(0);

    // скопировать редактируемый заголовок в статический
    var panel = $(this).closest(".panel");
    panel.find("#rule-header-static").find("#rule-name").text(panel.find("#rule-header-editable").find("#rule-name").val());
    panel.find("#rule-header-static").find("#rule-timer").text(panel.find("#rule-header-editable").find("#rule-timer").val());
    panel.find("#rule-header-static").find("#rule-timer-select").text(panel.find("#rule-header-editable").find("#rule-timer-select option:selected").text().toLowerCase().substring(0, 3).concat('.'));
    panel.find("#rule-header-static").find("#rule-count").text(panel.find("#rule-header-editable").find("#rule-count").val());
    panel.find("#rule-header-static").find("#rule-description").text(panel.find("#rule-header-editable").find("#rule-description").val());

    // обменять шапки местами        
    $(this).closest(".panel").find("#rule-header-editable").slideToggle(0);
    $(this).closest(".panel").find("#rule-header-static").slideToggle(0);

    var class_name = $(this).attr('class');
    $(this).fadeOut(100);
    if (class_name.indexOf("fa-chevron-down") > -1) {
        $(this).switchClass("fa-chevron-down", "fa-chevron-up");
    } else if (class_name.indexOf("fa-chevron-up") > -1) {
        $(this).switchClass("fa-chevron-up", "fa-chevron-down");
    }
    $(this).fadeIn(100);
}

/*
 * node - это parent ближайшего .rule-default
 */
function addListToRule(node, list_id, root) {
    if (typeof (list_id) === "undefined")
        list_id = count;
    else 
        if (list_id >= count) 
            count = parseInt(list_id);

    if (node.find('ol').length === 0)
        node.append(template_tree_node);
    else
        node.children('ol').append(template_tree_node_just_li);


    node.find('#list_').attr("id", "list_" + list_id);
    node.find('#list_' + list_id).children().addClass("rule-n-" + list_id);

    $('#sortable').nestedSortable('refresh');

    return $(".rule-n-" + list_id);
}

function onButtonAddClick() {
    var node = $(this).closest(".rule-default").parent();

    /* Попытка обойти can't find left or undefined */
    $('ul .list-group').remove()
    var new_node = addListToRule(node);

    createRule(new_node, "Правило #" + count);
}

function onButtonCopyClick() {
    var node = $(this).closest(".rule-default").parent();

    var new_node = addListToRule(node);

    createRule(new_node, "Правило #" + count);

    /*
     * Хак используется для снятия фокуса с элементов QueryBuilder.
     * Т. к. кнопка копирования не является кнопкой по факту, то 
     * при нажатии на неё не снимается фокус со всех остальных элементов 
     * ввода. 
     * Если не снять фокус, значение не зафиксируется, и не пройдёт 
     * валидация данных в QB. Соответственно не получится вытащить эти 
     * данные.
     */
    node.find('.form-control').blur();

    // получить id правила
    var id = node.closest('li').attr('id').substr('list_'.length);
    var id_new = new_node.closest('li').attr('id').substr('list_'.length);

    // скопировать все свойства оригинального правила в новое правило 
    // query-builder's       
    var qb_f = getQueryFromQBbyId(id, 'filter');
    var qb_i = getQueryFromQBbyId(id, 'inherit');
    var qb_m = getSelectizeValues(id);

    //if (isQBCheckboxChecked(id, 'filter')){
    setQBbyId(id_new, 'filter', qb_f);
    setQBCheckboxChecked(id_new, 'filter', isQBCheckboxChecked(id, 'filter'));
    //}

    if (isQBCheckboxChecked(id, 'inherit')) {
        setQBbyId(id_new, 'inherit', qb_i);
        setQBCheckboxChecked(id_new, 'inherit', isQBCheckboxChecked(id, 'inherit'));
    }

    if (isQBCheckboxChecked(id, 'modal')) {
        setQBCheckboxChecked(id_new, 'modal', isQBCheckboxChecked(id, 'modal'));
        setSelectizeValues(id_new, qb_m);
    }

    // Свойства из шапки 
    setCountById(id_new, getCountById(id));
    setNameById(id_new, getNameById(id) + '-копия');
    setDescriptionById(id_new, getDescriptionById(id));
    var timer = getTimerRawById(id);
    setTimerRawById(id_new, timer['val'], timer['select']);
}

function onButtonDeleteClick() {
    var node = $(this);
    console.log(bootbox)
    bootbox.setLocale("ru");
    bootbox.confirm({
        size: 'small',
        title: 'Удаление правила',
        message: 'Удалить правило?',
        buttons: {'confirm':{ label: 'Удалить', className: 'btn-danger'}, 'cancel': {label:'Отмена'}},
        callback: function (result) {
            if (result === true)
                node.closest('li').remove();
        }
    });
    
}

function fixFiltersButtonColor(node) {
    if (node.hasClass('active')) {
        node.children('span').css('color', '#444');
    } else {
        node.children('span').css('color', '#FFF');
    }
}

function onButtonFilterClick() {
    $(this).closest('table').find('.qb-filter').slideToggle(0);
    fixFiltersButtonColor($(this));
}

function onButtonInheritClick() {
    var disabled = $(this).hasClass('disabled')
    if (disabled)
        return;

    $(this).closest('table').find('.qb-inherit').slideToggle(0);
    fixFiltersButtonColor($(this));
}

function onButtonModalClick() {
    $(this).closest('table').find('.qb-modal').slideToggle(0);
    fixFiltersButtonColor($(this));
}



/*
 * СОХРАНЕНИЕ/ЗАГРУЗКА СОСТОЯНИЯ
 */

function saveDirective(resolved) {
    if (validateQB('filter') === false)
        return false;

    if (validateQB('inherit') === false)
        return false;

    if (validateQB('modal') === false)
        return false;

    // 1. Получить иерархию
    // 2. Вытащить данные
    // 3. Запихать в дерево
    var arr = $('#sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
    return dumpTree(arr, resolved);
}
;



function timerToSeconds(sec, ti) {
    var multiplier = 1;
    if (ti === "Minutes")
        multiplier = 60;
    else if (ti === "Hours")
        multiplier = 60 * 60;
    else if (ti === "Days")
        multiplier = 60 * 60 * 24;

    return sec * multiplier;
}

function secondsToTimer(sec, ti) {
    var multiplier = 1;
    if (ti === "Minutes")
        multiplier = 60;
    else if (ti === "Hours")
        multiplier = 60 * 60;
    else if (ti === "Days")
        multiplier = 60 * 60 * 24;

    return sec / multiplier;
}

function recursiveRestore(node, data, id) {
    if (typeof (data) === "object") {
        if ('rule' in data) {
            // создать правило, заполнить данными 
            $('ul .list-group').remove();
            var new_node = addListToRule(node, id);
            $('ul .list-group').remove();

            var modifiers = data['modifiers'];

            createRule(new_node, modifiers['name']);
            setTimerRawById(id, secondsToTimer(modifiers['timer'], modifiers['timer_select']), modifiers['timer_select']);
            setDescriptionById(id, modifiers['description']);
            setCountById(id, modifiers['count']);
            setQBbyId(id, 'filter', data['rule']);
            if ('inherit' in modifiers)
                setQBbyId(id, 'inherit', modifiers['inherit']);
            if ('modal' in modifiers)
                setSelectizeValues(id, modifiers['modal']);

            if ('absence' in modifiers){
                //setDescriptionById(id, 'absence ' + modifiers['absence']);
                setAbsenceById(id, modifiers['absence']); 
            }
                

            // восставновить состояние кнопок
            setQBCheckboxChecked(id, 'filter', modifiers['filter_button_checked']);
            setQBCheckboxChecked(id, 'inherit', modifiers['inherit_button_checked']);
            setQBCheckboxChecked(id, 'modal', modifiers['modal_button_checked']);

            //
            if ('childs' in data) {
                for (var i in data['childs']) {
                    recursiveRestore(new_node.closest('.rule-default').parent(), data['childs'][i], i);
                }
            }
        } else {
            for (var i in data) {
                recursiveRestore(node, data[i], i);
            }
        }
    }
}

function removeDirective() {
    $('#dc-content').find('li').remove();
}

function restoreDirective(data) {
    // очистить поле
    count = 0;
    removeDirective();
    // загрузить данные
    recursiveRestore($('#dc-content'), data);
}

function validateQB(name) {
    var validated = true;
    $(".qb-" + name).each(function () {
        var id = $(this).closest('li').attr('id').substring('list_'.length);
        /*
         * TODO: выработать политику включения/выключения фильтра. 
         * Пока что тут хак. 
         */
        if (isQBCheckboxChecked(id, name) || name === "filter") {
            var req = $(this).queryBuilder("validate");
            if (req === false)
                validated = false;
        }
    });
    return validated;
}

function dumpTree(arr, resolved, level) {
    var json = {};
    if (!level)
        level = 0;

    var id = "_";
    if (typeof (arr) === "object") {
        if ('id' in arr) {
            id = arr['id'];
            json[id] = {};
            json[id]['rule'] = getQueryFromQBbyId(id, 'filter', resolved);

            // Модификаторы дописываются к каждому правилу 

            var timer_r = getTimerRawById(id);
            var seconds = timerToSeconds(timer_r['val'], timer_r['select'])

            json[id]['modifiers'] = {
                'count': getCountById(id),
                'timer': seconds,
                'timer_select': timer_r['select'],
                'name': getNameById(id),
                'description': getDescriptionById(id),
                'filter_button_checked': isQBCheckboxChecked(id, 'filter'),
                'inherit_button_checked': isQBCheckboxChecked(id, 'inherit'),
                'modal_button_checked': isQBCheckboxChecked(id, 'modal')
            };
            
            json[id]['modifiers']['absence'] = isAbsenceChecked(id);
            
            

            /* Наследование дописывается к каждому правила
             * Но только если нажат чекбокс
             */

            if (isQBCheckboxChecked(id, 'inherit')) {
                json[id]['modifiers']['inherit'] = {};

                var q = getQueryFromQBbyId(id, 'inherit', resolved);
                json[id]['modifiers']['inherit'] = q;
            }

            if (isQBCheckboxChecked(id, 'modal')) {
                json[id]['modifiers']['modal'] = getSelectizeValues(id);
            }

        } else {
            // Исключительно для первой итерации.
            // подразумевается, что в дереве один корень
            for (var item in arr) {
                var value = arr[item];
                return dumpTree(value, resolved);
            }
        }

        if ('children' in arr) {
            json[id]['childs'] = {};
            for (var item in arr['children']) {
                var value = arr['children'][item];
                json[id]['childs'] = $.extend(json[id]['childs'], dumpTree(value, resolved));
            }
        }
    }
    return json;
}

/*
 * SETTERS/GETTERS
 */
function getQueryFromQBbyId(id, type, resolved) {
    if (resolved === true){    
        return $("#list_" + id).children('div').children('div').find('.qb-' + type).queryBuilder("getRulesResolved");
    }
    
    return $("#list_" + id).children('div').children('div').find('.qb-' + type).queryBuilder("getRules");
}

function isAbsenceChecked(id) {
    return $("#list_" + id).children('div').find('.absence-checkbox').prop('checked');
}

function setQBbyId(id, type, data) {
    $("#list_" + id).children('div').children('div').find('.qb-' + type).queryBuilder("setRules", data);
}

function getCountById(id) {
    return $("#list_" + id).children('div').find('.input-count').val();
}

function setCountById(id, val) {
    $("#list_" + id).children('div').find('.input-count').val(val);
}

function getTimerById(id) {
    var ti = $("#list_" + id).children('div').find('.input-timer-ti').val();
    var multiplier = 1;
    if (ti === "Minutes")
        multiplier = 60;
    else if (ti === "Hours")
        multiplier = 60 * 60;
    else if (ti === "Days")
        multiplier = 60 * 60 * 24;

    return $("#list_" + id).children('div').find('.input-timer').val() * multiplier;
}

function getTimerRawById(id) {
    var val = $("#list_" + id).children('div').find('.input-timer').val();
    var ti = $("#list_" + id).children('div').find('.input-timer-ti').val();
    return {
        'val': val,
        'select': ti
    };
}

function setTimerRawById(id, val, select) {
    $("#list_" + id).children('div').find('.input-timer-ti').val(select);
    $("#list_" + id).children('div').find('.input-timer').val(val);
}

function getNameById(id) {
    return $("#list_" + id).children('div').find('.rule-name').val();
}

function setNameById(id, val) {
    $("#list_" + id).children('div').find('.rule-name').val(val);
}

function getDescriptionById(id) {
    return $("#list_" + id).children('div').find('.rule-description').val();
}

function setDescriptionById(id, val) {
    $("#list_" + id).children('div').find('.rule-description').val(val);
}

function setAbsenceById(id, val) {
    $("#list_" + id).children('div').find('.absence-checkbox').prop('checked', val);
    if (val === true){
        $("#list_" + id).children('div').find('.absence-checkbox').closest(".panel-heading").css('background', '#76828E');
        $("#list_" + id).children('div').find('.absence-checkbox').closest(".panel-primary").css('border-color', '#76828E');
        $("#list_" + id).children('div').find('.absence-checkbox').closest(".panel-heading").find(".input-timer-ti")[0].style.setProperty('background-color', '#76828E', 'important');
    }
}

function getSelectizeValues(id) {
    var modal_ids = [];

    var node = $("#list_" + id).children('div');

    node.find('.modal-selectize :selected').each(function (i, selected) {
        modal_ids[i] = $(selected).val();
    });

    return modal_ids;
}

function setSelectizeValues(id, values) {
    var modal_ids = [];

    var node = $("#list_" + id).children('div').find('.qb-modal');

    createSelectize(node, values);
}

/*
 * НАСТРОЙКИ QB ДЛЯ НАСЛЕДОВАНИЯ
 */

function inheritQBInput(rule, name) {
    var filter_selected = $('#' + rule['id']).children('.rule-filter-container').children('select').val();

    var element_html = '<select name="' + name + '_1" class="form-control">';
    for (var i in filters) {
        var sel = '';
        if (filters[i]['id'] === filter_selected)
            sel = 'selected';
        element_html = element_html.concat('<option value="' + filters[i]['id'] + '" ' + sel + '>' + filters[i]['label'] + '</option>');
    }
    element_html = element_html.concat('</select><select name="' + name + '_2" class="form-control">');

    var node = $('#' + rule['id']).parent().closest('li');

    /*
     * Запрашиваем второе по дальности 'li' для того, чтобы пропустить 
     * текущее правило, и не добавлять его в список
     */
    node = node.parent().closest('li');
    /*
     * Бегаем по вышестоящим правилам, чтобы составить список правил, 
     * от которых можно наследоваться
     */
    while (typeof (node.attr("id")) !== "undefined") {
        var node_id = node.attr("id");
        var id_sub = node_id.substring('list_'.length);
        element_html = element_html.concat('<option value="' + id_sub + '">' + getNameById(id_sub) + '</option>');
        node = node.parent().closest('li');
    }

    element_html = element_html.concat('</select>');

    return element_html;
}

function inheritQBGetter(rule) {
    return [rule.$el.find('.rule-value-container [name$=_1]').val()
                , rule.$el.find('.rule-value-container [name$=_2]').val()];
}

function inheritQBSetter(rule, value) {
    if (rule.operator.nb_inputs > 0) {
        rule.$el.find('.rule-value-container [name$=_1]').val(value[0]).trigger('change');
        rule.$el.find('.rule-value-container [name$=_2]').val(value[1]).trigger('change');
    }
}

/*
 * STUFF
 */

function isQBCheckboxChecked(id, type) {
    var node = $("#list_" + id).children('div').find('.button-' + type);
    return node.hasClass('active') && (!node.hasClass('disabled'));
}

function setQBCheckboxChecked(id, type, status) {
    var node = $("#list_" + id).children('div').find('.button-' + type);
    node.children('input').prop("checked", status);
    if (status === true) {
        node.click();
        node.addClass('active');
    }
}

var template_dc = '\
        <div id="dc-content">\
            <ol id="sortable" class="list-unstyled">\
                <li class="list-unstyled" id="list_0"><div class="rule-default"></div></li>\
            </ol>\
        </div>';

var template_rule = '<div class="panel panel-primary panel-fix" id="panel">\
                <div class="panel panel-primary panel-fix">                           \
                    <div class="panel-heading">\
                        <table width="100%"><tr>\
                            <td class="rule-handler">\
                                <span class="fa fa-arrows icons-fix" aria-hidden="true"/>\
                            </td>\
                            <td width="1px">\
                                <span class="vertical-separator"  style="margin-left: 0px;" aria-hidden="true"/>\
                            </td>\
                            <td>\
                                <table width="100%" id="rule-header-editable"><tr>\
                                    <td width = "20px">\
                                        <span class="fa fa-pencil icons-fix" aria-hidden="true"/>\
                                    </td><td width = "100px">\
                                        <input id="rule-name" class="input-sm form-control material fix-font-color rule-name" type="text" value="Rule #">\
                                    </td><td width = "60px">\
                                        <span class="vertical-separator" aria-hidden="true"/>\
                                        <span class="fa fa-clock-o icons-fix" aria-hidden="true"/>\
                                    </td><td width = "60px">\
                                        <input id="rule-timer" class="input-sm form-control timeout material fix-font-color input-timer" type="number" value="1" min="1" size="1"/>\
                                    </td><td width = "100px">\
                                        <select id="rule-timer-select" class="input-sm form-control material fix-font-color fix-combol-background input-timer-ti">\
                                            <option selected value="Seconds">Секунды</option>\
                                            <option value="Minutes">Минуты</option>\
                                            <option value="Hours">Часы</option>\
                                            <option value="Days">Дни</option>\
                                        </select>\
                                    </td><td width = "60px">\
                                        <span class="vertical-separator" aria-hidden="true"/>\
                                        <span class="fa fa-repeat icons-fix" aria-hidden="true"/>\
                                    </td><td width = "60px">\
                                        <input id="rule-count" class="input-sm form-control timeout material fix-font-color input-count" type="number" value="1" min="1" size="1"/>\
                                    </td><td width = "60px">\
                                        <span class="vertical-separator" aria-hidden="true"/>\
                                        <span class="fa fa-chain-broken icons-fix" aria-hidden="true"/>\
                                    </td><td width = "15px">\
                                        <div class="checkbox" style=" margin-right: 0px; margin-left: 20px;">\
                                            <input style="" class="absence-checkbox" type="checkbox"/>\
                                            <i style="margin-right: 0px;"></i>\
                                        </div>\
                                    </td><td width = "60px">\
                                        <span class="vertical-separator" aria-hidden="true"/>\
                                        <span class="fa fa-pencil-square-o icons-fix" aria-hidden="true"/>\
                                    </td><td>\
                                        <input id="rule-description" class="input-sm form-control material fix-font-color rule-description rule-description-style placeholder-fix" type="text" size="1" placeholder="Описание правила"/>\
                                    </td>\
                                </tr></table>\
\
                                <table width="100%" id="rule-header-static"><tr>\
                                    <td width = "20px">\
                                        <span class="fa fa-pencil icons-fix" aria-hidden="true"/>\
                                    </td><td width="0" style="white-space: nowrap;">\
                                        <span id="rule-name" class="vertical-separator-static"/>\
                                    </td><td width = "60px">\
                                        <span class="fa fa-clock-o icons-fix" aria-hidden="true"/>\
                                    </td><td width = "60px">\
                                        <span id="rule-timer" class="vertical-separator-static"/>\
                                    </td><td width = "100px">\
                                        <span id="rule-timer-select" class="vertical-separator-static"/>\
                                    </td><td width = "60px">\
                                        <span class="fa fa-repeat icons-fix" aria-hidden="true"></span>\
                                    </td><td width = "60px">\
                                        <span id="rule-count" class="vertical-separator-static"/>\
                                    </td><td width = "60px">\
                                        <span class="fa fa-pencil-square-o icons-fix" aria-hidden="true"></span>\
                                    </td><td width="100%">\
                                        <span id="rule-description" class="vertical-separator-static"/>\
                                    </td>\
                                </tr></table>\
                            </td><td width="80px">\
                                <span class="fa fa-trash icons-fix manage-group hover-icons button-delete" aria-hidden="true"></span>\
                                <span class="fa fa-copy icons-fix manage-group hover-icons button-copy" aria-hidden="true"></span>\
                                <span class="fa fa-plus icons-fix  manage-group hover-icons button-add" aria-hidden="true"></span>\
                                <span class="manage-group manage-group-right-padding"></span>\
                            </td><td width="50px">\
                                <span class="fa fa-chevron-up collapse-two icons-fix hover-icons" aria-hidden="true"></span>\
                            </td>\
                        </tr></table>\
                    </div>\
                    <div class="panel-body panel-body-fix">       \
                        <table class="table table-fix" >\
                            <tr> \
                                <td class="table-td-fix" style="margin-rigth:0px!important; padding-rigth:0px!important">      \
                                    <div>\
                                        <div class="qb-filter" style="padding-rigth:0px!important"></div>                              \
                                        <div class="qb-inherit"></div>                              \
                                        <div class="qb-modal"></div>                              \
                                    </div>\
                                </td>\
                                <td width="30px" class="table-td-fix" style="padding-right:3px!important">\
                                    <div class="btn-group-vertical" data-toggle="buttons" style="padding-top: 4px;">\
                                        <label class="btn btn-primary active disabled button-filter" data-toggle="tooltip" data-placement="left" data-original-title="Фильтр">\
                                            <input type="checkbox" autocomplete="off" checked><span class="fa fa-filter icons-fix"></span>\
                                        </label>\
                                        <label class="btn btn-primary button-inherit" data-toggle="tooltip" data-placement="left" data-original-title="Наследование">\
                                            <input type="checkbox" autocomplete="off"><span class="fa fa-sort-amount-asc icons-fix"></span>\
                                        </label>\
                                        <label class="btn btn-primary button-modal" data-toggle="tooltip" data-placement="left" data-original-title="Модальность">\
                                            <input type="checkbox" autocomplete="off"><span class="fa fa-paperclip icons-fix"></span>\
                                        </label>\
                                    </div>\
                                </td>\
                            </tr>\
                        </table>   \
                    </div>                                                  \
                </div>\
            </div>';

var template_tree_node = '<ol>\
                <li class="ui-state-default" id="list_">\
                    <div class="rule-default"></div>\
                </li>\
            </ol>';

var template_tree_node_just_li = '<li class="ui-state-default" id="list_">\
                <div class="rule-default"></div>\
            </li>';
