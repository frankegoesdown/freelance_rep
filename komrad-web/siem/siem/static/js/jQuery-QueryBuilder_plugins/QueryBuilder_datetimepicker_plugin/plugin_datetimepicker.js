$.fn.queryBuilder.define('datetimepicker', function (options) {

    this.on('getRuleInput.filter', function (h, rule, name) {

        var filter = rule.filter;

        if (filter.type === 'datetime') {
            var datetimeFormat = 'DD/MM/YYYY, HH:mm:ss';
        }

        if (filter.type === 'time') {
            var datetimeFormat = 'HH:mm:ss';
        }

        if (filter.type === 'datetime' || filter.type === 'time') {

            function dateTimeQBSetup(rule) {
                // it isn`t working in html to pdf tool
                try {
                    var input = rule.$el.find('.rule-value-container input');
                    input.datetimepicker({
                        allowInputToggle: true,
                        format: datetimeFormat,
                        locale: 'ru'
                    });
                    input.blur()
                } catch (err) {

                }
            }

            setTimeout(function () {
                dateTimeQBSetup(rule);
            }, 200)

            function valueSetter(rule, value) {
                var element = rule.$el.find('.rule-value-container input');
                if (rule.operator.type === 'between' || rule.operator.type === 'not_between') {
                    var values = [];
                    element.each(function (index, item) {
                        var new_value = moment.unix(rule.value[index]).format(datetimeFormat);
                        var input = $(item);
                        values.push(new_value);
                        $(this).val(new_value);
                    })

                    return values;
                }
                if (filter.type === 'time') {
                    var new_value = moment().seconds(value).format(datetimeFormat);
                    element.val(new_value);
                    return;
                }

                var new_value = moment.unix(value).format(datetimeFormat);
                element.val(new_value);
            }

            function valueGetter(rule) {
                //setTimeout(function() {}, 10);
                var inputs = rule.$el.find('.rule-value-container input');
                if (rule.operator.type === 'between' || rule.operator.type === 'not_between') {
                    var range = [];
                    inputs.each(function (index, item) {
                        var input = $(item);
                        $(this).on('blur.datetime', function () {
                            $(this).change();
                        });

                        range.push(moment(input.val(), datetimeFormat).unix());
                    });
                    return range;
                }
                inputs.on('blur.datetime', function () {
                    inputs.change();
                });

                if (filter.type === 'time') {
                    var value = moment.duration(inputs.val(), datetimeFormat).asSeconds()
                    return value
                }

                var value = moment(inputs.val(), datetimeFormat).unix();
                return value
            }

            function validation(datetime) {
                var emptyErr = 'неверное заполнение',
                    input = rule.$el.find('.rule-value-container input'),
                    hasError = true;
                input.each(function (index, item) {
                    var singleInp = $(item);
                    if (singleInp.val() !== undefined &&
                        singleInp.val() !== '') {
                        hasError = false;
                    } else {
                        hasError = true;
                    }
                });

                if (!hasError) {
                    return true;
                } else {
                    return emptyErr;
                }

            }

            filter.validation = {
                callback: validation
            };
            filter.valueGetter = valueGetter;
            filter.valueSetter = valueSetter;

        }
    });


}, {
    font: 'glyphicons',
    color: 'default'
});