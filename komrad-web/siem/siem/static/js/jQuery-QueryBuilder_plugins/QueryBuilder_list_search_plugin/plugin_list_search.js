$.fn.queryBuilder.define('list_search', function (options) {

    this.on('afterUpdateRuleOperator.queryBuilder', function (h, rule, name) {


        var input = rule.$el.find('.rule-value-container input'),
            filter = rule.filter,
            resultItems = [];
        if (rule.operator.type == 'in' ||
            rule.operator.type == 'not_in') {
            
            input.attr('type', 'text');
            //get flat list from server
            function getLists() {

                $.get('/bof/all', function (data) {
                    data.forEach(function (item) {
                        resultItems.push(item);
                    });

                });
            }

            if (resultItems.length === 0) {
                getLists();
            }

            //display lists in ul
            function displayLists(node) {
                var lists = node.parent().parent().parent().find("ul.lists");
                lists.each(function (index, item) {
                    $(item).css('visibility', 'hidden');
                });

                var ul = '<ul class="list-group dropdown-menu lists" style="display: block; visibility: hidden"></ul>'

                if (!node.next()[0]) {
                    node.parent().append(ul);
                }

                var innode = node.parent().find(".lists");
                innode.empty();
                resultItems.forEach(function (item) {
                    innode.append("<li class='list-group-item input_1' data-value='" +
                        item.name + "' data-list='" + item.id + "'>" +
                        item.name + "</li>");
                });

                innode.css('visibility', 'visible');
                var lists = innode.children('li');
                lists.each(function (i, list) {
                    list.style.cursor = 'pointer';
                    list.style.overflowX = 'hidden';
                    list.addEventListener('click', function (event) {
                        node.val(list.dataset.value);
                        node.attr('data-inplist', list.dataset.list);
                        node.attr('data-value', list.dataset.value);
                        innode.css('visibility', 'hidden');
                        node.trigger('change');
                    })
                });
            }

            input.each(function (index, item) {
                var item = $(item);
                item.on('focus.list', function (e) {
                    displayLists($(this));
                });

                item.on('blur.list', function (e) {
                    //var lists = $(this).parent().find(".lists");
                    //lists.css('visibility', 'hidden');
                    reset($(this));
                });
            });

            //set defaults if value is empty
            function reset(node) {
                if (node.val() == undefined || node.val() == '') {
                    node.removeAttr('data-inplist');
                    node.removeAttr('data-value');
                    node.trigger('change');
                }
                rule.value.name = node.val();
            }

            //set value to qb
            function valueSetter(rule, value) {
                var input = rule.$el.find('.rule-value-container input');
                input.attr('data-inplist', value.uiid);
                input.attr('data-value', value.name);
                input.val(value.name);
            }

            //get value from qb
            function valueGetter(rule) {
                var input = rule.$el.find('.rule-value-container input');
                return {
                    uuid: input.attr('data-inplist'),
                    name: input.attr('data-value')
                };
            }

            //check if input value in list
            function listValueValidator(rule, result, input) {
                for (var i = 0; i < result.length; i++) {
                    if (rule.value.name !== undefined &&
                        rule.value.name !== '' &&
                        input.val() === result[i].name) {
                        return false;
                    }
                }
                return true
            }

            //validate input
            function validation() {
                var message,
                    input = rule.$el.find('.rule-value-container input'),
                    hasError = true;
                hasError = listValueValidator(rule, resultItems, input);

                if (!hasError) {
                    return true;
                } else {
                    for (var i = 0; i < resultItems.length; i++) {
                        if (input.val() != resultItems[i].name &&
                            rule.value.name !== '' &&
                            rule.value.name !== undefined) {
                            message = 'Значение не из списка';
                        } else {
                            message = 'Поле не может быть пустым';
                        }
                    }
                    return message;
                }
            }

            filter.validation = {
                callback: validation
            };
            filter.valueGetter = valueGetter;
            filter.valueSetter = valueSetter;

        } else {
            input.each(function (index, item) {
                var item = $(item);
                item.removeAttr('data-inplist');
                item.removeAttr('data-value');
                item.off('focus.list');
                item.off('blur.list');
            })
        }

    });


    $(document).click(function (event) {
        if ($(event.target).closest("ul").length) return;
        $('ul.lists').css('visibility', 'hidden');
        event.stopPropagation();
    });


}, {
    font: 'glyphicons',
    color: 'default'
});