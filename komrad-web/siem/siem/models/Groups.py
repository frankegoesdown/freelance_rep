# -*- coding: utf-8 -*-

from sqlalchemy import Column
from siem.extensions import db
from abstractions import siemModel
from flask_login import current_user
from sqlalchemy.exc import IntegrityError


class Group(siemModel):
    """Summary

    Attributes:
        active (int): 0 - disables, 1 - active
        alone (int): 1 - forever alone group, 1 - united mans group
        id (int): primary_key
        name (unicode): just a name. if (aline) -> group_name is user_name
    """
    __tablename__ = 'groups'

    id = Column(db.Integer, primary_key=True)
    name = Column(db.Unicode(255))
    alone = Column(db.Integer)
    active = Column(db.Integer)
    desc = Column(db.Unicode(1023))

    users = db.relationship("UsersToGroups", back_populates="group")
#    reactions = relationship("ReactionsToGroups", back_populates="group")

    def __init__(self, name, is_alone, desc='', active=1):
        self.name = name
        self.alone = is_alone
        self.active = active
        self.desc = desc

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id': self.id,
            'name': self.name,
            'desc': self.desc,
        }

    @property
    def serializeFull(self):
        """Return object data in easily serializeable format"""
        return {
            'id': self.id,
            'name': self.name,
            'alone': self.alone,
            'active': self.active,
            'users': self.users,
            'reactions': self.reac
        }

    @classmethod
    def get_by_id(self, _id):
        return self.query.filter_by(id=_id).first()

    @staticmethod
    def getGroupes(is_active=1, is_alone=0):
        return [row.serialize for row in Group.query.filter_by(
            active=is_active, alone=is_alone)]

    @staticmethod
    def get_all():
        return [x.serialize for x in Group.query.all()]

    @staticmethod
    def getDefaultId():
        """Get default group id

        Returns:
            Group: default group (id:0)
        """
        return 1

    @staticmethod
    def add(json_data):
        name = json_data.get('name')
        desc = json_data.get('desc', None)
        is_alone = json_data.get('is_alone')
        statement = Group(name, is_alone, desc)
        db.session.add(statement)
        try:
            db.session.commit()
        except IntegrityError:
            return False, u"Group with name {name} was created earlier".format(
                name=name
            )
        return True, None

    @staticmethod
    def update(_id, json_data):
        statement = Group.get_by_id(_id)
        statement.name = json_data.get('name', statement.name)
        statement.desc = json_data.get('desc', statement.desc)
        return db.session.commit()

    @staticmethod
    def remove(_id):
        """removing group

        Args:
            _id (int): primary key for record search

        Raises:
            ValueError: Block admin record remove
        """
        if _id == 1:
            raise ValueError('Unremovable record')
        statement = Group.get_by_id(_id)
        db.session.delete(statement)
        return db.session.commit()

    @staticmethod
    def assasinate(_id):
        if id == 0:
            raise ValueError('Unupdatable record')
        statement = Group.get_by_id(_id)
        statement.active = 0
        return db.session.commit()


    @staticmethod
    def ressurect(data):
        """make inactive group active again
        
        :param data: int as record id or json with name of group
        :return: nothing
        """

        statement = None
        if isinstance(data, dict):
            statement = Group.query.filter_by(name=data['name']).first()
            if statement.id == 0:
                raise ValueError('Immutable record')

        elif isinstance(data, int):
            if data == 0:
                raise ValueError('Immutable record')
            statement = Group.get_by_id(data)

        statement.active = 1
        return db.session.commit()

