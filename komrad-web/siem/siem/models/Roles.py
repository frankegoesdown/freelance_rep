# -*- coding: utf-8 -*-

from sqlalchemy import Column
from siem.extensions import db
from abstractions import siemModel
from Accessability import Accessability
from sqlalchemy.orm.exc import NoResultFound

ACCESS_LVLS = {
    'none_access': 0,
    'read': 1,
    'write': 2,
}


class Role(siemModel):

    __tablename__ = 'roles'

    id = Column(db.Integer, primary_key=True)
    name = Column(db.Unicode(200), nullable=False, unique=True)

    def _get_access_map(self):
        try:
            my_access_map = Accessability.query.filter_by(
                type='role', type_id=self.id).one().getAccessMap()
        except NoResultFound:
            my_access_map = Accessability.getDefaultAccess()
        return my_access_map

    def _set_access_map(self, access_map):

        my_access_map = Accessability.query.filter_by(
            type='role', type_id=self.id).one()
        my_access_map.setAccessMap(access_map, True)
        return db.session.commit()

    access_map = property(_get_access_map, _set_access_map)

    def __init__(self, name):
        self.name = name

    @classmethod
    def get_by_id(self, _id):
        return self.query.filter_by(id=_id).first()

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id': self.id,
            'name': self.name,
            'access_map': self.access_map
        }

    def createAccessInstance(self, access_map):
        new_access_map = Accessability()
        new_access_map.type = "role"
        new_access_map.type_id = self.id
        new_access_map.setAccessMap(access_map, True)
        db.session.add(new_access_map)


    @staticmethod
    def addRole(name, access_map=Accessability.getDefaultAccess()):
        statement = Role(name)
        db.session.add(statement)
        db.session.commit()

        statement.createAccessInstance(access_map)
        return db.session.commit()

    @staticmethod
    def updateRecord(id, name, access_map=Accessability.getDefaultAccess()):
        if id == 1:
            raise ValueError('Immutable record')
        statement = Role.get_by_id(id)
        statement.name = name
        try:
            statement.access_map = access_map
        except NoResultFound:
            statement.createAccessInstance(access_map)
        db.session.commit()
        return 'done'

    @staticmethod
    def removeRole(id):
        if id == 1:
            raise ValueError('Immutable record')
        statement = Role.get_by_id(id)
        db.session.delete(statement)
        db.session.commit()
        # logic for rm node and all leafes in there
        return
