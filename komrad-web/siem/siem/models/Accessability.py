# -*- coding: utf-8 -*-

from sqlalchemy import Column

from abstractions import siemModel
from scapy.all import *
from siem.extensions import db
import json

import siem.route_access as sr


class Accessability(siemModel):

    __tablename__ = 'accessability'

    type = Column('type', db.Unicode(45), nullable=False, primary_key=True)
    type_id = Column('type_id', db.Integer, nullable=False, primary_key=True)
    _access_map = Column('access_map', db.Unicode, nullable=False)

    def getAccessMap(self):
        if not self._access_map:
            return self.getDefaultAccess()
        a_map = json.loads(self._access_map)
        if self.type == 'role':
            for k in sr.ACCESS_DOMAINS_ALL:
                if k not in a_map.keys():
                    a_map[k] = 0
        return a_map

    def setAccessMap(self, access_map, autofilling=False):
        res = {}
        if autofilling:
            for k in self.getDefaultAccess().keys():
                res[k] = access_map.get(k, 0)
        else:
            for k, v in access_map.iteritems():
                if k in self.getDefaultAccess().keys():
                    res[k] = v

        self._access_map = json.dumps(res)

    # Hide password encryption by exposing password field only.
    # access_map = db.synonym(
    #     '_access_map',
    #     descriptor=property(_get_access_dict, _set_access_dict))

    db.PrimaryKeyConstraint('type', 'type_id')

    @property
    def serialize(self):
        return {
            'type': self.type,
            'type_id': self.type,
            'access_map': self._access_map
        }

    def __repr__(self):
        return '<Access levels for: %s #%d>' % (self.type, self.type_id)

    @staticmethod
    def getDefaultAccess():
        res = {}
        for k in sr.ACCESS_DOMAINS_ALL:
            res[k] = 0
        return res
