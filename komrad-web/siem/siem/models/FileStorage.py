# -*- coding: utf-8 -*-
from flask import current_app
from sqlalchemy import Column
from siem.extensions import db


class FileStorage(db.Model):
    __tablename__ = 'file_storage'

    id = Column(db.Integer, primary_key=True, nullable=False)
    path = Column(db.Unicode(1023), nullable=False)

    def __init__(self, path):
        self.path = path

    @classmethod
    def get_by_id(self, _id):
        return self.query.filter_by(id=_id).first()

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id': self.id,
            'path': current_app.config['UPLOAD_FOLDER'] + self.path
        }

    @staticmethod
    def create(path):
        """rewrite uuid of record, delete daemon and add new
        """
        st = FileStorage(path)
        db.session.add(st)
        db.session.commit()
        return st.id

    @staticmethod
    def remove(id):
        """removing users

        Args:
            id (int): primary key for record search

        Raises:
            ValueError: Block admin record remove
        """
        pass


