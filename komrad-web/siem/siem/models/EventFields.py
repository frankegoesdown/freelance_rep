# -*- coding: utf-8 -*-

from sqlalchemy import Column
from siem.extensions import db
from abstractions import siemModel

from scapy.all import *
import json


class EventFields(siemModel):
    """Active record for event_fields table

    Attributes:
        description (column object): Description
        id (column object): primary key
        name (column object): column unique name
        name_ru (column object): russion localization
        operators (column object): avialable operators
        type (column object): [string, integer, datetime] data type
    """
    __tablename__ = 'event_fields'

    id = Column('id', db.Integer, primary_key=True)
    name = Column('name', db.String(127), nullable=False, unique=True)
    name_ru = Column('name_ru', db.Unicode(1023), nullable=False, unique=True)
    description = Column('description', db.String(255))
    type = Column('type', db.String(127), nullable=False)
    _operators = Column('operators', db.Unicode(1023), nullable=False)
    faceted = Column('faceted', db.Integer, nullable=False)
    data_type = Column('data_type', db.String(45))
    parent_id = Column(db.Integer, db.ForeignKey('event_fields.id'))

    children = db.relationship("EventFields")

    @classmethod
    def get_roots(self):
        return self.query.filter_by(parent_id=None, data_type=None)

    @classmethod
    def get_trees(self):
        return [x.serialize for x in self.get_roots()]

    @staticmethod
    def line2tree(source):
        # бомбит по ужаса много запросов, каждое поддерево это отдельный
        # запрос в БД.
        # Легче выпуллить все сразу и через питон его и обраьотать
        #!TODO сменить это рекурсивное гавно на структуризацию на месте
        roots = EventFields.get_roots()
        base = None
        net = None
        other = []
        for root in roots:
            if root.name == "base":
                base = root.SourceIntoRoot(source)
                continue
            if root.name == "net_data":
                net = root.SourceIntoRoot(source)
                continue
            subtree = root.SourceIntoRoot(source)
            if subtree:
                other.append(subtree)

        return base, net, other

    def SourceIntoRoot(self, source):
        if not self.parent_id:
            clds = {}
            for c in self.children:
                srzd = c.SourceIntoRoot(source)
                if not srzd:
                    continue
                clds[c.name] = srzd

            if len(clds) == 0:
                return None
            return {
                'id': self.id,
                'name': self.name,
                'label': self.name_ru,
                'description': self.description,
                'children': clds
            }

        if self.name in source:
            return {
                'label': self.name_ru,
                'description': self.description,
                'value': source[self.name]
            }
        return None

    @classmethod
    def get_by_id(self, query_id):
        return self.query.filter_by(id=query_id).first_or_404()

    @classmethod
    def get_by_id_safe(self, _id):
        return self.query.filter_by(id=_id).filter(self.faceted.isnot(None)).first()

    @classmethod
    def getFaceted(self):
        return [row.serialize for row in self.query.filter_by(faceted=1)]

    @classmethod
    def getFieldLocale(self, dtype):
        return self.query.filter_by(name=dtype).first_or_404().name_ru

    def is_unique(self):
        if self.query.filter_by(name=self.name).first():
            return False
        return True


    @classmethod
    def get_all(self):
        """Cache and return table data as list of dicts

        Returns:
            list: all fields, dict by dict
        """
        return [row.serialize for row in self.query.filter(self.faceted.isnot(None))]

    def _get_oprs(self):
        if not self._operators:
            return None
        return json.loads(self._operators)

    def _set_oprs(self, password):
        self._password = json.dumps(operators)

    # Hide password encryption by exposing password field only.
    operators = db.synonym('_operators',
                           descriptor=property(_get_oprs, _set_oprs))

    @classmethod
    def getItemByName(self, field):
        return next((item for item in \
            self().get_all() if item["name"] == field), field)

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        if not self.parent_id:
            return {
                'id': self.id,
                'name': self.name,
                'label': self.name_ru,
                'description': self.description,
                'children': [x.serialize for x in self.children]
            }

        return {
            'id': self.id,
            'name': self.name,
            'label': self.name_ru,
            'description': self.description,
            'type': self.type,
            'operators': json.loads(self._operators),
            'faceted': self.faceted,
            'data_type': self.data_type
        }

    def __repr__(self):
        return '<Field: %r>' % (self.name)

