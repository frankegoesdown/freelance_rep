# -*- coding: utf-8 -*-

from sqlalchemy import Column
from siem.extensions import db
from abstractions import siemModel

import json

st_work = 0
st_pause = 1
st_removed = 2


class DirStore(siemModel):
    """
    active record for dir_store table
    It`s table with tree structure like:
    +-------+-------------+
    | id    | breadcrumbs |
    +-------+-------------+
    | 1     | 1           |
    | 2     | 1.1         |
    | 3     | 1.2         |
    | 4     | 1.1.1       |
    +-------+-------------+

    Tree has nodes and leafes, node has nullable uuid and data fields, but
    leaf has this data and can detected by tham.
    Tree has special logic for CRUD operation, and all logic should be putted
    in this class.

    Attributes:
        breadcrumbs (unicode): tree path
        data (unicode): directive data body
        id (int): primary key
        name (unicode): name
        ru_name (unicode): local name
        uuid (unicode): directive uuid
    """
    __tablename__ = 'dir_store'

    id = Column('id', db.Integer, primary_key=True)
    is_folder = Column('is_folder', db.Integer)
    breadcrumbs = Column('breadcrumbs', db.String(255), nullable=False)
    name = Column('name', db.Unicode(100))
    ru_name = Column('ru_name', db.Unicode(100))
    uuid = Column('uuid', db.Unicode(50), unique=True)
    data = Column('data', db.Unicode(81920), default=u'{}')
    status = Column('status', db.Integer)

    @staticmethod
    def createDirStore(breadcrumbs, name, ru_name, uuid=None, data=None,
                       status=0, is_folder=0):
        """New record creater

        Args:
            breadcrumbs (unicode): tree path
            name (unicode): node name
            ru_name (unicode): local name
            uuid (unicode, optional): uuid
            data (unicode, optional): dir body

        Returns:
            TYPE: Description
        """
        statement = DirStore()

        statement.is_folder = is_folder
        statement.breadcrumbs = breadcrumbs
        statement.name = name
        statement.ru_name = ru_name
        statement.status = status

        if uuid:
            statement.uuid = uuid
        if data:
            statement.data = data

        db.session.add(statement)
        db.session.commit()
        return

    @staticmethod
    def updateRecord(id, breadcrumbs, name, ru_name, uuid=None, data=None,
                     status=0, is_folder=0):
        """update record

        Args:
            id (int): primary key
            breadcrumbs (unicode): tree path
            name (unicode): node name
            ru_name (unicode): local name
            uuid (unicode, optional): uuid
            data (unicode, optional): dir body

        Returns:
            TYPE: Description
        """
        statement = DirStore.get_by_id(id)
        statement.is_folder = is_folder
        statement.breadcrumbs = breadcrumbs
        statement.name = name
        statement.ru_name = ru_name
        statement.uuid = uuid
        statement.data = data
        statement.status = status
        db.session.commit()
        return 'done'

    @staticmethod
    def updateDirStoreStatus(id, status):
        """ Update status

        Args:
            id(int): primary key
            status (int): status
        """
        statement = DirStore.get_by_UUID(id)
        statement.status = status
        db.session.commit()

        return 'done'

    @staticmethod
    def removeDirStore(id):
        """remove record (cascade)

        Args:
            id (int): primary key

        Returns:
            TYPE: Description
        """

        global st_removed
        statement = DirStore.get_by_id(id)
        statement.status = st_removed
        db.session.commit()

        # logic for rm node and all leafes in there
        return

    @classmethod
    def get_by_name(self, name):
        return self.query.filter_by(name=name).count()

    @classmethod
    def get_by_id(self, query_id):
        return self.query.filter_by(id=query_id).first_or_404()

    @classmethod
    def get_by_UUID(self, uuid):
        return self.query.filter_by(uuid=uuid).first_or_404()

    @staticmethod
    def get_all():
        return DirStore.query.all()

    @classmethod
    def get_all_as_dict(self):
        global st_removed
        res = []
        for f in DirStore.query.all():
            if f.as_dict()['status'] != st_removed:
                res.append(f.as_dict())
        return res

    def _get_oprs(self):
        return json.loads(self._operators)

    def __repr__(self):
        return '<Directive: %r>' % (self.name)
