# -*- coding: utf-8 -*-

from flask import Blueprint
from flask import abort
from flask import send_from_directory
from flask import current_app
from flask import jsonify
from flask import request
from werkzeug.utils import secure_filename

import os
import time
import hashlib

from siem.models import FileStorage
from siem.wrappers import access_required
import siem.route_access as ra

fileserver = Blueprint('fileserver', __name__, url_prefix='/file')


def createFileInStorage(file, file_fmt):
    m = hashlib.md5()
    for x in range(0, 10):
        m.update(str(time.time()) + '1f85erg6e5r1g')
        h = m.hexdigest()
        dir_path = h[0:3] + '/' + h[4:7] + '/' + h[8:11] + '/'
        file_path = dir_path + h[12:15] + '.' + file_fmt
        if os.path.exists(current_app.config['UPLOAD_FOLDER'] + file_path):
            continue
        if not os.path.exists(dir_path):
            os.makedirs(current_app.config['UPLOAD_FOLDER'] + dir_path)

        file.save(os.path.join(current_app.config['UPLOAD_FOLDER'],
                  file_path))
        return file_path
    return None


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1] in current_app.config['ALLOWED_EXTENSIONS']


@fileserver.route("", methods=["POST"])
@access_required(ra.FILES_ACCESS)
def upload_file():
    if 'file' not in request.files:
        return abort(400)

    file = request.files['file']
    if file.filename == '':
        return abort(400)

    path = None
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        fmt = filename.split('.')[1]
        path = createFileInStorage(file, fmt)

    if not path:
        return jsonify({'status': 'failed'}), 500

    file_id = FileStorage.create(path)
    return jsonify({'id': file_id}), 200


@fileserver.route("/<string:a>/<string:b>/<string:c>/<string:filename>",
                  methods=["GET"])
@access_required(ra.FILES_ACCESS)
def get_file(a, b, c, filename):
    path = current_app.config['UPLOAD_FOLDER'] + '/' + a + '/' + b + '/' + c
    return send_from_directory(path, filename)
