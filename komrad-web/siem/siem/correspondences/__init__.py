# -*- coding: utf-8 -*-
"""Blueprint and controllers of event listener module

Attributes:
    ACCESS_MODULE (str): module name for access wrapper
    event_listener (TYPE): Blueprint for this module
"""

# from ..extensions import queue_manager
from flask import Blueprint

corresps = Blueprint('correspondences', __name__,
                     url_prefix='/correspondences',
                     template_folder='templates')
ACCESS_MODULE = 'access_el'

from controllers import *
# from sio import *
