# -*- coding: utf-8 -*-

from flask import jsonify, request
from siem.wrappers import access_required
from siem.facts_base import facts_base
from siem.facts_base.models.BOFRequester import BofRequester
import siem.route_access as ra


@facts_base.route('/clist/<string:_id>', methods=["GET"])
@access_required(ra.PAGE_FACTS_BASE)
def get_custom_list(_id):
    res = BofRequester.getList(_id)
    return jsonify(res), 200


@facts_base.route('/clist', methods=["POST"])
@access_required(ra.PAGE_FACTS_BASE)
def create_custom_list():
    data = request.get_json()
    res = BofRequester.createList(data)
    return jsonify(res), 200


@facts_base.route('/clist', methods=["PUT"])
@access_required(ra.PAGE_FACTS_BASE)
def update_custom_list():
    data = request.get_json()
    res = BofRequester.updateList(data)
    return jsonify(res), 200


@facts_base.route('/clist/<string:_id>', methods=["DELETE"])
@access_required(ra.PAGE_FACTS_BASE)
def delete_custom_list(_id):
    res = BofRequester.removeList(_id)
    return jsonify(res), 200
