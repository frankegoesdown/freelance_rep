# -*- coding: utf-8 -*-

from flask import jsonify, render_template, request
from siem.wrappers import access_required
from siem.facts_base import facts_base
from siem.facts_base.models.BOFRequester import BofRequester
import siem.route_access as ra


@facts_base.route('/all', methods=["GET"])
@access_required(ra.PAGE_FACTS_BASE)
def get_all_lists():
    res = BofRequester.getAllLists()
    return jsonify(sorted(res, key=lambda k: k['name'])), 200


@facts_base.route('/lists', methods=["GET"])
@access_required(ra.PAGE_FACTS_BASE)
def ajax_all_lists():
    return render_template('facts_base/all_lists.html')


@facts_base.route('/raw_request', methods=["POST"])
@access_required(ra.PAGE_FACTS_BASE)
def raw_auto_request():
    data = request.get_json()
    res = BofRequester.sendRawRequest(data)
    return jsonify(res), 200

