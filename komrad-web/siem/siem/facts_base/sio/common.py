# -*- coding: utf-8 -*-

from siem.wrappers import access_required

from flask_socketio import Namespace, emit, join_room, leave_room, \
    close_room, rooms, disconnect
from siem.extensions import sio
from flask import request
from flask_login import login_required

ACCESS_MODULE = 'access_correl'


class CorrelNamespace(Namespace):

    def getRoomName(self):
        return 'lists'

    def on_leave(self, message):
        leave_room(self.getRoomName())

    def on_disconnect_request(self):
        leave_room(self.getRoomName())
        disconnect()

    @login_required
    def on_join(self, data):

        join_room(self.getRoomName())
        emit('note', data, room=self.getRoomName())

    def on_connect(self):
        print('Client connected to base_of_facts', request.sid)

    def on_disconnect(self):
        leave_room(self.getRoomName())
        print('Client disconnected from to base_of_facts', request.sid)


sio.on_namespace(CorrelNamespace('/facts_base'))
