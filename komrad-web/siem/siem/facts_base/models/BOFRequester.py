# -*- coding: utf-8 -*-
import requests
import json

from flask import current_app as app


class BofRequester(object):

    def checkSql(self, req):
        try:
            self.sendRawRequest({u'request': req})
        except requests.exceptions.HTTPError as e:
            if e.response.status_code == 400:
                return 400
            raise

    def formatValues(self, values):
        for k, row in enumerate(values):
            values[k] = [x.rstrip() for x in row]
        return values

    @classmethod
    def getAllLists(self):
        data = requests.get(
            app.config['FACTS_BASE_URL'] + 'baseoffacts/lists').content
        return json.loads(data)

    @classmethod
    def getList(self, _id):
        get_url = app.config['FACTS_BASE_URL'] + 'baseoffacts/custom_list' + \
            '?id=' + _id
        response = requests.get(get_url)
        if response.status_code == requests.codes.ok:
            return json.loads(response.content)
        response.raise_for_status()

    @classmethod
    def createList(self, data):
        values = self().formatValues(data['values'])
        to_send = {
            u'name': data[u'name'],
            u'values': values
        }
        response = requests.post(
            app.config['FACTS_BASE_URL'] + 'baseoffacts/custom_list',
            json=to_send)

        if response.status_code == requests.codes.ok:
            return json.loads(response.content)
        response.raise_for_status()

    @classmethod
    def updateList(self, data):
        values = self().formatValues(data['values'])
        to_send = json.dumps({
            u'id': data['id'],
            u'values': values,
            u'name': data['name']
        })
        response = requests.put(
            app.config['FACTS_BASE_URL'] + 'baseoffacts/custom_list',
            data=to_send)
        if response.status_code == requests.codes.ok:
            return {u'result': u'ok'}
        response.raise_for_status()

    @classmethod
    def removeList(self, _id):
        delete_url = app.config['FACTS_BASE_URL'] + \
            'baseoffacts/custom_list' + '?id=' + _id
        response = requests.delete(delete_url)
        if response.status_code == requests.codes.ok:
            return response.status_code
        response.raise_for_status()

    @classmethod
    def getAutoList(self, _id):
        get_url = app.config['FACTS_BASE_URL'] + \
            'baseoffacts/auto_list?id=' + _id
        response = requests.get(get_url)
        if response.status_code == requests.codes.ok:
            return json.loads(response.content)
        response.raise_for_status()

    @classmethod
    def createAutoList(self, data):
        if self().checkSql(data['sql']) == 400:
            return 400
        to_send = json.dumps({
            u'name': data['name'],
            u'sql': data['sql'],
            u'update_period': data['update_period'],
            u'timer_select': data['timer_select']
        })
        response = requests.post(
            app.config['FACTS_BASE_URL'] + 'baseoffacts/auto_list',
            data=to_send)
        if response.status_code == requests.codes.ok:
            return json.loads(response.content)
        response.raise_for_status()

    @classmethod
    def updateAutoList(self, data):

        if self().checkSql(data['sql']) == 400:
            return 400
        to_send = json.dumps({
            u'name': data['name'],
            u'sql': data['sql'],
            u'update_period': data['update_period'],
            u'id': data['id'],
            u'timer_select': data['timer_select']
        })
        response = requests.put(
            app.config['FACTS_BASE_URL'] + 'baseoffacts/auto_list',
            data=to_send)
        if response.status_code == requests.codes.ok:
            return {u'result': u'ok'}
        response.raise_for_status()

    @classmethod
    def removeAutoList(self, _id):
        get_url = app.config['FACTS_BASE_URL'] + \
            'baseoffacts/auto_list?id=' + _id
        response = requests.delete(get_url)
        if response.status_code == requests.codes.ok:
            return response.status_code
        response.raise_for_status()

    @classmethod
    def sendRawRequest(self, data):
        to_send = json.dumps({
            u'request': data['request']
        })
        response = requests.post(
            app.config['FACTS_BASE_URL'] + 'raw_request',
            data=to_send)
        if response.status_code == requests.codes.ok:
            return json.loads(response.content)
        response.raise_for_status()
