# -*- coding: utf-8 -*-

from siem.wrappers import access_required

from flask_login import current_user
from flask_socketio import emit, send
from siem.extensions import sio
from flask_socketio import join_room, leave_room
import json
import siem.route_access as ra

room = 'realtime'


@sio.on('join_realtime')
@access_required(ra.PAGE_REALTIME)
def join_realtime(data):
    join_room(room)
    send('welcome to the realtime', room=room)


@sio.on('leave_realtime')
@access_required(ra.PAGE_REALTIME)
def leave_realtime(data):
    leave_room(room)
    send('Goodbye', room=room)


@sio.on_error_default
def def_err(data):
    pass
