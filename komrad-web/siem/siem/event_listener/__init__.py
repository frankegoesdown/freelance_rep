# -*- coding: utf-8 -*-
"""Blueprint and controllers of event listener module

Attributes:
    ACCESS_MODULE (str): module name for access wrapper
    event_listener (TYPE): Blueprint for this module
"""

# from ..extensions import queue_manager
from models.widget_hook import WidgetHook
from flask import Blueprint

event_listener = Blueprint('event_listener', __name__)

from controllers import *
from sio import *
