# -*- coding: utf-8 -*-
"""Blueprint and controllers of event listener module

Attributes:
    ACCESS_MODULE (str): module name for access wrapper
    event_listener (TYPE): Blueprint for this module
"""

from flask import current_app
from flask import render_template
from siem.models import EventFields, Query
from siem.wrappers import access_required
from siem.event_listener import event_listener
from flask_login import current_user
import json

import siem.route_access as ra
import os


def getJsFiles():
    s_folder = current_app.config['PROJECT_ROOT'] + '/siem/' + \
               current_app._static_folder + '/js/app'
    js_files = []


    modules = {}

    for path, subdirs, files in os.walk(s_folder):
        module_files = ['js/app' + (path + '/' + x).replace(s_folder, '') for x in files if 'module' in x]
        module_files += ['js/app' + (path + '/' + x).replace(s_folder, '') for x in files if 'module' not in x]
        d_key = path.replace(s_folder, '')

        # put in '' the root files
        if d_key is '':
            modules[d_key] = module_files
            continue

        for k in modules.keys():
            # skip pushing in ''
            if k is '':
                continue
            # find parent dir in availdable dirs
            if k in d_key:
                d_key = k
                break
        # including into parent dir ppath to file
        modules[d_key] = modules.get(d_key, []) + module_files

    # make a list from modules
    load_order = ['', '/core', '/event-listener']

    # if the module load order is declared; put it
    for m_name in load_order:
        js_files += modules.get(m_name, [])

    # other don`t need any order to load
    for m_name, files in modules.iteritems():
        if m_name not in load_order:
            js_files += files

    return js_files


@event_listener.route("/index", methods=["GET"])
@access_required(ra.ACCESS_DOMAINS_ALL)
def index():
    """Render for index page (manu and main layout)

    Returns:
        TYPE: compiled HTML view
    """
    access_map = {}
    for k, v in current_user.getFullAccessMap().iteritems():
        if v > 0:
            access_map[k] = v

    return render_template("index.html", a_map=access_map,
                           app_src_list=getJsFiles())


@event_listener.route("/summary", methods=["GET"])
@access_required(ra.PAGE_WIDGETS)
def ajax_summary():
    """Render for summary page

    Returns:
        TYPE: compiled HTML view
    """
    return render_template("event_listener/widgets.html")


@event_listener.route("/realtime", methods=["GET"])
@access_required(ra.PAGE_REALTIME)
def ajax_realtime():
    """Render for realtime page

    Returns:
        TYPE: compiled HTML view
    """
    return render_template("event_listener/realtime.html",
                           fields=EventFields.get_all())


@event_listener.route("/eventvisio", methods=["GET"])
@access_required(ra.PAGE_EVENTVISIO)
def ajax_event_visio():
    return render_template("event_listener/event_visio.html")


@event_listener.route("/querylist", methods=["GET"])
@access_required(ra.PAGE_QUERIES)
def ajax_query_list():
    """Rendered query list page

    Returns:
        TYPE: compiled HTML view
    """
    return render_template("event_listener/query_list.html")


@event_listener.route("/search/<int:query_id>", methods=["GET"])
@event_listener.route("/search/", methods=["GET"])
@access_required(ra.PAGE_SEARCH)
def ajax_query_constructor(query_id=None):
    """Render for search page

    Args:
        query_id (None, optional): id of query, use it for query preloading

    Returns:
        TYPE: compiled HTML view
    """
    query_data = {}
    if query_id:
        query_data = Query.get_by_id(query_id).as_dict()
        query_data['body'] = json.loads(query_data['body'])
    return render_template("event_listener/search.html",
                           fields=EventFields.get_all(),
                           query_data=query_data)
