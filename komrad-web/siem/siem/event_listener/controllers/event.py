# -*- coding: utf-8 -*-
"""
"""

from flask import render_template, jsonify, send_file, abort
from flask_babel import lazy_gettext as _
from siem.wrappers import access_required
import StringIO
from siem.models import EventFields
from ..models.LuceneRequester import LuceneRequester
from ..models.rubicon_event import RubiconPCAP
from siem.event_listener import event_listener
import siem.route_access as ra


@event_listener.route("/event/<string:id>", methods=["GET"])
@access_required(ra.ACCESS_EVENT_DATA)
def get_event_datail(id):
    """Render template for single event

    Args:
        id (str): event_id value

    Returns:
        HTML: Compiled template
    """
    data = LuceneRequester.getEvents(id)
    base, net, other = EventFields.line2tree(data)

    res = {
        'base': base,
        'net': net,
        'other': other
    }
    if data[u'plugin_id'] == 1011:
        try:
            ex_data = RubiconPCAP(data)
            ex_data = ex_data.getExtentionData()
            res['other'].append({
                u'name': 'rubicon',
                u'label': unicode(_(u'Rubicon PCAP')),
                u'tree': ex_data['pcup_tree']['proto'],
                u'hexdump': ex_data['hexdump']
            })
        except:
            pass
    return jsonify(res)



@event_listener.route("/event.html", methods=["GET"])
@access_required(ra.ACCESS_EVENT_DATA)
def get_event_templates():
    """Render template for single PCAP event

    Args:
        id (str): event_id value

    Returns:
        HTML: Compiled template
    """
    return render_template('event_listener/event/event_main.html')

