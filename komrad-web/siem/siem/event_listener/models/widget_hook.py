from ...client_manager.base_hook import BaseClientHook
from widget_data import WidgetData
from flask_login import current_user


class WidgetHook(BaseClientHook):
    """Hook catches events with name "WD_is_ready" and replace the data
    """

    def __init__(self):
        self.timestamp = 0
        self.get_last = True

    def catch(self, message):
        """Catch widget event

        Args:
            message (dict): queue message

        Returns:
            Boolean: True if it is WD_is_ready event
        """
        if message['event'] == 'WD_is_ready':
            self.timestamp = 0
            return True
        if message['event'] == 'WD_is_builded':
            self.timestamp = 0
            self.get_last = False
            return True
        return False

    def modify(self, message):
        """Modify the message

        Args:
            message (dict): message from queue

        Raises:
            NotImplementedError: this method must be reimplemented
        """
        wd = WidgetData(message['data']['uuid'])
        result = wd.getWidgetData(self.timestamp, self.get_last)
        message['event'] = 'WD:' + message['data']['uuid']
        message['data'] = result
        return message

    def catchAndModify(self, message):
        """Catch the message

        Args:
            message (dict): message from queue

        Raises:
            NotImplementedError: this method must be reimplemented
        """
        if self.catch(message):
            return self.modify(message)
        return message


