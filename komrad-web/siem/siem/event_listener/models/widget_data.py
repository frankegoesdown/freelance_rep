# -*- coding: utf-8 -*-

from siem.extensions import redis_store
from scapy.all import *
import json
import random

from siem.models import Widgets


class WidgetData(object):
    """Class for getting statistic data for widgets
    """
    def __init__(self, uuid):
        self.setts = self.getWidgetSetts(uuid)
        if self.setts is None:
            raise ValueError(u'Wrong UUID of widget')

    def getWidgetData(self, start_time=0, get_last=False):
        """Get data widget by UUID

        Args:
            type (string): What graph type would be use data (change return
                           data format)
            widget_uuid (string): widget UUID
            start_time (fload): data returned from this time
            delta (string): Return delta or total data

        All timestamps increased by 1000 for js timestamp format
        Returns:
            list or dict: data formatted by type
        """

        if self.setts.graph_type in ['drilldown']:
            result = self.getDrillDown()
        data = self.makeRequest(start_time, get_last)
        result = []

        if self.setts.graph_type in ['line', 'area']:
            result = self.getLine(data)

        elif self.setts.graph_type in ['pie', 'pie3d']:
            result = self.getPie(data)

        elif self.setts.graph_type == 'polar':
            result = self.getPolar(data)

        elif self.setts.graph_type in ['stackedcolumn', 'bar']:
            result = self.getStackedBar(data)

        elif self.setts.graph_type in ['gauge', 'solidgauge']:
            result = self.getGauge(data)

        elif self.setts.graph_type == 'column3d':
            result = self.getColumn3d(data)

        elif self.setts.graph_type in ['facetedLine', 'column']:
            result = self.getFacetedLine(data)
        return result

    def getWidgetSetts(self, uuid):
        return Widgets.get_by_id(uuid)

    def makeRequest(self, start_from=0, get_last=False):
        """Make request to redis for getting widget data by uuid

        Args:
            uuid (TYPE): widget uuid
            start_from (TYPE): start timestamp
            delta (TYPE): calucate delta btw values

        Returns:
            TYPE: list of dict with facets, timestamp and total values

        Raises:
            ValueError: Data not received from server
        """

        events = redis_store.keys(self.setts.id + ':*')
        if len(events) == 0:
            raise ValueError("No data received")
        if len(events) == 1:
            raise ValueError("Not enough data for delta calculating")
        events.sort()
        start = 1
        if get_last:
            if self.setts.show_delta:
                events = events[-2:]
            else:
                events = events[-1:]
                start = 0
        result = []
        last_record = None

        # restucture data, json load and adding timestamp
        values = redis_store.mget(events)
        last_ts = 0
        # save first element
        if self.setts.show_delta:
            last_record = json.loads(values[0])
            last_ts = int(float(events[0][37:]) * 1000)
        for i in range(start, len(events)):
            # if redis haven`t keys it will return None value

            if not values[i]:
                continue
            to_append = json.loads(values[i])
            to_append[u'prev_ts'] = last_ts
            # timestamp added to index like uuid:ts
            rec_timestamp = int(float(events[i][37:]) * 1000)
            last_ts = rec_timestamp

            if start_from and float(rec_timestamp) <= start_from:
                if self.setts.show_delta:
                    last_record = copy.deepcopy(to_append)
                continue
            if self.setts.show_delta:
                new = copy.deepcopy(to_append)

                # facets for getting keys of two dicts last_record
                # and to_append
                facets = copy.copy(last_record[u'facet'])
                facets.update(to_append[u'facet'])

                # set delta between facets
                for key in facets.keys():
                    to_append[u'facet'][key] = \
                        int(to_append[u'facet'].get(key, 0)) - \
                        int(last_record[u'facet'].get(key, 0))

                # set delta between total values
                to_append[u'total'] = int(to_append[u'total']) - \
                    int(last_record[u'total'])
                last_record = copy.deepcopy(new)
            # add timestamp value to result list
            to_append[u'timestamp'] = rec_timestamp
            result.append(to_append)
        return result

    def getDrillDown(self):
        # that methed almost takes the last value from storage
        events = redis_store.keys(self.setts.id + ':*')
        if len(events) == 0:
            raise ValueError("No data received")
        values = redis_store.mget(events)
        result = {
            u'series': [],
            u'drilldown': {
                u'series': []
            }
        }
        event = json.loads(values[-1])
        for sr, ddwn in event.iteritems():
            result[u'series'].append({
                u'name': sr,
                u'drilldown': sr,
                u'y': ddwn['total']
            })
            result[u'drilldown'][u'series'].append({
                u'name': sr,
                u'id': sr,
                u'data': [[a, int(b)] for a, b in ddwn['facet'].iteritems()]
            })

        return result

    def makeRequestDelta(self, start_from=0, get_last=False):
        return None

    def makeRequestNoDelta(self, start_from=0, get_last=False):
        return None

    def prapareDeltaArray(self, source):
        return None

    def getLine(self, data):
        """data fromatter to line

        Example:

        {
          "result": [
            {
              "x": "time - float",
              "y": "total - number"
            }
          ]
        }

        Args:
            data (TYPE): source data

        Returns:
            TYPE: formatted data
        """
        result = []
        for i in data:
            result.append({
                u'x': float(i['timestamp']),
                u'y': int(i['total']),
                u'prev_ts': float(i['prev_ts'])
            })
        return result

    def getPie(self, data):
        """data fromatter to pie

        Example:

        {
          "result": [
            {
              "name": "192.65.23.1 - facet[key] - string",
              "y" : "1342 - facet[value] - number"
            },
            {
              "name": "192.6.23.1 - facet[key] - string",
              "y" : "53420 - facet[value] - number"
            }
          ]
        }

        Args:
            data (TYPE): source data

        Returns:
            TYPE: formatted data
        """
        result = []
        # get last element data:
        for key, value in data[-1]['facet'].iteritems():
            to_append = {
                u'name': key,
                u'y': int(value),
                u'timestamp': data[-1]['timestamp'],
                u'prev_ts': data[-1]['prev_ts']
            }
            result.append(to_append)
        if self.setts.sorted_by_value:
            result = sorted(result, key=lambda k: k['y'])
        else:
            result = sorted(result, key=lambda k: k['name'].lower())
        return result

    def getColumn(self, data):
        return self.getFacetedLine(data)

    def getColumn3d(self, data):
        """data fromatter to column 3d - last value

        Example:

        {
          "result": [{
            "categories": 1231231233
          },{
            "series": [{
              "name": "192.168.10.11",
              "data": [132221]
            }, {
              "name": "192.168.20.54",
              "data": [12223]
            }, {
              "name": "192.168.7.211",
              "data": [555]
            }]
          }
          ]
        }

        Args:
            data (TYPE): source data

        Returns:
            TYPE: formatted data
        """
        result = {}
        result[u'categories'] = data[-1]['timestamp'] / 1000
        series = []
        # get last element data:
        for key, value in data[-1]['facet'].iteritems():
            series.append({
                u'name': key,
                u'data': [int(value)]
            })
        if not self.setts.sorted_by_value:
            series = sorted(series, key=lambda k: k['name'].lower())
        else:
            series = sorted(series, key=lambda k: k['data'][0])
        result[u'series'] = series
        result[u'timestamp'] = data[-1]['timestamp'],
        result[u'prev_ts'] = data[-1]['prev_ts']
        return result

    def getFacetedLine(self, data):
        """data fromatter to pie

        Example:

        {
          "result": [{
              "name": "0.0.0.0 facet[key] - str",
              "data": [{
                "x": "1464166844880 - timestamp * 1000 - number",
                "y": "4157602 facet[value] - number"
              },{
                "x": "1464166854880 - timestamp * 1000 - number",
                "y": "4157602 facet[value] - number"
              }]
            },{
              "name": "192.168.2.10 facet[key] - str",
              "data": [{...},{...}]
            }
          ]
        }

        Args:
            data (TYPE): source data

        Returns:
            TYPE: formatted data
        """
        all_facets = []
        response = []
        for i in data:
            for key in i['facet']:
                if key not in all_facets:
                    all_facets.append(key)
        if not self.setts.sorted_by_value:
            all_facets = sorted(all_facets, key=unicode.lower)

        # zero data by facets
        if not all_facets:
            new_seria = {
                'name': 'no data',
                'data': []
            }
            for i in data:
                new_seria['data'].append(
                    {
                        'x': float(i['timestamp']),
                        'y': 0,
                        'prev_ts': float(i['prev_ts'])
                    })
            response.append(new_seria)
            return response

        for key in all_facets:
            new_seria = {
                'name': key,
                'data': []
            }
            for i in data:
                if key in i['facet']:
                    new_seria['data'].append(
                        {
                            'x': float(i['timestamp']),
                            'y': int(i['facet'][key]),
                            'prev_ts': float(i['prev_ts'])
                        })
                else:
                    new_seria['data'].append(
                        {
                            'x': float(i['timestamp']),
                            'y': 0,
                            'prev_ts': float(i['prev_ts'])
                        }
                    )
            response.append(new_seria)

        if self.setts.sorted_by_value:
            response = sorted(response, key=lambda k: k["data"][0]["y"])
        return response

    def getStackedBar(self, data):
        return []

    def getPolar(self, data):
        """Format data for polar graph

        Example:
            {
                "result": [
                    {
                        "categories": ["192.168.24.21", "192.163.24.21"],
                        "data":[100, 200]
                    }
                ]
            }

        Args:
            data (TYPE): dource data

        Returns:
            TYPE: formatted data
        """

        result = {
            'categories': [],
            'data': [],
            'timestamp': data[-1]['timestamp'],
            'prev_ts': data[-1]['prev_ts']
        }
        before = None
        if self.setts.sorted_by_value:
            before = sorted(data[-1]['facet'].items(), key=lambda k: int(k[1]))
        else:
            before = sorted(data[-1]['facet'].items(),
                            key=lambda k: k[0].lower())
        for key, value in before:
            result['categories'].append(key)
            result['data'].append(int(value))
        return result

    def getGauge(self, data):
        return {u'data': [random.randint(1, 100)]}

