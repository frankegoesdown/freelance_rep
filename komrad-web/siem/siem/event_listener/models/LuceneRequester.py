# -*- coding: utf-8 -*-

from flask import current_app

from scapy.all import *
import requests
import json

from flask import current_app as app
from exceptions import NoEventsReceived


class LuceneRequester(object):
    """Class includes all request to backends servers like lucene, redis or
    lvldb (like trait for functions)
    """

    # default timeout for requests:
    __def_request_timeout = 0.8

    def __init__(self):
        pass

    def make_request(self, source, request, parameters,
                     timeout=__def_request_timeout,
                     filter=None, nodes=['leveldb1']):
        """
        make requert to leveldb server
        :param source:
        :param request: type of request
        :param parameters: request parameter
        :param nodes:
        :param filter:
        :return:
        """
        url = "%s%s" % (app.config['FRAMEWORK_URL'], source)
        if filter:
            parameters['filter'] = filter
        data = {'request': request, 'parameters': json.dumps(parameters),
                'nodes': json.dumps(nodes)}
        try:
            reply = requests.post(url, data=data, timeout=timeout)
        except requests.exceptions.Timeout:
            return u'[]'
        except requests.exceptions.ConnectionError:
            return None
        return reply.text

    @classmethod
    def lvldb_range_request(self, ts_start, ts_end, query):
        """Make request to level db

        :param ts_start: from timestamp
        :param ts_end: to timestamp
        :param query: filter from query-builder
        :return: json data response
        """
        return self().make_request('leveldb', 'range',
            {'start': int(ts_start), 'end': int(ts_end)},
            __def_request_timeout)

    @staticmethod
    def getEvents(key):
        """Return detailed data of key or keys

        :param key: string or list - if string - key is single,
                    if list - we have many keys
        :return: json data response
        """
        response = None
        if type(key) == list:
            url = current_app.config['LUCENE_POST_URL'] + 'events_list'
            response = requests.post(url, json={"keys": key})
        if type(key) == str or type(key) == unicode:
            url = current_app.config['LUCENE_POST_URL'] + 'event?id=' + key
            response = requests.get(url)

        if not response:
            raise TypeError('Invalid data type')
        if response.status_code == 400:
            raise NoEventsReceived('Keys argument not found in request string')
        if response.status_code == 404:
            raise NoEventsReceived('Events not found')
        if response.status_code == 500:
            raise NoEventsReceived('Unexpected exception')

        return json.loads(response.content)

    @staticmethod
    def lucene_request(query, page=1, rusult='100', sort_by='event_id',
                       sort_reverse=True, time_from=0, time_to=0):
        """Make request to lucene

        :param query: filter from query-builder
        :return: json data response
        """

        data = {
            "query": query,
            'results': rusult,
            'sort_by': sort_by,
            'sort_reverse': sort_reverse,
            'page': page,
            'from': time_from,
            'to': time_to
        }
        reply = None
        try:
            reply = requests.post(app.config['LUCENE_POST_URL'] + 'search',
                                  json=data)
        except requests.exceptions.Timeout:
            return u'[]'
        except requests.exceptions.ConnectionError:
            return None
        return reply.text

    @staticmethod
    def timerangeRequest(time_from, time_to=0, count=10, query_body="{}"):
        """Getter data by time range

        Args:
            time_from (string): From time point
            time_to (int, string): To time point
            count (int, optional): Count of returned rows
            query_body (str, optional): Description

        Returns:
            dict: {result: list of rows, total: count (integer)}
        """
        if count == 0:
            count = 1

        js = {"query": {
            "rules": [{
                "value": [
                    '%s0000000' % time_from,
                    '%s0000000' % time_to],
                "field": "event_id",
                "operator": "between",
                "input": "text",
                "type": "string",
                "id": "event_id"
            }],
            "condition": "AND"
        },
            "sort_by": "event_id",
            "sort_reverse": "false",
            "results": str(count)
        }

        return json.loads(requests.post(current_app.config['LUCENE_POST_URL'] +
            'search', json=js).text)

