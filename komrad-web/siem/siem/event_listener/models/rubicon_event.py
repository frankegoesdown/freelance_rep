# -*- coding: utf-8 -*-

from binascii import unhexlify, hexlify
from scapy.all import *
import struct
import zlib
import time
import datetime
import hexdump
import xmltodict

from flask import current_app as app
from flask_login import current_user


def barnyard_to_binary(packet_data, packet_ms, event_ts,
                       packet_length):
    sensor_id = 0
    event_id = 1000
    linktype = 1
    # packet_second = time.mktime(
    #     datetime.datetime.strptime(string_date,
    #                                "%Y-%m-%d %H:%M:%S").timetuple())
    snort_header = struct.pack("!IIIIIII", sensor_id, event_id,
                               int(event_ts), int(event_ts),
                               int(packet_ms), linktype,
                               int(packet_length))
    binary_data = hexlify(zlib.compress(unhexlify(hexlify(snort_header) +
                                                  packet_data)))
    return binary_data


class RubiconPCAP(object):
    """Event data processor for Rubican PCAP files

    I don`t know how it work, I copied it from old comrad project.

    Attributes:
        binary_data (TYPE):  magic value
        pcap_file (string): path to temp pcap file
        PCAP_MAGIC_NUMBER (int): magic value
        PCAP_SIGFIGS (int): magic value
        PCAP_SNAPLEN (int): magic value
        PCAP_VERSION_MAJOR (int): magic value
        PCAP_VERSION_MINOR (int): magic value
        xml_data (TYPE):  magic value
    """
    PCAP_MAGIC_NUMBER = 0xa1b2c3d4L
    PCAP_VERSION_MAJOR = 2
    PCAP_VERSION_MINOR = 4
    PCAP_SNAPLEN = 65535

    CONDITION = {'plugin_id': 1011}

    def __init__(self, event):
        self.event = event
        if not self.checkEvent():
            raise AssertionError("I haven't enough data")

        self.xml_data = None
        self.pcap_file = app.config['PCAP_FILES_PATH'] + \
            current_user.username \
            + str(time.time())

        self.hd = hexdump.hexdump(
            hexdump.restore(str(event['packet'])), result='return')
        self.binary_data = barnyard_to_binary(
            event['packet'], event['packet_ms'], event['timestamp_event'],
            event['packet_length'])

        self.pcup_data = self.getPacketData()
        self.tree = self.getXmlTree()

    def checkEvent(self):
        print self.event.keys()
        for v in ['packet', 'packet_ms', 'timestamp_event', 'packet_length']:
            if v not in self.event.keys():
                return False
        return True

    def getExtentionData(self):
        return {
            'hexdump': unicode(self.hd),
            'pcup_tree': self.getXmlTree()
        }

    def getXmlTree(self):
        return xmltodict.parse(self.xml_data)['pdml']['packet']

    def getPacketData(self):
        tmpdata = unhexlify(self.binary_data)
        bin_data = zlib.decompress(tmpdata)

        sensor_id, event_id, event_second, packet_second, packet_ms, \
            linktype, packet_length = struct.unpack("!IIIIIII", bin_data[0:28])

        # Extract the real packet.
        packet_data = bin_data[28:]

        result = ''

        # global header
        with open(self.pcap_file, 'w') as fd:
            result += struct.pack('IHHIIII',
                                  self.PCAP_MAGIC_NUMBER,
                                  self.PCAP_VERSION_MAJOR,
                                  self.PCAP_VERSION_MINOR,
                                  0,
                                  0,
                                  packet_length,
                                  linktype)

            # packet header
            result += struct.pack('IIII', packet_second, packet_ms,
                                  packet_length, packet_length)

            result += packet_data
            fd.write(result)

        p = subprocess.Popen(['tshark', '-V', '-r', self.pcap_file, '-T',
                              'pdml'], stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        out, err = p.communicate()
        self.xml_data = out

        # rm tmp file
        os.remove(self.pcap_file)

        return result

