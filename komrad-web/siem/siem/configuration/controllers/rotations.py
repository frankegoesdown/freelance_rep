# -*- coding: utf-8 -*-

from flask import render_template, jsonify, request, abort

from siem.wrappers import access_required
from siem.configuration import configs
from ..models.rotations import RotationRequester
import siem.route_access as ra


@configs.route("/rotations", methods=["GET"])
@access_required(ra.PAGE_ROTATIONS)
def ajax_rotation():
    return render_template('configuration/rotations.html')


@configs.route("/rotations/all", methods=["GET"])
@access_required(ra.PAGE_ROTATIONS)
def get_all_rotations():
    data = RotationRequester.getAll()
    return jsonify(data), 200


@configs.route("/rotations/cfg", methods=["GET"])
@access_required(ra.PAGE_ROTATIONS)
def get_config_rotations():
    data = RotationRequester.getConfig()
    return jsonify(data), 200


@configs.route("/rotations/cfg", methods=["POST"])
@access_required(ra.PAGE_ROTATIONS)
def update_config_rotations():
    res = RotationRequester.upConfig(request.get_json())
    return jsonify({'status': res}), 200


@configs.route("/rotation/<int:_id>", methods=["GET"])
@access_required(ra.PAGE_ROTATIONS)
def get_rotation(_id):
    return jsonify({'success': True}), 200


@configs.route("/rotations", methods=["POST"])
@access_required(ra.PAGE_ROTATIONS)
def update_rotation():
    to_state = request.get_json().get(u'to_state', None)
    if not to_state:
        abort(400)
    id_list = request.get_json().get(u'id_list', None)
    if not id_list:
        abort(400)

    # id_list = [str(x) for x in id_list]

    if to_state == 'archieve':
        RotationRequester.toRestoredArchive(id_list)

    elif to_state == 'remove':
        RotationRequester.toNone(id_list)

    elif to_state == 'restore':
        RotationRequester.toRestored(id_list)

    elif to_state == 'save':
        RotationRequester.toRestored(id_list)

    return jsonify({'success': True}), 200

@configs.route("/rotations/calc", methods=["POST"])
def calc_rotation():
    to_state = request.get_json().get(u'to_state', None)
    if not to_state:
        abort(400)
    id_list = request.get_json().get(u'id_list', None)
    if not id_list:
        abort(400)

    elif to_state == 'calculate':
        RotationRequester.calculate(id_list)

    return jsonify({'success': True}), 200	
	

@configs.route("/rotations/comment", methods=["POST"])
@access_required(ra.PAGE_ROTATIONS)
def update_rotation_comment():
    comment = request.get_json().get(u'comment', None)
    if not comment:
        abort(400)
    id = request.get_json().get(u'id', None)
    if not id:
        abort(400)

    RotationRequester.setComment(id, comment)

    return jsonify({'success': True}), 200