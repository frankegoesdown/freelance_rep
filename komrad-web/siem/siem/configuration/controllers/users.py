# -*- coding: utf-8 -*-

from flask import render_template, jsonify, request

from siem.wrappers import access_required
from siem.configuration import configs
from siem.models import User, UsersToGroups, Group
import siem.route_access as ra
from siem.extensions import db


@configs.route('/users-and-roles', methods=["GET"])
@access_required(ra.PAGE_USERS)
def ajax_users_and_roles():
    """
    """
    return render_template("configuration/users_and_other.html")


@configs.route('/users/all', methods=["GET"])
@access_required(ra.PAGE_USERS)
def users_get_list():
    """
    """
    return jsonify({'response':
        [i.serialize for i in User.query.filter().all()]}), 200


@configs.route("/user", methods=["POST"])
@access_required(ra.PAGE_USERS)
def create_user_controller():
    """Users adder and updater
    POST for adding and PUT for updating"""
    groups = request.get_json().get('groups', [])
    groups = [int(x) for x in groups]
    username = request.get_json().get('username')
    email = request.get_json().get('email')
    role = request.get_json().get('role')
    description = request.get_json().get('description', '')
    first_name = request.get_json().get('first_name', '')
    last_name = request.get_json().get('last_name', '')

    password = request.get_json().get('password')
    img = request.get_json().get('image_id', None)
    User.add(username, email, password, role, description, first_name,
             last_name, img, groups)
    return jsonify({'success': True}), 200


@configs.route("/user", methods=["PUT"])
@access_required(ra.PAGE_USERS)
def update_user_controller():
    """Users adder and updater
    POST for adding and PUT for updating"""
    _id = request.get_json().get('id')
    user = User.get_by_id(_id)

    user.username = request.get_json().get('username', user.username)
    user.email = request.get_json().get('email', user.email)
    user.role = request.get_json().get('role', user.role)
    user.description = request.get_json().get('description', user.description)
    user.first_name = request.get_json().get('first_name', user.first_name)
    user.last_name = request.get_json().get('last_name', user.last_name)
    user.image = request.get_json().get('image_id', user.image)
    # User.update(_id, username, email, role,
    #             description, first_name, last_name, img, groups)

    groups = request.get_json().get('groups', None)
    if groups:
        groups = [int(x) for x in groups]
        old_groups = UsersToGroups.query.filter_by(user_id=_id).all()
        for of in old_groups:
            db.session.delete(of)
        for i in groups:
            assoc = UsersToGroups()
            assoc.group_id = i
            user.groups.append(assoc)
        if not Group.getDefaultId() in groups:
            groups.append(Group.getDefaultId())

    access_map = request.get_json().get('access_map', None)
    if access_map:
        User.setAccessMap(_id, access_map)

    access_map = request.get_json().get('access_map', None)
    if access_map:
        User.setAccessMap(_id, access_map)

    password = request.get_json().get('password', None)
    if password:
        User.updatePassword(_id, password)

    db.session.commit()
    return jsonify({'success': True}), 200



@configs.route("/user/<int:_id>", methods=["DELETE"])
@access_required(ra.PAGE_USERS)
def delete_user_controller(_id):
    User.assasinate(_id)
    return jsonify({'success': True}), 200


@configs.route("/user/restore", methods=["POST"])
@access_required(ra.PAGE_USERS)
def restore_user_controller():
    _id = request.get_json().get('id')
    User.ressurect(_id)
    return jsonify({'success': True}), 200
