# -*- coding: utf-8 -*-

from flask import Blueprint, render_template

from siem.wrappers import access_required
from siem.configuration import configs
from ..models.journals import Journals
import siem.route_access as ra


@configs.route('/journals', methods=["GET"])
@access_required(ra.PAGE_JOURNALS)
def ajax_journals():
    """
    """
    jrn = Journals()
    return render_template(
        "configuration/journals.html", data=jrn.getLast100lines())
