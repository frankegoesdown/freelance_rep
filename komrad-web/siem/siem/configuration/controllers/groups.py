# -*- coding: utf-8 -*-

from flask import jsonify, request

from siem.wrappers import access_required
from siem.configuration import configs
from siem.models import Group
import siem.route_access as ra


@configs.route('/group/all', methods=["GET"])
@access_required(ra.PAGE_USERS)
def groups_get_list():
    """
    """
    return jsonify({'response': Group.getGroupes()}), 200


@configs.route("/group", methods=["POST"])
@access_required(ra.PAGE_USERS)
def group_create():
    to_add = request.get_json()
    wanna_ressurect = to_add.get('restore', False)
    if wanna_ressurect:
        Group.ressurect(to_add)
        return jsonify({'success': True})

    to_add['is_alone'] = 0
    res, msg = Group.add(to_add)
    print res, msg
    if res:
        return jsonify({'success': True})
    return jsonify({'success': False, 'err_msg': 'duplicate'}), 400


@configs.route("/group", methods=["PUT"])
@access_required(ra.PAGE_USERS)
def group_update():
    _id = request.get_json().get('id')
    Group.update(_id, request.get_json())
    return jsonify({'success': True})


@configs.route("/group/<int:_id>", methods=["DELETE"])
@access_required(ra.PAGE_USERS)
def delete_group_controller(_id):
    Group.assasinate(_id)
    return jsonify({'success': True}), 200
