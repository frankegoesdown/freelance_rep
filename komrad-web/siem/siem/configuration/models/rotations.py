# -*- coding: utf-8 -*-

from flask_login import current_user

import requests
import json
import time
import datetime
import uuid

from flask import current_app as app
from itertools import ifilter


class RotationRequester(object):

    def getRotations(self):
        data = requests.get(app.config['ROTATIONS_URL'] + 'get_all')
        if data.status_code != requests.codes.ok:
            return []
        return json.loads(data.content)

    def checkSwitchs(self, rec, status):
        return True

    @classmethod
    def getAll(self):
        data = self().getRotations()
        result = []
        i = 0
        for d in data:
            i += 1
            if d['status'] in ['none', 'tomorrow']:
                continue
            d.update({
                'dt': int(time.mktime(datetime.datetime.strptime(d['dir_name'], "%Y_%m_%d").timetuple())) * 1000
            })
            # hide inner dirs structure
            del d['dir_name']
            del d['from']
            del d['to']
            result.append(d)
        return result

    @classmethod
    def calculate(self, ids):
        rotations = self().getRotations()
        for_send = []
        for elem in ifilter(lambda x: x['id'] in ids, rotations):
            if elem['status'] != 'restored' and elem['status'] != 'readonly':
                raise (AssertionError('wrong status to switch'))
            for_send.append(elem)
        for i in for_send:
            data = json.dumps({"catalog_name": i['dir_name']})
            res = requests.post(app.config['ROTATIONS_URL'] +
                                'inspect', data=data)
            if res.status_code != requests.codes.ok:
                pass
        return True

    @classmethod
    def toNone(self, ids):
        rotations = self().getRotations()
        for_send = []
        for elem in ifilter(lambda x: x['id'] in ids, rotations):
            if not self().checkSwitchs(elem, 'none'):
                raise(AssertionError('wrong status to switch'))
            for_send.append(elem)
        for i in for_send:
            data = json.dumps({"catalog_name": i['dir_name']})
            res = requests.post(app.config['ROTATIONS_URL'] +
                'to_none', data=data)
            if res.status_code != requests.codes.ok:
                pass
        return True

    @classmethod
    def toRestored(self, ids):
        rotations = self().getRotations()
        for_send = []
        for elem in ifilter(lambda x: x['id'] in ids, rotations):
            if not self().checkSwitchs(elem, 'restored'):
                raise(AssertionError('wrong status to switch'))
            for_send.append(elem)
        for i in for_send:
            data = json.dumps({"catalog_name": i['dir_name']})
            res = requests.post(app.config['ROTATIONS_URL'] +
                'to_restored', data=data)
            if res.status_code != requests.codes.ok:
                pass
        return True

    @classmethod
    def toRestoredArchive(self, ids):
        rotations = self().getRotations()
        for_send = []
        for elem in ifilter(lambda x: x['id'] in ids, rotations):
            if not self().checkSwitchs(elem, 'restored_archive'):
                raise(AssertionError('wrong status to switch'))
            for_send.append(elem)
        for i in for_send:
            data = json.dumps({"catalog_name": i['dir_name']})
            res = requests.post(app.config['ROTATIONS_URL'] +
                'to_restored_archive', data=data)
            if res.status_code != requests.codes.ok:
                pass
        return True

    @classmethod
    def setComment(self, id, comment):
        rotations = self().getRotations()
        record = next(item for item in rotations if item["id"] == id)
        if not record:
            return False
        data = json.dumps({
            "catalog_name": record['dir_name'],
            "comment": comment
        })
        res = requests.post(app.config['ROTATIONS_URL'] + 'add_comment',
            data=data)
        if res.status_code != requests.codes.ok:
            pass
        return True

    @staticmethod
    def getConfig():
        data = requests.get(app.config['ROTATIONS_URL'] + 'get_config')
        if data.status_code != requests.codes.ok:
            return []
        data = json.loads(data.content)
        return data

    @staticmethod
    def upConfig(settings):
        to_send = {}
        if 'ttl_readonly' in settings:
            to_send['ttl_readonly'] = settings['ttl_readonly']
        if 'ttl_archive' in settings:
            to_send['ttl_archive'] = settings['ttl_archive']
        if not to_send:
            return False
        res = requests.post(app.config['ROTATIONS_URL'] + 'update_config',
            data=json.dumps(to_send))
        if res.status_code != requests.codes.ok:
            return False
        return True


