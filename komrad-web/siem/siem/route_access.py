# -*- coding: utf-8 -*-
"""Установка роутов фреймворка, достижимости запросов для контретных страниц
Все роуты разделены на область видимости, причем часть роутов должна быть
даступна при наличии нескольких атрибутов, напимер в проекте есть роут для
запроса полей нормализации, достпность которого необходима почти каждой
странице

логика проверки оформлена в файле siem.wrappers

Следующие атрибуты однозначно характеризуют страницу:
    FILES_ACCESS (пусть к доступу файлов системы, вполне возможно что тут будет
                  дополнительная функция уже напрямую связанная с сессией
                  пользователя)
    PAGE_CORRESPONDENCIES
    PAGE_DIR_CONSTRUCTOR
    PAGE_EVENTVISIO
    PAGE_FACTS_BASE
    PAGE_INCIDENTS
    PAGE_JOURNALS
    PAGE_LICENSE
    PAGE_NAGVIS_NAGIOS
    PAGE_PLUGINS
    PAGE_QUERIES
    PAGE_REALTIME
    PAGE_ROTATIONS
    PAGE_SEARCH
    PAGE_USERS
    PAGE_WIDGETS
"""

from flask_babel import lazy_gettext as _

PAGE_SEARCH = ('PAGE_SEARCH',)
PAGE_REALTIME = ('PAGE_REALTIME',)
PAGE_WIDGETS = ('PAGE_WIDGETS',)
PAGE_QUERIES = ('PAGE_QUERIES',)
PAGE_CORRESPONDENCIES = ('PAGE_CORRESPONDENCIES',)
PAGE_DIR_CONSTRUCTOR = ('PAGE_DIR_CONSTRUCTOR',)
PAGE_INCIDENTS = ('PAGE_INCIDENTS',)
PAGE_EVENTVISIO = ('PAGE_EVENTVISIO',)
PAGE_FACTS_BASE = ('PAGE_FACTS_BASE',)
PAGE_LICENSE = ('PAGE_LICENSE',)
PAGE_JOURNALS = ('PAGE_JOURNALS',)
PAGE_USERS = ('PAGE_USERS',)
PAGE_PLUGINS = ('PAGE_PLUGINS',)
PAGE_ROTATIONS = ('PAGE_ROTATIONS',)
PAGE_NAGIOS = ('PAGE_AVAILABILITY',)
PAGE_NAGVIS = ('PAGE_MAP',)
PAGE_RESOURCES_CONTROL = ('PAGE_RESOURCES_CONTROL',)
FILES_ACCESS = ('FILES_ACCESS',) # rewrited later

ACCESS_DOMAINS_ALL = PAGE_SEARCH + PAGE_REALTIME + PAGE_WIDGETS + \
    PAGE_QUERIES + \
    PAGE_CORRESPONDENCIES + PAGE_DIR_CONSTRUCTOR + PAGE_INCIDENTS +\
    PAGE_EVENTVISIO + PAGE_FACTS_BASE + PAGE_LICENSE + PAGE_JOURNALS +\
    PAGE_USERS + PAGE_PLUGINS + PAGE_ROTATIONS + PAGE_NAGIOS +\
    PAGE_NAGVIS + PAGE_RESOURCES_CONTROL

FILES_ACCESS = FILES_ACCESS + ACCESS_DOMAINS_ALL

ACCESS_EVENT_DATA = PAGE_SEARCH + PAGE_REALTIME + PAGE_WIDGETS + \
    PAGE_QUERIES +\
    PAGE_QUERIES + PAGE_INCIDENTS +\
    PAGE_EVENTVISIO

ACCESS_FIELDS = PAGE_SEARCH + PAGE_REALTIME + PAGE_WIDGETS + PAGE_QUERIES +\
    PAGE_QUERIES + PAGE_DIR_CONSTRUCTOR + PAGE_INCIDENTS +\
    PAGE_EVENTVISIO + PAGE_FACTS_BASE


ACCESS_QUERIES = PAGE_SEARCH + PAGE_WIDGETS + PAGE_SEARCH +\
    PAGE_QUERIES + PAGE_EVENTVISIO


# именно из этого списка в будущем должено будет формироваться меню
# никак иначе, чтобы связать взаимодействие модулей в одном файле.
# было бы хорошо и роуты сюда высветить на основе этого словаря, а потом
# объединить их сложением структур, но
# фласк довольно ущербен и middleware слой socket-io engine ломает все на
# корню своим особенный реквест контекстом, к тому же совершенно непонятно
# как добавлять эти ограничения туда
# лучший вариант это свинтить в сторону торнадо или что еще лучше asyncio
MENU_STRUCTURE = [
    {
        'name': 'widgets',
        'access_domain': PAGE_WIDGETS[0],
        'label': _(u'Widgets')
    },
    {
        'name': 'realtime',
        'access_domain': PAGE_REALTIME[0],
        'label': _(u'Realtime data')
    },
    {
        'name': 'sources',
        'access_domain': PAGE_RESOURCES_CONTROL,
        'label': _(u'Resources'),
        'children': [
            {
                'name': 'Assets control',
                'access_domain': PAGE_RESOURCES_CONTROL[0],
                'label': _(u'Resource control')
            }
        ]
    },
    {
        'name': 'events',
        'access_domain': PAGE_SEARCH + PAGE_QUERIES,
        'label': _(u'Security events'),
        'children': [
            {
                'name': 'search',
                'access_domain': PAGE_SEARCH[0],
                'label': _(u'Search events')
            },
            {
                'name': 'query_list',
                'access_domain': PAGE_QUERIES[0],
                'label': _(u'Queries')
            }
        ]
    },
    {
        'name': 'correspondences',
        'access_domain': PAGE_CORRESPONDENCIES,
        'label': _(u'Compliance controle'),
        'children': [
            {
                'name': 'correspondences',
                'access_domain': PAGE_CORRESPONDENCIES[0],
                'label': u'ГОСТ Р ИСО/МЭК 27001-2006'
            }
        ]
    },
    {
        'name': 'correlation',
        'access_domain': PAGE_INCIDENTS + PAGE_DIR_CONSTRUCTOR,
        'label': _(u'Compliance controle'),
        'children': [
            {
                'name': 'dirconstructor',
                'access_domain': PAGE_DIR_CONSTRUCTOR[0],
                'label': _(u'Dir constructor')
            },
            {
                'name': 'alerts',
                'access_domain': PAGE_INCIDENTS[0],
                'label': _(u'Alerts list')
            }
        ]
    },
    {
        'name': 'eventvisio',
        'access_domain': PAGE_FACTS_BASE + PAGE_EVENTVISIO,
        'label': _(u'Analytics'),
        'children': [
            {
                'name': 'fblists',
                'access_domain': PAGE_FACTS_BASE[0],
                'label': _(u'Base of facts')
            },
            {
                'name': 'eventvisio',
                'access_domain': PAGE_EVENTVISIO[0],
                'label': _(u'Events Visualizer')
            }
        ]
    },
    {
        'name': 'avmonitor',
        'access_domain': PAGE_NAGIOS + PAGE_NAGVIS,
        'label': _(u'Availabilty monitoring'),
        'children': [
            {
                'name': 'availability',
                'access_domain': PAGE_NAGIOS[0],
                'label': _(u'Availability')
            },
            {
                'name': 'map',
                'access_domain': PAGE_NAGVIS[0],
                'label': _(u'The MAP')
            }
        ]
    },
    {
        'name': 'settings',
        'access_domain': (PAGE_LICENSE + PAGE_JOURNALS + PAGE_USERS +
                          PAGE_ROTATIONS + PAGE_PLUGINS),
        'label': _(u'Administration'),
        'children': [
            {
                'name': 'license',
                'access_domain': PAGE_LICENSE[0],
                'label': _(u'License control')
            },
            {
                'name': 'journals',
                'access_domain': PAGE_JOURNALS[0],
                'label': _(u'Journals')
            },
            {
                'name': 'usersandroles',
                'access_domain': PAGE_USERS[0],
                'label': _(u'Users')
            },
            {
                'name': 'rotations',
                'access_domain': PAGE_ROTATIONS[0],
                'label': _(u'Events storage')
            },
            {
                'name': 'plugins',
                'access_domain': PAGE_PLUGINS[0],
                'label': _(u'Plugins')
            }
        ]
    }
]


# я не хотел пропускать имена модулей в том же виде наружу
# тут довольно приебистый костыль
def domainAsDict(k, v):
    res = {}

    def addChildren(_child_list):
        for child in _child_list:
            res[child[k]] = child[v]

    for route in MENU_STRUCTURE:
        if 'children' in route.keys():
            addChildren(route['children'])
            continue
        res[route[k]] = route[v]

    return res


ACCESS_OUTER_NAMES = domainAsDict('access_domain', 'name')
ACCESS_INNER_NAMES = domainAsDict('name', 'access_domain')
ACCESS_LABELS = domainAsDict('access_domain', 'label')



# def setup_routes(app):

#     route = app.add_url_rule

#     from auth.controller import *
#     route('/', view_func=login, methods=["GET", "POST"])
#     route('/login', view_func=login, methods=["GET", "POST"])
#     route('/logout', view_func=logout, methods=["GET", "POST"])
#     route('/about_me', view_func=get_current_user_data, methods=["GET"])

#     from event_listener.controllers import *

#     access_fields = PAGE_SEARCH + PAGE_REALTIME + PAGE_WIDGETS + PAGE_QUERIES +\
#         PAGE_QUERIES + PAGE_DIR_CONSTRUCTOR + PAGE_INCIDENTS +\
#         PAGE_EVENTVISIO + PAGE_FACTS_BASE

#     route('/index', view_func=index, methods=["GET"], access_groups=ACCESS_DOMAINS)
#     route('/summary', view_func=ajax_summary, methods=["GET"], access_groups=PAGE_WIDGETS)

#     route('/search/', view_func=ajax_query_constructor, methods=["GET"], access_groups=PAGE_SEARCH)
#     route('/search/<int:query_id>', view_func=ajax_query_constructor, methods=["GET"], access_groups=PAGE_SEARCH)
#     route('/fullrequest', view_func=get_events_by_qb, methods=["POST"], access_groups=PAGE_SEARCH)
#     route('/range_slider', view_func=get_range_slider, methods=["POST"], access_groups=PAGE_SEARCH)

#     route('/fields/tree', view_func=get_event_fields_tree, methods=["GET"], access_groups=access_fields)
#     route('/fields/all', view_func=get_event_fields, methods=["GET"], access_groups=access_fields)
#     route('/fields/all/<int:faceted>', view_func=get_event_fields, methods=["GET"], access_groups=access_fields)
#     route('/event/getpcap/<string:id>', view_func=get_event_pcap, methods=["GET"], access_groups=access_fields)
#     route('/event/<string:id>', view_func=get_event_datail, methods=["GET"], access_groups=access_fields)
#     route('/eventvisio', view_func=ajax_event_visio, methods=["GET"], access_groups=access_fields)

#     access_fields = PAGE_SEARCH + PAGE_WIDGETS + PAGE_SEARCH +\
#         PAGE_QUERIES + PAGE_EVENTVISIO

#     route('/querylist', view_func=ajax_query_list, methods=["GET"], access_groups=PAGE_SEARCH)
#     route('/querylist/all', view_func=json_query_list, methods=["GET"], access_groups=access_fields)
#     route('/query/body', view_func=query_controller_getter, methods=["GET"], access_groups=access_fields)
#     route('/query/body/<int:query_id>', view_func=query_controller_getter, methods=["GET"], access_groups=access_fields)
#     route('/query/body', view_func=query_controller_updater, methods=["POST"], access_groups=access_fields)
#     route('/query/body/<int:query_id>', view_func=query_controller_updater, methods=["DELETE"], access_groups=access_fields)

#     route('/realtime', view_func=ajax_realtime, methods=["GET"], access_groups=PAGE_REALTIME)

#     route('/widget/<string:widget_uuid>/<float:start_time>', view_func=widgets_controller_getter, methods=["GET"], access_groups=PAGE_WIDGETS)
#     route('/widget/<string:widget_uuid>/<int:start_time>', view_func=widgets_controller_getter, methods=["GET"], access_groups=PAGE_WIDGETS)
#     route('/widget/report/<string:widget_uuid>', view_func=make_widget_report,  methods=["GET"], access_groups=PAGE_WIDGETS)
#     route('/widget/<string:widget_uuid>', view_func=get_widget, methods=["GET"], access_groups=PAGE_WIDGETS)
#     route('/widget/add', view_func=create_widgets_list, methods=["POST"], access_groups=PAGE_WIDGETS)
#     route('/user/widgets', view_func=widget_page_controller, methods=["POST"], access_groups=PAGE_WIDGETS)
#     route('/widget/<string:widget_uuid>', view_func=update_widgets_list, methods=["PUT"], access_groups=PAGE_WIDGETS)
#     route('/widget/<string:widget_uuid>', view_func=delete_widget, methods=["DELETE"], access_groups=PAGE_WIDGETS)

#     from facts_base.controllers import *
#     route('/bof/lists', view_func=ajax_all_lists, methods=["GET"], access_groups=PAGE_FACTS_BASE)
#     route('/bof/all', view_func=get_all_lists, methods=["GET"], access_groups=PAGE_FACTS_BASE)

#     route('/bof/clist/<string:_id>', view_func=get_custom_list, methods=["GET"], access_groups=PAGE_FACTS_BASE)
#     route('/bof/clist/', view_func=create_custom_list, methods=["POST"], access_groups=PAGE_FACTS_BASE)
#     route('/bof/clist/', view_func=update_custom_list, methods=["PUT"], access_groups=PAGE_FACTS_BASE)
#     route('/bof/clist/<string:_id>', view_func=delete_custom_list, methods=["DELETE"], access_groups=PAGE_FACTS_BASE)

#     route('/bof/alist/<string:_id>', view_func=get_auto_list, methods=["GET"], access_groups=PAGE_FACTS_BASE)
#     route('/bof/alist/', view_func=create_auto_list, methods=["POST"], access_groups=PAGE_FACTS_BASE)
#     route('/bof/alist/', view_func=update_auto_list, methods=["PUT"], access_groups=PAGE_FACTS_BASE)
#     route('/bof/alist/<string:_id>', view_func=delete_auto_list, methods=["DELETE"], access_groups=PAGE_FACTS_BASE)

#     from correspondences.controllers import *
#     route('/correspondences', view_func=ajax_correspondencies, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correspondences/download', view_func=pdf_correspondencies, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correspondences/switchimpl', view_func=switch_selected, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correspondences/switchsl', view_func=switch_implemented, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correspondences/set_comment', view_func=set_comment, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)

#     from correlation.controllers import *
#     route('/correl/alert_page', view_func=ajax_alert_page, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/alerts/all', view_func=ajax_get_alerts_list, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/incidents/page', view_func=get_incidents_by_request, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/alerts/stat', view_func=json_get_total_alerts, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/alerts/drilldown', view_func=json_get_alert_drilldown, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/alert', view_func=update_alert, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/alert/<string:uuid>', view_func=alert_data, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/incidents/set_states', view_func=mass_change_state, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/incidents/report', view_func=make_inc_report, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/incidents/export', view_func=make_inc_csv, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/assign_list', view_func=get_assign_list, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/groups/all', view_func=get_groups, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)

#     route('/dirconstructor', view_func=ajax_dircons, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/dir/<int:_id>', view_func=get_directive, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/uuid', view_func=generate_uuid, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/jstree', view_func=buildTreeStructure, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/jstree/<string:guid>', view_func=buildTreeStructure, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/get_directive_by_guid/<string:guid>', view_func=getDirectiveByGUID, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/add_folder', view_func=addFolderToTree, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/add_directive', view_func=addDirectiveToTree, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/check_directive_for_changes', view_func=checkDirectiveForChanges, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/dir', view_func=update_directive, methods=["POST", "DELETE", "PUT"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/reaction/<string:_id>', view_func=get_reaction, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/reaction', view_func=update_reaction, methods=["POST", "DELETE", "PUT"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/tree', view_func=get_directive_tree, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/save', view_func=save_directive, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/remove', view_func=remove_directive, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/pause', view_func=pause_directive, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/resume', view_func=resume_directive, methods=["POST"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/status/<string:_id>', view_func=get_directive_status, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)
#     route('/correl/alldirectives', view_func=get_alldirectives, methods=["GET"], access_groups=PAGE_CORRESPONDENCIES)

#     from configuration.controllers import *
#     route('/config/users-and-roles', view_func=ajax_users_and_roles, methods=["GET"], access_groups=PAGE_USERS)

#     route('/config/group/all', view_func=groups_get_list, methods=["GET"], access_groups=PAGE_USERS)
#     route('/config/group', view_func=group_updater, methods=["POST", "PUT"], access_groups=PAGE_USERS)
#     route('/config/group/<int:_id>', view_func=delete_group_controller, methods=["DELETE"], access_groups=PAGE_USERS)

#     route('/config/journals', view_func=ajax_journals, methods=["GET"], access_groups=PAGE_JOURNALS)

#     route('/config/license', view_func=ajax_license, methods=["GET"], access_groups=PAGE_LICENSE)

#     route('/config/plugins', view_func=ajax_plugins, methods=["GET"], access_groups=PAGE_PLUGINS)
#     route('/config/plugins/all', view_func=get_plugins, methods=["GET"], access_groups=PAGE_PLUGINS)
#     route('/config/plugins', view_func=update_plugins, methods=["POST"], access_groups=PAGE_PLUGINS)

#     route('/config/role', view_func=role_controller, methods=["POST", "PUT"], access_groups=PAGE_USERS)
#     route('/config/roles/all', view_func=roles_get_list, methods=["GET"], access_groups=PAGE_USERS)
#     route('/config/role/<int:_id>', view_func=delete_role_controller, methods=["POST"], access_groups=PAGE_USERS)

#     route('/config/user', view_func=update_user_controller, methods=["POST", "PUT"], access_groups=PAGE_USERS)
#     route('/config/users/all', view_func=users_get_list, methods=["GET"], access_groups=PAGE_USERS)
#     route('/config/user/<int:_id>', view_func=delete_user_controller, methods=["POST"], access_groups=PAGE_USERS)

#     route('/config/rotations', view_func=ajax_rotation, methods=["GET"], access_groups=PAGE_ROTATIONS)
#     route('/config/rotations/all', view_func=get_all_rotations, methods=["GET"], access_groups=PAGE_ROTATIONS)
#     route('/config/rotations/cfg', view_func=get_config_rotations, methods=["GET"], access_groups=PAGE_ROTATIONS)
#     route('/config/rotations/cfg', view_func=update_config_rotations, methods=["POST"], access_groups=PAGE_ROTATIONS)
#     route('/config/rotation/<int:_id>', view_func=get_rotation, methods=["GET"], access_groups=PAGE_ROTATIONS)
#     route('/config/rotations', view_func=update_rotation, methods=["POST"], access_groups=PAGE_ROTATIONS)
#     route('/config/rotations/comment', view_func=update_rotation_comment, methods=["POST"], access_groups=PAGE_ROTATIONS)

#     from availability_monitoring.controllers import *
#     route('/nagvis_frame', view_func=asref_nagvis_frame, access_groups=PAGE_NAGVIS_NAGIOS)
#     route('/cgi-bin/nagios3_frame', view_func=asref_nagios_frame, access_groups=PAGE_NAGVIS_NAGIOS)
#     route('/nagvis/<path:url>', view_func=nagvis_proxy, access_groups=PAGE_NAGVIS_NAGIOS)
#     route('/nagvis', view_func=nagvis_proxy, access_groups=PAGE_NAGVIS_NAGIOS)
#     route('/nagios3/<path:url>', view_func=nagious3_proxy, access_groups=PAGE_NAGVIS_NAGIOS)
#     route('/nagios3', view_func=nagious3_proxy, access_groups=PAGE_NAGVIS_NAGIOS)
#     route('/cgi-bin/nagios3', view_func=cli_bin_nagios, access_groups=PAGE_NAGVIS_NAGIOS)
#     route('/cgi-bin/nagios3/<path:url>', view_func=cli_bin_nagios, access_groups=PAGE_NAGVIS_NAGIOS)
#     route('/config/license', view_func=ajax_license, methods=["GET"], access_groups=PAGE_LICENSE)
#     route('/avmonitor/settings', view_func=ajax_nagios_settings, methods=["GET"], access_groups=PAGE_RESOURCES_CONTROL)
#     route('/settings/all', view_func=get_all_settings, methods=["GET"], access_groups=PAGE_RESOURCES_CONTROL)
#     route('/settings/add', view_func=add_setting, methods=["POST"], access_groups=PAGE_RESOURCES_CONTROL)
#     route('/settings/configure', view_func=configure_setting, methods=["POST"], access_groups=PAGE_RESOURCES_CONTROL)
#     route('/settings/delete', view_func=delete_setting, methods=["POST"], access_groups=PAGE_RESOURCES_CONTROL)

#     from fileserver.controller import upload_file, get_file
#     route('/file/<string:a>/<string:b>/<string:c>/<string:filename>', view_func=get_file, methods=["GET"], access_groups=FILES_ACCESS)
#     route('/file', view_func=upload_file, methods=["POST"], access_groups=FILES_ACCESS)








