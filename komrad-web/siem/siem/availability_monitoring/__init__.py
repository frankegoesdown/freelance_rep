# -*- coding: utf-8 -*-
"""Proxy bluepring for nagvis and nagious servers

Attributes:
    avail_monitor (blueprint):
"""

from flask import Blueprint

avail_monitor = Blueprint('avail_monitor', __name__)

from controllers import *