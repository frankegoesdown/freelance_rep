# -*- coding: utf-8 -*-
"""Its the proxy controllers for nagvis and nagios3
Temlates are same but they are wrapped by iframe tag
"""

from flask import render_template, jsonify, request, Response
import requests as curl

from siem.wrappers import access_required
from siem.availability_monitoring import avail_monitor
from flask import current_app as app
import siem.route_access as ra

import re


@avail_monitor.route('/nagvis_frame')
@access_required(ra.PAGE_NAGVIS)
def asref_nagvis_frame():
    """Iframe for nagvis
    """
    req_str = '<iframe src="nagvis"'\
              ' width="100%" height="100%"></iframe>'
    return Response(req_str)


@avail_monitor.route('/cgi-bin/nagios3_frame')
@access_required(ra.PAGE_NAGIOS)
def asref_nagios_frame():
    """Iframe for nagios
    """

    req_str = '<iframe src="cgi-bin/nagios3/status.cgi"'\
              ' width="100%" height="100%"></iframe>'
    return Response(req_str)


@avail_monitor.route('/nagvis/<path:url>',
                     methods=["GET", "POST", "PUT", "DELETE"])
@avail_monitor.route('/nagvis',
                     methods=["GET", "POST", "PUT", "DELETE"])
@access_required(ra.PAGE_NAGVIS)
def nagvis_proxy(url=''):
    """Proxy route for for nagvis
    """
    resp = make_request(request, url, 'nagvis/')
    excluded_headers = ['content-encoding', 'content-length',
                        'transfer-encoding', 'connection']
    headers = [
        (name.lower(), value) for (name, value) in resp.raw.headers.items()
        if name.lower() not in excluded_headers
    ]
    return Response(resp.content, resp.status_code, headers)


@avail_monitor.route('/nagios3/<path:url>')
@avail_monitor.route('/nagios3')
@access_required(ra.PAGE_NAGIOS)
def nagious3_proxy(url=''):
    """Proxy route for for nagios3
    """
    resp = make_request(request, url, 'nagios3/')
    excluded_headers = ['content-encoding', 'content-length',
                        'transfer-encoding', 'connection']
    headers = [
        (name.lower(), value) for (name, value) in resp.raw.headers.items()
        if name.lower() not in excluded_headers
    ]

    def dashrepl(matchobj):
        st = matchobj.group(0).replace('href="', 'href="nagios3/')
        return st

    req_str = re.sub('<frame .*(src|href)=(\".*\"|\'.*\').*>',
                     dashrepl, resp.content)

    return Response(req_str, resp.status_code, headers)


@avail_monitor.route('/cgi-bin/nagios3',
                     methods=["GET", "POST", "PUT", "DELETE"])
@avail_monitor.route('/cgi-bin/nagios3/<path:url>',
                     methods=["GET", "POST", "PUT", "DELETE"])
@access_required(ra.PAGE_NAGIOS)
def cli_bin_nagios(url=''):
    """Proxy route for for cgi-bin
    """
    resp = make_request(request, url, 'cgi-bin/nagios3/')
    excluded_headers = ['content-encoding', 'content-length',
                        'transfer-encoding', 'connection']
    headers = [
        (name.lower(), value) for (name, value) in resp.raw.headers.items()
        if name.lower() not in excluded_headers
    ]

    # def dashrepl(matchobj):
    #     # st = matchobj.group(0).replace('href="', 'href="/cgi-bin/nagios3/')
    #     # st = matchobj.group(0).replace('href=\'', 'href=\'/cgi-bin/nagios3/')
    #     return matchobj.group(0)

    # req_str = re.sub('href=(\".*\"|\'.*\')', dashrepl, req.content)

    return Response(resp.content, resp.status_code, headers)


def make_request(request, url, prefix=''):
    """Make request to nagvis services
    
    Args:
        request (str): flask request object
        url (str): destination url
        prefix (str, optional): prefix for url
    
    Returns:
        requests.response: response object
    """
    if request.query_string:
        url = '%s?%s' % (url, request.query_string)
    url = '%s%s%s' % (app.config['NAGVIS_URL'], prefix, url)
    # print "fetching %s url %s | %s" % (request.method, url, request.values)
    resp = curl.request(
        request.method,
        url,
        data=request.values)

    return resp
