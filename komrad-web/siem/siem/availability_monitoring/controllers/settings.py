# -*- coding: utf-8 -*-

from flask import render_template, jsonify, request
from ..models import settRequester

from siem.wrappers import access_required
from siem.availability_monitoring import avail_monitor
import siem.route_access as ra


@avail_monitor.route("/avmonitor/settings", methods=["GET"])
@access_required(ra.PAGE_RESOURCES_CONTROL)
def ajax_nagios_settings():
    return render_template('avmonitor/nagios-settings.html')


@avail_monitor.route("/settings/all", methods=["GET"])
@access_required(ra.PAGE_RESOURCES_CONTROL)
def get_all_settings():
    data = settRequester.getAllSetts()
    return jsonify(data), 200


@avail_monitor.route("/settings/add", methods=["POST"])
@access_required(ra.PAGE_RESOURCES_CONTROL)
def add_setting():
    data = request.get_json()
    result = settRequester.addSett(data)
    if not result:
        return jsonify({'response': 'Connection to backend is wrong'}), 500
    return jsonify(data), 200

@avail_monitor.route("/settings/configure", methods=["POST"])
@access_required(ra.PAGE_RESOURCES_CONTROL)
def configure_setting():
    data = request.get_json()
    result = settRequester.configSett(data)
    if not result:
        return jsonify({'response': 'Connection to backend is wrong'}), 500
    return jsonify(data), 200
	
@avail_monitor.route("/settings/delete", methods=["POST"])
@access_required(ra.PAGE_RESOURCES_CONTROL)
def delete_setting():
    data = request.get_json()
    result = settRequester.deleteSett(data['id'])
    if not result:
        return jsonify({'response': 'Connection to backend is wrong'}), 500
    return jsonify(data), 200