# -*- coding: utf-8 -*-

import pdfkit
from flask import current_app, url_for


def makeReport(rendered_tmpl):

    options = {
        'encoding': "utf8",
        '--enable-javascript': '',
        "debug-javascript": '',
        '--javascript-delay': 2000,
        '--margin-right': '20mm',
        '--margin-left': '30mm',
        '--margin-top': '20mm',
        '--margin-bottom': '20mm',
        '--page-size': 'A4',
    }

    css = [
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='bower_components/bootstrap/dist/css/bootstrap.min.css'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='css/report.css'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='js/jQuery-QueryBuilder/dist/css/query-builder.default.css'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static', filename='js/jQuery-QueryBuilder/dist/scss/default.scss')
    ]

    config = pdfkit.configuration(wkhtmltopdf='/usr/bin/wkhtmltopdf.sh')

    pdf = pdfkit.from_string(rendered_tmpl, False, options=options,
                             css=css, configuration=config)
    return pdf
