# -*- coding: utf-8 -*-

from wtforms.ext.appengine.db import model_form
from wtforms import Form, BooleanField, TextField, PasswordField, validators
from siem.models import User
from flask_babel import lazy_gettext, gettext


class LoginForm(Form):
    username = TextField(u'username', validators=[validators.required()])
    password = PasswordField(u'password', validators=[validators.required()])
    _logged_user = None

    def validate_on_submit(self):
        check_validate = super(LoginForm, self).validate()
        # if our validators do not pass
        if not check_validate:
            return False
        # Does our the exist
        self._logged_user = User.get_first_by_username(self.username.data)

        if not self._logged_user:
            self.username.errors.append(gettext(u'Invalid username or password'))
            return False

        # Do the passwords match
        if not self._logged_user.check_password(self.password.data):
            self.username.errors.append(gettext(u'Invalid username or password'))
            return False

        return True

