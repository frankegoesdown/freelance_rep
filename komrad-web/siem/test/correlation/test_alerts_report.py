from json import dumps

from test.siem_unittest import *


class TestAlertsReportCtrl(AdminTestCase):

    def login(self):
        username = 'somename'
        password = 'somepass'
        user_payload = {
            'username': username,
            'password': password,
            'email': 'some@email.com',
            'role': 1
        }
        self.client.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

        res = self.client.post('/config/user', json=dumps(user_payload))
        self.assertEqual(res.status_code, 200)

    def test_make_inc_report(self):
        # get
        res, status_code = self.client.get('/incidents/report').get_json()

        self.assertEqual(status_code, 200)

    def test_update_alert(self):
        self.login()
        # get

        payload = {
            'query': None,
            'order_by': 'id',
            'order': 'desc'
        }

        res = self.client.post('/incidents/report/all', json=dumps(payload))
        self.assertEqual(res.status_code, 200)

    def test_mass_change_state(self):
        self.login()
        # post

        # тут неплохо было бы создать payload, но нужны сущности, которые буду удалять,
        # для этого придумать бы фикстуры или билдеры, да и вообще для всех инстансов
        res = self.client.post('/incidents/set_states')
        self.assertEqual(res.status_code, 200)

    def test_mass_delete_alert(self):
        self.login()
        # post

        # тоже самое, а вообще нормально, что метод delete или update, а мы посылаем post?
        res, status_code = self.client.post('/incidents/delete')
        self.assertEqual(status_code, 200)

    def make_inc_csv(self):
        self.login()
        # get

        res = self.client.get('/incidents/export')
        self.assertEqual(res.status_code, 200)

