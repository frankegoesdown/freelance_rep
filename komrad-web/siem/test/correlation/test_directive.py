from json import dumps

from test.siem_unittest import *


class TestDirictiveCtrl(AdminTestCase):

    def test_get_directive(self):
        # нужна фикстурка с созданием дириктивы
        # get
        res, status_code = self.client.get('/correl/dir/<int:_id>').get_json()

        self.assertEqual(status_code, 200)

    def test_ajax_get_alerts_list(self):
        # get
        res, status_code = self.client.get('/correl/alerts/all').get_json()

        self.assertEqual(status_code, 200)

    def test_get_directive_by_guid(self):
        # get
        res, status_code = self.client.get('/correl/get_directive_by_guid/<string:guid>')
        self.assertEqual(status_code, 200)

    def test_add_folder_to_tree(self):
        payload = {
            'name': None
        }

        res, status_code = self.client.post('/correl/add_folder', json=dumps(payload))
        self.assertEqual(status_code, 200)

    def test_add_dirictive_to_tree(self):
        payload = {
            'name': None
        }

        res, status_code = self.client.post('/correl/add_directive', json=dumps(payload))
        self.assertEqual(status_code, 200)

    def test_check_diricrive_for_changes(self):
        payload = {
            'directive': None,
            'reaction': None,
            'prev_sel_guid': None
        }

        res, status_code = self.client.post('/correl/check_directive_for_changes', json=dumps(payload))
        self.assertEqual(status_code, 200)

    def test_update_dirictive(self):
        url = '/correl/dir'
        payload = {
            'id': None,
            'name': None,
            'breadcrumbs': None,
            'ru_name': None,
            'uuid': None,
            'data': None,
            'status': None
        }

        res, status_code = self.client.post(url, json=dumps(payload))
        self.assertEqual(status_code, 200)

        res, status_code = self.client.put(url, json=dumps(payload))
        self.assertEqual(status_code, 200)

        payload = {
            'id': None,
        }
        res, status_code = self.client.delete(url, json=dumps(payload))
        self.assertEqual(status_code, 200)

    def test_get_reaction(self):
        res, status_code = self.client.get('/correl/reaction/1')
        self.assertEqual(status_code, 200)

    def test_correl_reaction(self):
        url = '/correl/reaction'
        payload = {
            'id': None,
            'status': None,
            'mail_status': None,
            'scripts_status': None,
            'ext_status': None,
            'scripts': None,
            'ext_services': None,
            'groups_assign': None
        }

        res, status_code = self.client.post(url, json=dumps(payload))
        self.assertEqual(status_code, 200)

        res, status_code = self.client.put(url, json=dumps(payload))
        self.assertEqual(status_code, 200)

        payload = {
            'id': None,
        }
        res, status_code = self.client.delete(url, json=dumps(payload))
        self.assertEqual(status_code, 200)

    def test_save_directive(self):
        payload = {
            'uuid': None,
            'status': None
        }

        res, status_code = self.client.post('/correl/save', json=dumps(payload))
        self.assertEqual(status_code, 200)

    def test_remove_directive(self):
        payload = {
            'uuid': None
        }

        res, status_code = self.client.post('/correl/remove', json=dumps(payload))
        self.assertEqual(status_code, 200)

    def test_pause_directive(self):
        payload = {
            'uuid': None
        }

        res, status_code = self.client.post('/correl/pause', json=dumps(payload))
        self.assertEqual(status_code, 200)

    def test_resume_directive(self):
        payload = {
            'uuid': None
        }

        res, status_code = self.client.post('/correl/resume', json=dumps(payload))
        self.assertEqual(status_code, 200)

    def test_get_all_directives(self):
        res, status_code = self.client.post('/correl/alldirectives')
        self.assertEqual(status_code, 200)
