from test.siem_unittest import *
from flask import url_for
import time
from siem.extensions import db

from flask import Flask, session, request
from flask_socketio import SocketIO, send, emit, join_room, leave_room, \
    Namespace

import json

class TestUserCtrl(AdminTestCase):

    def login(self, username, password):
        return self.client.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        pass

    def test_create(self):
        pass
        s = 'neoone' + str(time.time())
        self.user_info = {
            'username': s,
            'password': 'neoone',
            'email': s + '@cnpo.ru',
            'role': 1
        }
        rv_create = self.client.post('/config/user', json=self.user_info)
        self.assertEqual(rv_create.status_code, 200)
        all_users = self.client.get('/config/users/all').get_json()
        user_index = next((index for (index, d) in enumerate(all_users["response"]) if d["username"] == self.user_info["username"]), -1)
        self.assertGreaterEqual(user_index, 0)

    def test_update(self):
        pass

    def test_remove(self):
        pass

    def test_get(self):
        pass
