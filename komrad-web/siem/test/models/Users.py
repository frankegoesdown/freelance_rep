from test.siem_unittest import *
from flask import url_for
import time
from siem.extensions import db


from siem.models import User, Group, UsersToGroups

import json

class TestUserCtrl(AdminTestCase):

    def test_create_remove(self):
        _test_str = randomword(8) + str(time.time())
        st_user = User.add(
            _test_str, _test_str + '@cnpo.ru', _test_str, 1,
            _test_str + 'name', _test_str + '-ltname')
        self.assertIsNotNone(User.query.filter_by(username=_test_str).all())
        self.assertIsNotNone(Group.query.filter_by(name=_test_str).all())
        User.remove(st_user.id)
        self.assertEqual(Group.query.filter_by(name=_test_str).all(), [])
        self.assertEqual(User.query.filter_by(username=_test_str).all(), [])
        self.assertEqual(UsersToGroups.query.filter_by(
            user_id=st_user.id).all(), [])

