from json import dumps

from test.siem_unittest import *


class TestCommonCtrl(AdminTestCase):

    def test_get_all_lists(self):
        # get
        res, status_code = self.client.get('/all').get_json()

        self.assertEqual(status_code, 200)

    def test_ajax_all_lists(self):
        # get
        res, status_code = self.client.get('/lists').get_json()

        self.assertEqual(status_code, 200)

    def test_raw_auto_request(self):
        payload = {
            'request': 'id=1'  # idk about request
        }
        # post
        res, status_code = self.client.post('/raw_request', json=dumps(payload))
        self.assertEqual(status_code, 200)
