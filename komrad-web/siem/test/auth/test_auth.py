from test.siem_unittest import *
from flask import url_for


from flask import Flask, session, request
from flask_socketio import SocketIO, send, emit, join_room, leave_room, \
    Namespace


class TestAuth(NoAuthTestCase):

    def login(self, username, password):
        return self.client.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.client.post('/logout', follow_redirects=True)

    def test_login_logout(self):
        rv = self.login('admin', 'admin')
        self.assertEqual(rv.status_code, 200)

        rv = self.logout()
        self.assertEqual(rv.status_code, 200)

        # Must be reworked
        # rv = self.login('nothing', 'noone')
        # self.assertEqual(rv.status_code, 400)
